﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TenDangnhapController : MonoBehaviour
{

    [SerializeField]
    public InputField InputName;

    [SerializeField]
    GameObject TenGoiY;
    [SerializeField]
    Text TxtThongBaoTrungTen;
    [SerializeField]
    Button[] ButtonNames;

    [SerializeField]
    LoginController Controll;

    Vector3 ViTriBanDau = new Vector3(81, 39, 0);
    Vector3 ViTriKhiGoiY = new Vector3(81, -59, 0);

    float H_BanĐau = 266;
    float H_KhiGoiY = 533;

    List<string> CacTenGoiY = null;
    public void Init(string Name, List<string> _CacTenGoiY)
    {
        InputName.characterLimit = 30;
        InputName.text = Name;
        CacTenGoiY = _CacTenGoiY;
        for (int i = 0; i < ButtonNames.Length; i++)
        {
            ButtonNames[i].onClick.RemoveAllListeners();
        }
        ButtonNames[0].onClick.AddListener(BtnName_1_OnClick);
        ButtonNames[1].onClick.AddListener(BtnName_2_OnClick);
        ButtonNames[2].onClick.AddListener(BtnName_3_OnClick);
        ButtonNames[3].onClick.AddListener(BtnName_4_OnClick);
        ButtonNames[4].onClick.AddListener(BtnName_5_OnClick);
        ButtonNames[5].onClick.AddListener(BtnName_6_OnClick);
        for (int i = 0; i < CacTenGoiY.Count; i++)
        {
            ButtonNames[i].gameObject.SetActive(true);
        }
    }

    public void ClearData()
    {
        InputName.text = "";
        CloseTenGoiY();
        CacTenGoiY = null;
    }
    public void ThayDoiKichThuoc(bool TroVeMacDinh = false)
    {
        if (TroVeMacDinh == false)
        {
            this.gameObject.GetComponent<RectTransform>().anchoredPosition = ViTriKhiGoiY;
            Vector2 t = new Vector2(831, H_KhiGoiY);
            this.gameObject.GetComponent<RectTransform>().sizeDelta = t;
        }
        else
        {
            this.gameObject.GetComponent<RectTransform>().anchoredPosition = ViTriBanDau;
            Vector2 t = new Vector2(831, H_BanĐau);
            this.gameObject.GetComponent<RectTransform>().sizeDelta = t;
        }
    }

    public void ShowTenGoiY(bool HopLe)
    {
        ThayDoiKichThuoc();
        if (HopLe == false)
        {
            TxtThongBaoTrungTen.text = "Tên " + InputName.text + " đã có người sử dung, Vui lòng chọn tên khác";
        }
        else
        {
            TxtThongBaoTrungTen.text = "Bạn có thẻ sử dụng Tên " + InputName.text;
        }

        for (int i = 0; i < CacTenGoiY.Count; i++)
        {
            ButtonNames[i].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = CacTenGoiY[i];
        }
        TenGoiY.SetActive(true);
    }


    public void CloseTenGoiY()
    {
        ThayDoiKichThuoc(true);
        TxtThongBaoTrungTen.text = "";
        for (int i = 0; i < ButtonNames.Length; i++)
        {
            ButtonNames[i].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "";
        }
        TenGoiY.SetActive(false);
    }




    public void BtnXacNhanOnClick()
    {
        Debug.Log("TEN USER CAN KIEM TRA:  " + InputName.text);
        Controll.KiemTraTen(InputName.text);
    }



    public void BtnName_1_OnClick()
    {
        InputName.text = CacTenGoiY[0];
    }

    public void BtnName_2_OnClick()
    {
        InputName.text = CacTenGoiY[1];
    }

    public void BtnName_3_OnClick()
    {
        InputName.text = CacTenGoiY[2];
    }
    public void BtnName_4_OnClick()
    {
        InputName.text = CacTenGoiY[3];
    }
    public void BtnName_5_OnClick()
    {
        InputName.text = CacTenGoiY[4];
    }
    public void BtnName_6_OnClick()
    {
        InputName.text = CacTenGoiY[5];
    }
}


