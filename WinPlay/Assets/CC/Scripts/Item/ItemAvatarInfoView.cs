﻿using UnityEngine;
using System.Collections;
using BaseCallBack;
using UnityEngine.UI;

public class ItemAvatarInfoView : MonoBehaviour {

	Button btnAvt;

	onCallBackString click;
	string nameAvt = "";

	public void Init(string _s, onCallBackString _click){
		nameAvt = _s;
		click = _click;

		Transform trsfAvt = transform.GetChild (0).GetChild (0);
        //trsfAvt.GetComponent<Image> ().sprite = _spr;
        //Debug.LogError("avatar " + _s);
        StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + _s, trsfAvt.GetComponent<Image> ()));
        btnAvt = trsfAvt.GetComponent<Button> ();
		btnAvt.onClick.AddListener (BtnAvtOnClick);
	}
    public IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    void BtnAvtOnClick ()
	{
		if (click != null)
			click (nameAvt);
	}
}
