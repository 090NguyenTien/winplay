﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;

public class ItemInAppView : MonoBehaviour
{


    Text txtCost, txtChip, txtGold;
    [SerializeField]
	private Image imgMain;
	Button btnItem;

	onCallBackString itemClick;

	string productID = "";

    [SerializeField]
    private List<Sprite> lstSpriteGold = null;

    [SerializeField]
    GameObject ObjThuongVip;
    [SerializeField]
    Text txtVipChip;

    public void Init(int _index, string _cost, string _chip, string _gold, string _productID, onCallBackString _itemClick, string VipChip)
	{
		GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;

		txtChip = transform.Find ("TxtValueChip").GetComponent<Text> ();
		txtGold = transform.Find ("TxtValueGold").GetComponent<Text> ();

		txtCost = transform.Find ("TxtValueCost").GetComponent<Text> ();

		btnItem = GetComponent<Button> ();
		btnItem.onClick.AddListener (ItemOnClick);

		productID = _productID;

		txtChip.text = _chip;
		txtGold.text = _gold;
		txtCost.text = "$ " + _cost;

        //Debug.LogError(_index);
        imgMain.sprite = lstSpriteGold[_index <= lstSpriteGold.Count ? _index - 1 : lstSpriteGold.Count - 1];//DataHelper.GetGoldItem (_index);
        imgMain.SetNativeSize();

        itemClick = _itemClick;

        if (VipChip != "")
        {
            txtVipChip.text = "+" + VipChip;
            ObjThuongVip.SetActive(true);
        }
	}

	void ItemOnClick()
	{
		itemClick (productID);
	}
}
