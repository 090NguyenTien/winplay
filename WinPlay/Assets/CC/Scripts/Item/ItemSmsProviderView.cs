﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemSmsProviderView : MonoBehaviour {

	[SerializeField]
	Text txtProvider;
	[SerializeField]
	Toggle toggleItem;

	onCallBackString itemOnClick;
	[HideInInspector]
	public string Provider = "";

	public void Init(string _provider, onCallBackString _onClick)
	{
		Provider = _provider;

		string first = _provider [0].ToString ();
		first = first.ToUpper ();
		_provider = _provider.Replace (_provider [0].ToString(), first);

		txtProvider.text = _provider;

		itemOnClick = _onClick;

		toggleItem.onValueChanged.AddListener (ToggleOnChange);

		toggleItem.group = transform.parent.GetComponent<ToggleGroup> ();
	}

	void ToggleOnChange (bool _enable)
	{
		if (_enable) {
			itemOnClick (Provider);
		}
	}

	public bool EnableToggle{
		set{ toggleItem.isOn = value; }
		get{ return toggleItem.isOn; }
	}
}
