﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemCardProviderExchangeView : MonoBehaviour {

	[SerializeField]
	Text txtProvider;
	[SerializeField]
	Toggle toggleItem;

	onCallBackInt itemOnClick;
	[HideInInspector]
	public int Index;

	public void Init(int _index, string _provider, onCallBackInt _onClick)
	{
		Index = _index;

		string first = _provider [0].ToString ();
		first = first.ToUpper ();
		_provider = _provider.Replace (_provider [0].ToString(), first);

		txtProvider.text = _provider;

		itemOnClick = _onClick;

		toggleItem.onValueChanged.AddListener (ToggleOnChange);

		toggleItem.group = transform.parent.GetComponent<ToggleGroup> ();
	}

	void ToggleOnChange (bool _enable)
	{
		if (_enable) {
			itemOnClick (Index);
		}
	}

	public bool EnableToggle{
		set{ toggleItem.isOn = value; }
		get{ return toggleItem.isOn; }
	}
}
