﻿using UnityEngine;
using System.Collections;

public class ItemEventMainView {

	string urlSpr;

	public void Init(string _urlSprMain)
	{
		urlSpr = _urlSprMain;
	}


	public string UrlSpr{
		get{ return urlSpr; }
		set{ urlSpr = value; }
	}
	public string UrlSprAction {
		get;
		set;
	}
	public string UrlAction {
		get;
		set;
	}

	public Sprite SprBig {
		get;
		set;
	}
	public Sprite SprAction {
		get;
		set;
	}

	public bool IsAction {
		get;
		set;
	}
	public bool IsWheel {
		get;
		set;
	}
}
