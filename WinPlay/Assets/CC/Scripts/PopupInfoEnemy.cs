﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
using UnityEngine.SceneManagement;

public class PopupInfoEnemy : MonoBehaviour
{
    [SerializeField]
    GameObject BtnKetBan, TxtStatusFriend;
    [SerializeField]
    Image Avatar, KhungAvatar;
    [SerializeField]
    Text TxtTen, TxtLoaiVip, TxtLevel, TxtChip, TxtGem;
    [SerializeField]
    List<Sprite> ListSprBoom;
    [SerializeField]
    Transform ChuaItemsBoom;
    int Enemy;
    onCallBackInt onClick;
    [SerializeField]
    GameObject Popup;
    [SerializeField]
    ControllerMB.MBInGameController MauBinhControll;
    [SerializeField]
    ControllerXT.XTInGameController XiToControll;

    [SerializeField]
    TLMNRoomController TienLenControll;
    public static PopupInfoEnemy Instance { get; set; }

    int sfsId_MB;
    string userId;
    // Use this for initialization
    public void Init(string _userId, Sprite _ava, Sprite _KhungAva, string _ten, string _vip, string _level, string _chip, string _gem, int _enemy, int sfsid = -2)
    {
        Debug.Log("Vo Toi Init");
        this.userId = _userId;
        if (_ava != null)
        {
            Avatar.sprite = _ava;
        }

        if (_KhungAva != null)
        {
            KhungAvatar.sprite = _KhungAva;
        }
        TxtTen.text = _ten;
        TxtLoaiVip.text = "Vip " + _vip;
        TxtLevel.text = "Level " + _level;
        TxtChip.text = _chip;
        TxtGem.text = _gem;

        Enemy = _enemy;

        sfsId_MB = sfsid;

        int MyVip = MyInfo.MY_ID_VIP;

        for (int i = 0; i < ChuaItemsBoom.childCount; i++)
        {

            // ChuaItemsBoom.GetChild(i).GetComponent<Button>().onClick.AddListener(ItemBoomOnClick);
            int VipUnClock = DataHelper.DicItemBoom[i];
            GameObject Boom = ChuaItemsBoom.GetChild(i).gameObject;
            if (MyVip >= VipUnClock)
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
            }
            else
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomClockOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
                Boom.GetComponent<Image>().color = Color.gray;
                Text TxtVip = Boom.transform.GetChild(0).gameObject.GetComponent<Text>();
                TxtVip.text = "Vip " + VipUnClock.ToString();
                TxtVip.gameObject.SetActive(true);
            }


        }

        Popup.SetActive(true);
        //Check đã là bạn chưa
        CheckIsFriend();
    }
    void CheckIsFriend()
    {

    }
    public void AddFriend()
    {
        BtnKetBan.SetActive(false);
        TxtStatusFriend.GetComponent<Text>().text = "Đang Chờ Đồng Ý";
        SFS.Instance.RequestAddFriend(userId);
    }
    Sprite GetSprite(int _index)
    {
        return ListSprBoom[_index];
    }


    public void ItemBoomClockOnClick(int id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        // Popup.SetActive(false);
    }

    public void ItemBoomOnClick(int id)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);


        if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
        {

            TienLenControll.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
        {
            PhomRoomController.Instance.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.MBScene.ToString()))
        {
            MauBinhControll.NemBoomControll(0, Enemy, id, sfsId_MB);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.XTScene.ToString()))
        {
            XiToControll.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.BaiCaoScence.ToString()))
        {
            BaiCaoRoomController.Instance.NemBoomControll(0, Enemy, id);
        }



    }

    public void ClosePopup()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);
    }

}
