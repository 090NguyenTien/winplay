﻿using BaseCallBack;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FriendItem : MonoBehaviour
{
    [SerializeField]
    Image ImgAvatar, ImgAvatarBorder, avatarBorderDefault, warning, Focus;
    [SerializeField]
    Sprite offIcon;
    [SerializeField]
    Text TxtUserName;
    [SerializeField]
    Button BtnHuyKetBan;
    bool isOnline;
    public string uId, uName, avatar;
     int avatarBorder;
    public int sfsId;
    int maxMessage = 50;
    
    GameObject ChatContainer, textObject;
    List<Messager> messageList = new List<Messager>();
    List<IDictionary> packetList = new List<IDictionary>();
    onCallBackGameObject callback;
    void Start()
    {
        Focus.gameObject.SetActive(false);
        warning.gameObject.SetActive(false);
    }
    public void Init(GameObject _ChatContainer, GameObject _textObject, onCallBackGameObject _cb, string _uId, string _uName, string _avatar, int _borderAvatar,bool _isOnline, int _sfsId)
    {
        ChatContainer = _ChatContainer;
        textObject = _textObject;
        callback = _cb;
        uId = _uId;
        uName = _uName;
        avatar = _avatar;
        avatarBorder = _borderAvatar;
        isOnline = _isOnline;
        TxtUserName.text = uName;
        sfsId = _sfsId;
        if (avatar != "")
        {
            //StartCoroutine(UpdateAvatarThread(avatar, ImgAvatar));
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, ImgAvatar));
            }
            else
            {
                //ImgAvatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, ImgAvatar));
            }
        }else
        {
            ImgAvatar.sprite = offIcon;
        }

        if (avatarBorder != -1)
        {
            if (DataHelper.GetBoderAvatar(avatarBorder.ToString()) != null)
            {
                this.ImgAvatarBorder.sprite = DataHelper.GetBoderAvatar(avatarBorder.ToString());
                this.ImgAvatarBorder.gameObject.SetActive(true);
                this.avatarBorderDefault.gameObject.SetActive(false);
            }
            else
            {
                this.ImgAvatarBorder.gameObject.SetActive(false);
                this.avatarBorderDefault.gameObject.SetActive(true);
            }
        }        
    }


    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

    }
    public void InActiveFriend()
    {
        Focus.gameObject.SetActive(false);
    }
    public void ActiveFriendChat()
    {
        InActiveAllFriend();
        Focus.gameObject.SetActive(true);
        warning.gameObject.SetActive(false);
        callback(gameObject);
    }
    void InActiveAllFriend()
    {
        Transform parent = gameObject.transform.parent;
        foreach(Transform friendItem in parent)
        {
            if (friendItem.GetComponent<FriendItem>() != null)
            {
                friendItem.GetComponent<FriendItem>().InActiveFriend();
            }
        }
    }

    public void AddChatFromServer(IDictionary param)
    {
        if (packetList.Count > maxMessage)
        {
            packetList.Remove(packetList[0]);            
        }
        packetList.Add(param);
        if (Focus.gameObject.activeSelf)
        {
            ShowMessage();
        }
    }
    public void ShowOfflineMessage(string msg)
    {
        Messager newMsg = new Messager();
        newMsg.text = msg;
        GameObject newText = null;

        newText = Instantiate(textObject, ChatContainer.transform);

        if (newText != null)
        {
            newMsg.textObject = newText.GetComponent<Text>();
            newMsg.textObject.text = newMsg.text;
            
            //newMsg.textObject.color = Color.blue;
            
            messageList.Add(newMsg);
        }
    }
    public void ShowMessage()
    {
        while (messageList.Count > 0)//xóa hết tin nhắn cũ tạo lại tin nhắn mới
        {
            messageList.Remove(messageList[0]);
        }
        int childs = ChatContainer.transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(ChatContainer.transform.GetChild(i).gameObject);
        }
        foreach (IDictionary param in packetList)
        {
            string msg = (string)param["message"];
            Room room = (Room)param["room"];
            User sender = (User)param["sender"];
            SFSObject data = (SFSObject)param["data"];

            string uNameSender = data.GetUtfString("uName");
            int idSFS = data.GetInt("idSFS");

            Messager newMsg = new Messager();
            newMsg.text = msg;
            GameObject newText = null;

            newText = Instantiate(textObject, ChatContainer.transform);

            if (newText != null)
            {
                newMsg.textObject = newText.GetComponent<Text>();
                newMsg.textObject.text = uNameSender + " : " + newMsg.text;
                if (idSFS == MyInfo.SFS_ID)
                {
                    newMsg.textObject.color = Color.yellow;
                }
                messageList.Add(newMsg);
            }
        }

    }
    public void OnHuyKetBan()
    {
        AlertController.api.showAlert("Bạn Muốn Xóa " + uName + " Khỏi Danh Sách Bạn Bè!",()=> {
            SFS.Instance.RequestHuyKetBan(uId);
        },true);        
    }
    internal void ChangeStatusOff()
    {
        Init(ChatContainer, textObject, callback, uId, uName, "", -1, false, -1);
    }

    internal void ChangeStatusON(string _uid, int _sfsid, string _avatar, int _avatarborder)
    {
        Init(ChatContainer, textObject, callback, _uid, uName, _avatar, _avatarborder, true, _sfsid);
    }
    public void RemoveFriend()
    {

    }
}
