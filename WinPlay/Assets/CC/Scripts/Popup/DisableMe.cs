﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMe : MonoBehaviour {

    public void disableMe()
    {
        gameObject.SetActive(false);
    }
}
