﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using DG.Tweening;

public class PopupAlertManager : MonoBehaviour {

	[SerializeField]
	GameObject panel, panelAlert, panelNoti, ImgGoldTime;
    
    [SerializeField]
    AlertPromotion alertPromotion;
	[SerializeField]
	CanvasGroup canvasNoti;
	[SerializeField]
	RectTransform trsfNoti;
		[SerializeField]
		Text txtContent, txtNoti;
		[SerializeField]
		Button btnOk;

		onCallBack okOnClick;

		public void Init()
		{
				btnOk.onClick.AddListener (OkOnClick);
		}
	public void Show(string _title, string _content, onCallBack _okClick)
	{
		panel.SetActive (true);
		panelAlert.SetActive (true);
		panelNoti.SetActive (false);
		txtContent.text = _content;
		okOnClick = _okClick;
	}
	public void Show(string _content, onCallBack _okClick,bool isShowBtnShop=false)
	{
		panel.SetActive (true);
		panelAlert.SetActive (true);
		panelNoti.SetActive (false);

		txtContent.text = _content;
		okOnClick = _okClick;

        if(_content == ConstText.ErrorConnection)
        {
            MyInfo.CALL_SERVICE_INIT = true;
        }
	}
	Coroutine notiThread;
	Tweener tweenNotiMove, tweenNotiFade;

	const float TIME_NOTI = 1;

	public void ShowNoti(string _msg){

		if (notiThread != null)
			StopCoroutine (notiThread);

		if (tweenNotiMove != null)
			tweenNotiMove.Kill ();

		if (tweenNotiFade != null)
			tweenNotiFade.Kill ();


		panel.SetActive (true);
		panelNoti.SetActive (true);
		panelAlert.SetActive (false);

		trsfNoti.localPosition = Vector3.zero;
		tweenNotiMove = trsfNoti.DOAnchorPos (new Vector2 (0, 50), TIME_NOTI);

		txtNoti.text = _msg;

		canvasNoti.alpha = 1;
		CallBack (TIME_NOTI / 2, () => {
			tweenNotiFade = canvasNoti.DOFade (0, TIME_NOTI / 2).OnComplete (HideNoti);
		});
	}
	void CallBack(float _time, onCallBack _callBack){
		notiThread = StartCoroutine (GameHelper.Thread (_time, _callBack));
	}

    public void Hide()
    {
        panelAlert.SetActive(false);
    }
    public void HideNoti(){
		panelNoti.SetActive (false);
	}




		void OkOnClick()
		{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

				okOnClick ();
		}
    public void showAlertPromotion()
    {
        if(ImgGoldTime != null) ImgGoldTime.SetActive(true);
        int showed = PlayerPrefs.GetInt("isShowed", -1);       
        if (showed == -1)
        {           
            alertPromotion.Show();
        }
        else
        {            
            int show = Random.Range(0, 4);
            Debug.Log("Khuong===========show"+ show);
            if (show == 2)
            {
                alertPromotion.Show();
            }
        }
        
    }
    
}
