﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelLuatChoiController : MonoBehaviour
{

    [SerializeField]
    GameObject PanelLuatChoi;

    [SerializeField]
    Image ImgLuatChoi;

    [SerializeField]
    Button Btn;

    [SerializeField]
    Sprite MB_1, MB_2, PHOM, XT, TLDL, TLMN, CAO, LEN, XUONG;
    int id = 0;

   void Init()
    {
        Btn.onClick.RemoveAllListeners();
        Btn.gameObject.SetActive(false);
        Debug.Log("id game ====  " + GameHelper.currentGid);
        id = GameHelper.currentGid;
        if (id == 1)
        {
            // xi to
            ImgLuatChoi.sprite = XT;
        }
        else if (id == 2)
        {
            // mb
            MoMB_1();
            Btn.gameObject.SetActive(true);

        }
        else if (id == 4)
        {
            // phom
            ImgLuatChoi.sprite = PHOM;
        }
        else if (id == 5)
        {
            // tlmn
            ImgLuatChoi.sprite = TLMN;
        }
        else if (id == 6)
        {
            // tldl
            ImgLuatChoi.sprite = TLDL;
        }
        else if (id == 10)
        {
            // Cao
            ImgLuatChoi.sprite = CAO;
        }
    }

    void MoMB_2()
    {
        ImgLuatChoi.sprite = MB_2;
        Btn.gameObject.GetComponent<Image>().sprite = LEN;
        Btn.onClick.RemoveAllListeners();
        Btn.onClick.AddListener(MoMB_1);
    }

    void MoMB_1()
    {
        ImgLuatChoi.sprite = MB_1;
        Btn.gameObject.GetComponent<Image>().sprite = XUONG;
        Btn.onClick.RemoveAllListeners();
        Btn.onClick.AddListener(MoMB_2);
    }

    public void OpenMe()
    {
        Init();
        PanelLuatChoi.SetActive(true);
    }

    public void CloseMe()
    {
        PanelLuatChoi.SetActive(false);
    }
}
