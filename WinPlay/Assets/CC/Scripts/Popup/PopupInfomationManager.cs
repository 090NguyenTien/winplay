﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class PopupInfomationManager : MonoBehaviour {

	[SerializeField]
    public GameObject panel, panelInfo, panelChangePass;
	[SerializeField]
    public Transform trsfPopup;
	[SerializeField]
    public Button btnSave, btnChangePass, btnClose, btnLogOut;
	[SerializeField]
    public Image imgAvatar, imgBorderAvt, MY_imgBorderAvt;
	[SerializeField]
    public Text txtVIP ,txtName, txtChip, txtGold, txtWin, txtLose, txtDraw;

	[SerializeField]
    public InputField inputUsername, inputPhone, inputEmail, inputID;
	[SerializeField]
    public Toggle toggleMale, toggleFemale, toggleNone;

	onCallBack saveClick, closeClick;

    [SerializeField]
    public Toggle ToggleThongTin, ToggleVip, ToggleAvatar;
    [SerializeField]
    GameObject PanelVip;

    [SerializeField]
    Image SliderVIP;

    [SerializeField]
    Text VIPhientai, TenVipMin, TenVipMax, PointMin, PointMax, ThuongDatMoc, ThuongNgay, ThuongPhanTram, VipPoint;


    [SerializeField]
    HomeViewV2 View;
    [SerializeField]
    LobbyView LobbyView;

    [SerializeField]
    Text TxtbtnChangePass, TxtbtnLogOut;
    [SerializeField]
    Sprite SprBorAvatar_DF;


    public void Init(onCallBack _saveClick, onCallBack _closeClick)
	{
		btnSave.onClick.AddListener (BtnSaveOnClick);

        TxtbtnChangePass.text = "Đổi mật khẩu";
        btnChangePass.onClick.RemoveAllListeners();
        btnChangePass.onClick.AddListener (BtnChangePassOnClick);
        btnChangePass.gameObject.SetActive(true);

		btnClose.onClick.AddListener (BtnCloseOnClick);

		btnCancelPass.onClick.AddListener (BtnCancelPassOnClick);
		btnUpdatePass.onClick.AddListener (BtnUpdatePassOnClick);

        TxtbtnLogOut.text = "Đăng xuất";
        btnLogOut.onClick.RemoveAllListeners();
        btnLogOut.onClick.AddListener(LogOutOnClick);

        btnLogOut.gameObject.SetActive(true);

        

        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        if (loginType == LoginType.Facebook)
        {
            btnChangeAvatar.gameObject.SetActive(false);
        }
        else
        {
         //   btnChangeAvatar.gameObject.SetActive(true);
            btnChangeAvatar.onClick.AddListener(BtnChangeAvatarOnClick);
        }
            
		btnUpdateAvatar.onClick.AddListener (BtnUpdateAvatarOnClick);
		btnCancelUpdateAvatar.onClick.AddListener (BtnCancelUpdateAvatarOnClick);
        btnCancelUpdateAvatar.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Hủy";


        //  

        saveClick = _saveClick;
		closeClick = _closeClick;

        ToggleThongTin.onValueChanged.RemoveAllListeners();
        ToggleThongTin.onValueChanged.AddListener(delegate { MoThongTin(); });

        ToggleVip.onValueChanged.RemoveAllListeners();
        ToggleVip.onValueChanged.AddListener(delegate { MoVIP(); });

        ToggleAvatar.onValueChanged.RemoveAllListeners();
        ToggleAvatar.onValueChanged.AddListener(delegate { MoAvatar(); });

        


        //  TG.onValueChanged()
    }




    void MoThongTin()
    {
        if (ToggleThongTin.isOn == true)
        {
            btnChangePass.onClick.RemoveAllListeners();
            btnChangePass.onClick.AddListener(BtnChangePassOnClick);
            TxtbtnChangePass.text = "Đổi mật khẩu";
            btnChangePass.gameObject.SetActive(true);

            btnLogOut.onClick.RemoveAllListeners();
            btnLogOut.onClick.AddListener(LogOutOnClick);
            TxtbtnLogOut.text = "Đăng xuất";
            btnLogOut.gameObject.SetActive(true);

            ToggleAvatar.isOn = false;
            ToggleVip.isOn = false;

            panelInfo.SetActive(true);
            panelChangePass.SetActive(false);
            PanelVip.SetActive(false);
            panelAvatar.SetActive(false);
        }
    }


    void MoVIP ()
    {
        if (ToggleVip.isOn == true)
        {
            btnChangePass.gameObject.SetActive(false);
            btnLogOut.gameObject.SetActive(false);


            ToggleAvatar.isOn = false;
            ToggleThongTin.isOn = false;

            panelInfo.SetActive(false);
            panelChangePass.SetActive(false);
            PanelVip.SetActive(true);
            panelAvatar.SetActive(false);

            DuLieuTapVip();

            

        }
    }

    void MoAvatar()
    {
        if (ToggleAvatar.isOn == true)
        {
            btnChangePass.onClick.RemoveAllListeners();
            btnChangePass.onClick.AddListener(OpenScollAvatar);
            btnChangePass.gameObject.GetComponent<Image>().color = Color.white;
            TxtbtnChangePass.text = "Đổi Avatar";
            btnChangePass.gameObject.SetActive(true);

            btnLogOut.onClick.RemoveAllListeners();
            btnLogOut.onClick.AddListener(OpenScollBorAvatar);

            btnLogOut.gameObject.GetComponent<Image>().color = Color.gray;

            TxtbtnLogOut.text = "Đổi khung";
            btnLogOut.gameObject.SetActive(true);

            ToggleVip.isOn = false;
            ToggleThongTin.isOn = false;

            panelChangePass.SetActive(false);
            panelInfo.SetActive(false);
            PanelVip.SetActive(false);
            panelAvatar.SetActive(true);
            BtnChangeAvatarOnClick();

            ScollAvatar.SetActive(true);
            ScollBoderAvatar.SetActive(false);

           
            
        }
    }


    public void OpenScollAvatar()
    {
        ScollAvatar.SetActive(true);
        ScollBoderAvatar.SetActive(false);
        btnUpdateAvatar.onClick.RemoveAllListeners();
        btnUpdateAvatar.onClick.AddListener(BtnUpdateAvatarOnClick);
        btnChangePass.gameObject.GetComponent<Image>().color = Color.white;
        btnLogOut.gameObject.GetComponent<Image>().color = Color.gray;

        btnCancelUpdateAvatar.onClick.RemoveAllListeners();
        btnCancelUpdateAvatar.onClick.AddListener(BtnCancelUpdateAvatarOnClick);

        btnCancelUpdateAvatar.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Hủy";
    }

    public void OpenScollBorAvatar()
    {
        CreateItemBorderAvatar();
        ScollAvatar.SetActive(false);
        ScollBoderAvatar.SetActive(true);

        btnChangePass.gameObject.GetComponent<Image>().color = Color.gray;
        btnLogOut.gameObject.GetComponent<Image>().color = Color.white;

        btnCancelUpdateAvatar.onClick.RemoveAllListeners();

        btnUpdateAvatar.onClick.RemoveAllListeners();
        btnUpdateAvatar.onClick.AddListener(BtnUpdateBorderAvatarOnClick);

        Scene m_Scene = SceneManager.GetActiveScene();

        if (m_Scene.name == "HomeSceneV2")
        {
            
            btnCancelUpdateAvatar.onClick.AddListener(View.OpenPanelShopBorAvatar);
        }
        else if (m_Scene.name == "WaitingRoom")
        {
           
            btnCancelUpdateAvatar.onClick.AddListener(LobbyView.OpenPanelShopBorAvatar);
        }

        

        btnCancelUpdateAvatar.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Thêm Khung";

       
    }

    public void MoPanelVip()
    {
        ToggleVip.isOn = true;
        ToggleAvatar.isOn = false;
        ToggleThongTin.isOn = false;
        MoVIP();
    }
    public void MoPanelVipNgoaiTrangHome()
    {
        Show();
        MoPanelVip();
    }


    void DuLieuTapVip()
    {
        int Id_VipTiepTheo = MyInfo.MY_ID_VIP;


        TenVipMin.text = MyInfo.MY_VIP;
        TenVipMax.text = MyInfo.MY_NEXT_VIP;

        GoiVip GoiVipTiepTheo = VIP_Controll.DicVipData[Id_VipTiepTheo];

        long MaxPoint = long.Parse(GoiVipTiepTheo.MaxPoint);

        long MinPoint = long.Parse(GoiVipTiepTheo.MinPoint);

        PointMax.text = Utilities.GetStringVipPointByLong(MaxPoint);

        PointMin.text = Utilities.GetStringVipPointByLong(MinPoint);

        long VipCollect = long.Parse(MyInfo.MY_VIP_COLLECT.ToString());


        ChangeSliderVIP(MaxPoint, MinPoint, VipCollect);
        VIPhientai.text = "Quyền lợi của " + MyInfo.MY_VIP;

       

        if (MyInfo.MY_ID_VIP < 1)
        {
            ThuongDatMoc.text = "";
            ThuongNgay.text = "";
            ThuongPhanTram.text = "";
        }
        else
        {
            GoiVip GoiVip = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];

            long DatMoc = long.Parse(GoiVip.ThuongDatMoc);
            long Ngay = long.Parse(GoiVip.ThuongNgay);
            long PhanTram = long.Parse(GoiVip.ThuongPhanTram);

            string DM = Utilities.GetStringMoneyByLong(DatMoc);
            string NG = Utilities.GetStringMoneyByLong(Ngay);
            string PT = Utilities.GetStringVipPointByLong(PhanTram);
            ThuongDatMoc.text = "Nhận ngay " + DM + " Chip khi đặt mốc " + MyInfo.MY_VIP;
            ThuongNgay.text = "Nhận " + NG + " Chip mỗi ngày";
            ThuongPhanTram.text = "Tặng thêm " + PT + " % Chip mỗi lần nạp Chip";
        }
        

        string VCL = Utilities.GetStringVipPointByLong(VipCollect);
        VipPoint.text = "VIP POINT: " + VCL;

    }



    void ChangeSliderVIP(long MaxPoint, long MinPoint, long VipCollect)
    {
        long Tong = MaxPoint - MinPoint;
        long Diem = VipCollect - MinPoint;
        // test
        
        
       float KQ = float.Parse(Diem.ToString()) / float.Parse(Tong.ToString());

        SliderVIP.fillAmount = KQ;
       
    }




    public void LogOutOnClick()//goi tu editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        SFS.Instance.LogOut();
    }

    void BtnCloseOnClick ()
	{
        panelChangePass.SetActive(false);
        closeClick ();
	}

	public void Show()
	{
		panel.SetActive (true);

		txtName.text = MyInfo.NAME;
		imgAvatar.sprite = MyInfo.sprAvatar;

        if (MyInfo.sprBorAvatar != null)
        {
            MY_imgBorderAvt.sprite = MyInfo.sprBorAvatar;
        }
        else
        {
            MY_imgBorderAvt.sprite = SprBorAvatar_DF;
        }

        MY_imgBorderAvt.transform.localScale = new Vector2(1.1f, 1.1f);

        imgBorderAvt.sprite = DataHelper.GetVip (MyInfo.VIPPOINT);

        txtVIP.text = MyInfo.MY_VIP;
        

        txtChip.text = Utilities.GetStringMoneyByLong (MyInfo.CHIP);
		txtGold.text = Utilities.GetStringMoneyByLong (MyInfo.GOLD);
		//		txtWin.text = MyInfo.WIN.ToString ();
		//		txtLose.text = MyInfo.LOSE.ToString ();
		//		txtDraw.text = MyInfo.DRAW.ToString ();

		inputUsername.text = MyInfo.NAME;
		inputEmail.text = MyInfo.EMAIL;
		inputID.text = MyInfo.CMND;
		inputPhone.text = MyInfo.PHONE;

		if (MyInfo.GENDER == -1)
			toggleNone.isOn = true;
		else if (MyInfo.GENDER == 0)
			toggleMale.isOn = true;
		else if (MyInfo.GENDER == 1)
			toggleFemale.isOn = true;

		SetEnablePanelRight (true, false, false);

		GameHelper.DoOpen (trsfPopup, OnShow);
		
	}
	void OnShow()
	{
		
	}
	public void Hide()
	{
        panelInfo.SetActive(true);
        PanelVip.SetActive(false);
        panelAvatar.SetActive(false);
        ToggleThongTin.isOn = true;
        GameHelper.DoClose (trsfPopup, OnHide);
	}
	void OnHide()
	{
		panel.SetActive (false);
	}

	void BtnSaveOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		ShowErrorInfo ();

		bool isOK1 = inputEmail.text.Contains ("@") && inputEmail.text.Contains (".");
		if (!isOK1) {
			if (string.IsNullOrEmpty (inputEmail.text))
				API.Instance.RequestUpdateUserInfo (inputID.text, inputPhone.text, inputEmail.text,MyInfo.AvatarName, RspUpdateUserInfo);
			else
				ShowErrorInfo ("Email không hợp lệ.");
		}
		else
			API.Instance.RequestUpdateUserInfo (inputID.text, inputPhone.text, inputEmail.text, MyInfo.AvatarName, RspUpdateUserInfo);
	}

	#region INFO

	[SerializeField]
	Text txtErrorInfo;

	void ShowErrorInfo(string _msg = "")
	{
		txtErrorInfo.text = _msg;
	}

	#endregion

	#region Change Avatar

	[Header("CHANGE AVATAR")]
	[SerializeField]
	GameObject panelAvatar;
	[SerializeField]
	Transform trsfAvatarParent, trsfBoderAvatarParent;
	[SerializeField]
	Button btnChangeAvatar, btnUpdateAvatar, btnCancelUpdateAvatar;
	[SerializeField]
	GameObject objAvatar, objBorAvatar, ScollAvatar, ScollBoderAvatar;

	bool isInited = false;

	string nameAvtCurrent = "";
    string nameBoAvtCurrent = "";
    int Id_NewBorder = -1;

    void BtnChangeAvatarOnClick ()
	{
		ShowPanelAvatar ();
	}

    void BtnChangeBoderAvatarOnClick()
    {
        ShowPanelAvatar();
    }


    void BtnCancelUpdateAvatarOnClick(){
		//SetEnablePanelRight (true, false, false);

        ToggleThongTin.isOn = true;
        MoThongTin();

    }

	void InitAvatar(){
		//Dictionary<string, Sprite> dictAvatar = DataHelper.GetAvatars ();
        //  Dictionary<string, Sprite> dictBorderAvatar = DataHelper.GetBoderAvatars();
        ArrayList avatarlist = DataHelper.GetAvatarList();
        /*foreach (string key in dictAvatar.Keys){
			GameObject item = Instantiate (objAvatar) as GameObject;
			item.transform.SetParent (trsfAvatarParent);
			item.transform.localScale = Vector3.one;
			item.GetComponent<ItemAvatarInfoView> ().Init (key, dictAvatar [key], ItemAvatarInfoOnClick);
		}*/
        foreach (string avatar in avatarlist)
        {
            GameObject item = Instantiate(objAvatar) as GameObject;
            item.transform.SetParent(trsfAvatarParent);
            item.transform.localScale = Vector3.one;
            item.GetComponent<ItemAvatarInfoView>().Init(avatar, ItemAvatarInfoOnClick);
        }
        // CreateItemBorderAvatar();

        /*
        for (int i = 0; i < dictBorderAvatar.Count; i++)
        {
            for (int j = 0; j < MyInfo.MY_BORDERS_AVATAR.Count; j++)
            {
                if (i == MyInfo.MY_BORDERS_AVATAR[j])
                {
                    GameObject item = Instantiate(objBorAvatar) as GameObject;
                    item.transform.SetParent(trsfBoderAvatarParent);
                    item.transform.localScale = Vector3.one;
                    item.GetComponent<ItemBoderAvatarInfoView>().Init(i.ToString(), dictBorderAvatar[i.ToString()], ItemBorAvatarInfoOnClick);
                }
            }
        }
        */



        isInited = true;
       // Debug.LogError("trsfBoderAvatarParent.GetChildCount() ----------- " + trsfBoderAvatarParent.GetChildCount());
		ShowPanelAvatar ();
	}

    public void CreateItemBorderAvatar()
    {
        Dictionary<string, Sprite> dictBorderAvatar = DataHelper.GetBoderAvatars();
        int t = 0;
        t = trsfBoderAvatarParent.childCount;
        if (t > 0)
        {
            for (int i = 0; i < t; i++)
            {
                GameObject obj = trsfBoderAvatarParent.GetChild(i).gameObject;
                Destroy(obj);
            }
        }

        int count = dictBorderAvatar.Count;
        int BorCount = MyInfo.MY_BORDERS_AVATAR.Count;
        List<int> ListBors = MyInfo.MY_BORDERS_AVATAR;

        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < BorCount; j++)
            {
                if (i == ListBors[j])
                {
                    GameObject item = Instantiate(objBorAvatar) as GameObject;
                    item.transform.SetParent(trsfBoderAvatarParent);
                    item.transform.localScale = Vector3.one;
                    item.GetComponent<ItemBoderAvatarInfoView>().Init(i.ToString(), dictBorderAvatar[i.ToString()], ItemBorAvatarInfoOnClick);
                }
            }
        }       
    }


    void ItemBorAvatarInfoOnClick(string _name)
    {
        nameBoAvtCurrent = _name;
        
        Id_NewBorder = int.Parse(nameBoAvtCurrent);

        if (DataHelper.GetBoderAvatar(_name) != null)
        {
            MY_imgBorderAvt.sprite = DataHelper.GetBoderAvatar(_name);
        }
        else
        {
            MY_imgBorderAvt.sprite = SprBorAvatar_DF;
        }

        
    }




    void ItemAvatarInfoOnClick (string _name)
	{
	    nameAvtCurrent = _name;
        StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + _name, imgAvatar));
        //imgAvatar.sprite = DataHelper.GetAvatar (_name);
	}
    public IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    void BtnUpdateAvatarOnClick ()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		API.Instance.RequestUpdateUserAvatar (nameAvtCurrent, RspUpdateAvatar);

		SFS.Instance.SendZoneRefreshData ();

	}



    void BtnUpdateBorderAvatarOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        API.Instance.RequestUpdateUserBorderAvatar(Id_NewBorder, RspUpdateBorderAvatar);

        SFS.Instance.SendZoneRefreshData();

    }


    void RspUpdateBorderAvatar(string _json)
    {
        Debug.Log("Update Border AVATAR: " + JSONNode.Parse(_json).Value);

        JSONNode node = JSONNode.Parse(_json);

        if (node["status"].AsInt == -1)
        {
            Debug.Log("Khung Avatar khong so huu ");
        }
        else
        {
            nameBoAvtCurrent = node["avatar_border"].Value;

            MyInfo.BorderAvatarName = nameBoAvtCurrent;
         //   
            MyInfo.sprBorAvatar = DataHelper.GetBoderAvatar(nameBoAvtCurrent);

           // MyInfo.MY_ID_VIP = 

            MyInfo.MY_BOR_AVATAR = int.Parse(nameBoAvtCurrent);
            MyInfo.BorderAvatarName = nameBoAvtCurrent;


            Scene m_Scene = SceneManager.GetActiveScene();

            if (m_Scene.name == "HomeSceneV2")
            {
                View.ShowBorAvatar(MyInfo.sprBorAvatar);
            }
            else if (m_Scene.name == "WaitingRoom")
            {
                LobbyView.ShowBorAvatar(MyInfo.sprBorAvatar);
            }



            

           // SetEnablePanelRight(true, false, false);
            Debug.Log("TIEN=====MyInfo.BorderAvatarName==========" + MyInfo.BorderAvatarName);
            Debug.Log("TIEN=====MyInfo.sprBorAvatar==========" + MyInfo.sprBorAvatar);
        }

        
    }


    void RspUpdateAvatar (string _json)
	{
		Debug.Log ("Update AVATAR: " + JSONNode.Parse (_json).Value);

		JSONNode node = JSONNode.Parse (_json);

		nameAvtCurrent = node ["avatar"].Value;

		MyInfo.AvatarName = nameAvtCurrent;
        Debug.Log("KHUONG=====MyInfo.AvatarName==========" + MyInfo.AvatarName);
        //MyInfo.sprAvatar = DataHelper.GetAvatar (nameAvtCurrent);
        //StartCoroutine(DataHelper.UpdateAvatarThread(API.PREFIX_AVT + nameAvtCurrent, MyInfo.sprAvatar));

        if (nameAvtCurrent.Contains("http") == true)
        {
            StartCoroutine(DataHelper.UpdateAvatarThread(nameAvtCurrent, MyInfo.sprAvatar));
        }
        else
        {
            //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
            StartCoroutine(DataHelper.UpdateAvatarThread(API.PREFIX_AVT + nameAvtCurrent, MyInfo.sprAvatar));
        }
        SetEnablePanelRight (false, false, true);
	}

	void ShowPanelAvatar(){
		Debug.Log ("SHOW AVATAR - INITED - " + isInited);
		if (isInited) {
			SetEnablePanelRight (false, false, true);
			return;
		} else {
			InitAvatar ();
		}
	}

    #endregion




    #region Change Pass

    [SerializeField]
	InputField inputOldPass, inputNewPass1, inputNewPass2;
	[SerializeField]
	Button btnCancelPass, btnUpdatePass;
	[SerializeField]
	Text txtErrorPass;

	void BtnChangePassOnClick ()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		SetEnablePanelRight (false, true, false);

		inputOldPass.text = inputNewPass1.text = inputNewPass2.text = "";
		ShowErrorPass ();
	}
	void BtnCancelPassOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		SetEnablePanelRight (true, false, false);
	}
	void BtnUpdatePassOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		ShowErrorPass ();

		if (string.IsNullOrEmpty (inputOldPass.text))
			ShowErrorPass ("Mật khẩu sai");
		else if (string.IsNullOrEmpty (inputNewPass1.text) || string.IsNullOrEmpty (inputNewPass2.text))
			ShowErrorPass ("Mật khẩu chứa ít nhất " + LoginController.MIN_CHAR_PASSWORD + " ký tự.");
		else if (!inputNewPass1.text.Equals (inputNewPass2.text))
			ShowErrorPass ("Xác nhận mật khẩu không trùng khớp.");
		else if (inputNewPass1.text.Length < LoginController.MIN_CHAR_PASSWORD)
			ShowErrorPass ("Mật khẩu chứa ít nhất " + LoginController.MIN_CHAR_PASSWORD + " ký tự.");
		else if (inputNewPass1.text.Length > LoginController.MAX_CHAR_PASSWORD)
			ShowErrorPass ("Mật khẩu chứa nhiều nhất " + LoginController.MAX_CHAR_PASSWORD + " ký tự.");
		else
			API.Instance.RequestUpdatePassword (inputOldPass.text, inputNewPass1.text, RspUpdatePass);
	}

	void RspUpdatePass (string _json)
	{
		Debug.Log (_json);
		JSONNode node = JSONNode.Parse (_json);

		if (!string.IsNullOrEmpty (node ["code"].Value)) {
			if (!string.IsNullOrEmpty (node ["message"].Value))
				ShowErrorPass (node ["message"].Value);
		} else {
			GameHelper.SavePass (inputNewPass1.text);
			ShowErrorPass ("Đổi mật khẩu thành công.");
		}
	}

	void ShowErrorPass(string _msg = "")
	{
		txtErrorPass.text = _msg;
	}

	#endregion

	void SetEnablePanelRight(bool _isInfo, bool _isPass, bool _isAvatar)
	{
		panelInfo.SetActive (_isInfo);
		panelChangePass.SetActive (_isPass);
		panelAvatar.SetActive (_isAvatar);

		ShowErrorInfo ();
		ShowErrorPass ();

		ClearPassword ();
	}

	void ClearPassword()
	{
		inputOldPass.text = inputNewPass1.text = inputNewPass2.text = "";
	}

	void RspUpdateUserInfo (string _json)
	{
		Debug.Log(_json);
		JSONNode node = JSONNode.Parse (_json);
		JSONNode nodeInfo = node ["info"];

		MyInfo.PHONE = nodeInfo [API.PHONE].Value;
		MyInfo.CMND = nodeInfo [API.CMND].Value;
		MyInfo.EMAIL = nodeInfo [API.EMAIL].Value;
		saveClick ();
	}
}

