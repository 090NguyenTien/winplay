﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseGiftReceive : MonoBehaviour {

    [SerializeField]
    Image imgGold, imgCircle;
    [SerializeField]
    Text txtGold, txtCircle;
    GameObject parent;
    public void Init(long gold, int circle, GameObject parent, bool isAnimate=false)
    {
        this.parent = parent;
        txtGold.text = gold.ToString();
        txtCircle.text = circle.ToString();
        if (isAnimate) StartCoroutine(animateGiftToTarget());
    }
    public IEnumerator animateGiftToTarget()
    {
        gameObject.SetActive(true);
        gameObject.transform.position = parent.transform.position;
        Vector3 targetPosition = HomeViewV2.api.getGoldTranform().position;
        transform.DOMove(targetPosition, 0.6f, false);
        yield return new WaitForSeconds(0.6f);
        gameObject.SetActive(false);
        yield return null;
    }
    
}
