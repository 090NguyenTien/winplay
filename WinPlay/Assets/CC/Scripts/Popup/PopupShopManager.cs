﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;
using BaseCallBack;
using System;
using UnityEngine.Purchasing;

public class PopupShopManager : MonoBehaviour
{

    [SerializeField]
    Image ImgBtnNapChip, ImgBtnNapGem;
    [SerializeField]
    Sprite SprChon, SprKhongChon;

    [SerializeField]
    GameObject PanelBuyChip, PanelBuyGem;

    [SerializeField]
    GameObject panel, panelCard, panelIAP, panelSMS, popupConfirm;

    [SerializeField]
    GameObject panelCard_GEM, panelIAP_GEM, panelSMS_GEM;

    [SerializeField]
    Button btnBack;


    [SerializeField]
    Toggle toggleCard, toggleSMS, toggleIAP;

    [SerializeField]
    Toggle toggleCard_GEM, toggleSMS_GEM, toggleIAP_GEM;

    [SerializeField]
    PopupAlertManager Alert;
    [SerializeField]
    PopupFullManager Full;

    [SerializeField]
    GameObject TitlePromotion;

    bool isInited = false;
    static string CurrentProvider = "";
    [SerializeField]
    PopupAlertManager popupPurchased;

    #region CARD

    [SerializeField]
    Text txtCardCost, txtCardChip, txtError, txtConfrimPrice;
    [SerializeField]
    Button btnChargingCard, btnConfrimSend, btnConfimBack;
    [SerializeField]
    InputField inputCardPin, inputCardSerial;
    [SerializeField]
    Dropdown dropProvider;






    [SerializeField]
    Dropdown priceProvider;

    [SerializeField]
    List<Text> ListCardChip, _ListCardCostChip;
    [SerializeField]
    List<GameObject> ListGiftVipByCard;




    [SerializeField]
    List<Text> ListCardGem, _ListCarCostdGem;

    [SerializeField]
    GameObject TitleVIP;
    [SerializeField]
    Text My_LoaiVip, My_Phantram;



    string CardPrice = "";

    static string lstCardProvider = "";
    static string lstCardProvider_GEM = "";

    #endregion

    #region SMS

    static private int HeadLine;

    [SerializeField]
    GameObject objItemProvider;
    [SerializeField]
    GameObject objItemSMS;

    // <"10000", "MB 01">
    static Dictionary<string, IDictionary> dictProviderData;
    static Dictionary<string, ItemSmsProviderView> dictProvider;
    #endregion
    

    public void BtnNapChipClick()
    {
        ImgBtnNapChip.sprite = SprChon;
        ImgBtnNapGem.sprite = SprKhongChon;

        PanelBuyChip.SetActive(true);
        PanelBuyGem.SetActive(false);
    }

    public void BtnNapGemClick()
    {
        
        ImgBtnNapChip.sprite = SprKhongChon;
        ImgBtnNapGem.sprite = SprChon;

        PanelBuyChip.SetActive(false);
        PanelBuyGem.SetActive(true);
        if (MyInfo.IS_APK_PURE)
        {
            toggleIAP.gameObject.SetActive(false);//đóng cổng in-app trên apkpure
            toggleIAP_GEM.gameObject.SetActive(false);//đóng cổng in-app trên apkpure
        }
            
    }


    #region IN APP

    [SerializeField]
    GameObject objItemInApp, objItemInApp_GEM;

    #endregion


    [SerializeField]
    List<Text> lstTxtInAppValueChip, lstTxtInAppValueCost,
        lstTxtSMSValueChip, lstTxtSMSValueCost;

    [SerializeField]
    Transform trsfInAppParent, trsfInAppParent_GEM, trsfSmsParentProvider, trsfSmsParentItem, trsfSmsParentItem_GEM;
    //[SerializeField]
    //bool IsCardOnline = false;

    static bool isRequested = false;
    static bool isRequested_GEM = false;

    static string lstCardCost, lstCardCost_GEM, lstCardChip, lstCardGem_GEM, lstCardGold,
        lstInAppCost, lstInAppCost_GEM, lstInAppChip, lstInAppGem_GEM, lstInAppGold, lstInAppIdAndroid, lstInAppIdAndroid_GEM, lstInAppIdIos, lstInAppIdIos_GEM,
        lstSmsCost, lstSmsChip, lstSmsGold, lstSmsContentViettel, lstSmsContentMobi, lstSmsContentVina;

    static string jsonInit = "";
    static string jsonInit_GEM = "";

    onCallBack callBackShopCard, backClick;

    public void Init(onCallBack _callBackShopCard, onCallBack _backClick)
    {
        btnBack.onClick.AddListener(BtnBackOnClick);

        toggleCard.onValueChanged.AddListener(ToggleOnChange);
        toggleIAP.onValueChanged.AddListener(ToggleOnChange);
        toggleSMS.onValueChanged.AddListener(ToggleOnChange);

        toggleCard_GEM.onValueChanged.AddListener(ToggleOnChange_GEM);
        toggleIAP_GEM.onValueChanged.AddListener(ToggleOnChange_GEM);
        toggleSMS_GEM.onValueChanged.AddListener(ToggleOnChange_GEM);




        btnChargingCard.onClick.AddListener(OpenPopupConfrim);

        callBackShopCard = _callBackShopCard;
        backClick = _backClick;

        isClickedItemSms = false;

        //init Shop Card Payment
        toggleIAP.isOn = true;
       
        toggleCard.isOn = false;
        toggleSMS.isOn = false;
        if (MyInfo.IS_APK_PURE)
        {
            toggleIAP.isOn = false;
            toggleCard.isOn = true;
        }
        toggleCard.gameObject.SetActive(false);
        toggleSMS.gameObject.SetActive(false);
        if (MyInfo.IS_APK_PURE)
        {
            toggleIAP.gameObject.SetActive(false);
            toggleCard.gameObject.SetActive(true);
        }
        //end
        SetEnablePanelContent(toggleCard.isOn, toggleIAP.isOn, toggleSMS.isOn);





        toggleIAP_GEM.isOn = true;
        toggleCard_GEM.isOn = false;
        toggleSMS_GEM.isOn = false;

        toggleCard_GEM.gameObject.SetActive(false);
        toggleSMS_GEM.gameObject.SetActive(false);
        //end
        SetEnablePanelContent_GEM(toggleCard_GEM.isOn, toggleIAP_GEM.isOn, toggleSMS_GEM.isOn);



        if (jsonInit == "") RequestAPI();

        if (jsonInit_GEM == "") RequestAPI_GEM();




        Debug.Log("PopupShopManager==========EnablePayment====" + GameHelper.EnablePayment);
        Debug.Log("PopupShopManager==========GameHelper.CardPayment.card====" + GameHelper.CardPayment.card);
        if (!GameHelper.EnablePayment)
        {
            toggleCard.gameObject.SetActive(false);
            toggleSMS.gameObject.SetActive(false);

            toggleCard_GEM.gameObject.SetActive(false);
            toggleSMS_GEM.gameObject.SetActive(false);
        }
        else
        {


            if (GameHelper.CardPayment.card)
            {
                toggleCard.gameObject.SetActive(true);
                toggleCard_GEM.gameObject.SetActive(true);
            }
            if (GameHelper.CardPayment.sms)
            {
                toggleSMS.gameObject.SetActive(true);
                toggleSMS_GEM.gameObject.SetActive(true);
            }
        }
        //if (!MyInfo.SHOW_SHOP_FROM_SERVICE)
        //{
        //    toggleCard.gameObject.SetActive(true);

        // }
        if (MyInfo.PC_VERSION)
        {
            toggleCard.gameObject.SetActive(true);
            toggleIAP.gameObject.SetActive(false);
            toggleCard.isOn = true;
            toggleIAP.isOn = false;
            SetEnablePanelContent(toggleCard.isOn, toggleIAP.isOn, toggleSMS.isOn);


            toggleCard_GEM.gameObject.SetActive(true);
            toggleIAP_GEM.gameObject.SetActive(false);
            toggleCard_GEM.isOn = true;
            toggleIAP_GEM.isOn = false;
            SetEnablePanelContent_GEM(toggleCard_GEM.isOn, toggleIAP_GEM.isOn, toggleSMS_GEM.isOn);
        }
        popupPurchased.Init();
        Alert.Init();
        Full.Init();

        TitlePromotion.SetActive(false);

        if (MyInfo.IS_APK_PURE)
        {
            toggleIAP.gameObject.SetActive(false);//đóng cổng in app trên ApkPure
            toggleIAP_GEM.gameObject.SetActive(false);//đóng cổng in-app trên apkpure
        }
           

    }

    private bool CheckInput()
    {
        if (inputCardPin.text.Length <= 6)
        {
			txtError.text = "Mã nạp thẻ không hợp lệ!";
            return false;
        }
        if (inputCardSerial.text.Length <= 6)
        {
			txtError.text = "Mã serial không hợp lệ!";
            return false;
        }
        return true;
    }

    void OpenPopupConfrim()
    {
        txtError.gameObject.SetActive(false);
        bool check = CheckInput();
        if (check == true)
        {
            string t = priceProvider.captionText.text;
            string c = "";
            if (t == "100000")
            {
                c = "100.000 Đ";
            }
            else if (t == "10000")
            {
                c = "10.000 Đ";
            }
            else if (t == "20000")
            {
                c = "20.000 Đ";
            }
            else if (t == "50000")
            {
                c = "50.000 Đ";
            }
            else if (t == "200000")
            {
                c = "200.000 Đ";
            }
            else if (t == "500000")
            {
                c = "500.000 Đ";
            }
            txtConfrimPrice.text = c;
            popupConfirm.SetActive(true);
            btnConfimBack.onClick.AddListener(ClosePopupConfrim);
            btnConfrimSend.onClick.AddListener(ChargingCardOnClick);
        }
        else
        {
            txtError.gameObject.SetActive(true);
        }

    }

    public void OpenPopupConfrim_2(string price)
    {
        CardPrice = price;
        Debug.Log("voo OpenPopupConfrim_2(string price)");
        txtError.gameObject.SetActive(false);
        string t = price;
        string c = "";
        if (t == "100000")
        {
            c = "100.000 Đ";
        }
        else if (t == "10000")
        {
            c = "10.000 Đ";
        }
        else if (t == "20000")
        {
            c = "20.000 Đ";
        }
        else if (t == "50000")
        {
            c = "50.000 Đ";
        }
        else if (t == "200000")
        {
            c = "200.000 Đ";
        }
        else if (t == "500000")
        {
            c = "500.000 Đ";
        }
        txtConfrimPrice.text = c;
        popupConfirm.SetActive(true);
        btnConfimBack.onClick.AddListener(ClosePopupConfrim);

        btnConfrimSend.onClick.RemoveAllListeners();
        btnConfrimSend.onClick.AddListener(ChargingCardOnClick);

    }



    public void OpenPopupConfrim_GEM(string price)
    {
        CardPrice = price;
        Debug.Log("voo OpenPopupConfrim_2(string price)");
        txtError.gameObject.SetActive(false);
        string t = price;
        string c = "";
        if (t == "100000")
        {
            c = "100.000 Đ";
        }
        else if (t == "10000")
        {
            c = "10.000 Đ";
        }
        else if (t == "20000")
        {
            c = "20.000 Đ";
        }
        else if (t == "50000")
        {
            c = "50.000 Đ";
        }
        else if (t == "200000")
        {
            c = "200.000 Đ";
        }
        else if (t == "500000")
        {
            c = "500.000 Đ";
        }
        txtConfrimPrice.text = c;
        popupConfirm.SetActive(true);
        btnConfimBack.onClick.AddListener(ClosePopupConfrim);

        btnConfrimSend.onClick.RemoveAllListeners();
        btnConfrimSend.onClick.AddListener(ChargingCardOnClick_GEM);

    }



    void ClosePopupConfrim()
    {
        // CardPrice = "";
        txtConfrimPrice.text = "";
        popupConfirm.SetActive(false);
        txtError.gameObject.SetActive(false);
        btnConfimBack.onClick.RemoveListener(ClosePopupConfrim);
        btnConfrimSend.onClick.RemoveListener(ChargingCardOnClick);
    }

    void BtnBackOnClick()
    {
        backClick();
    }

    void RequestAPI()
    {
        API.Instance.RequestShop(ResponseAPI);
    }


    void RequestAPI_GEM()
    {
        API.Instance.RequestShop_GEM(ResponseAPI_GEM);
    }






    void ResponseAPI(string _json)
    {
        Debug.Log("JSON shop:\n" + _json);
        jsonInit = _json;

        isRequested = true;
        lstCardCost = "";
        lstCardChip = "";
        lstCardGold = "";
    }


    void ResponseAPI_GEM(string _json)
    {
        Debug.Log("JSON shop GEM:\n" + _json);
        jsonInit_GEM = _json;

        isRequested_GEM = true;
        lstCardCost_GEM = "";
        lstCardGem_GEM = "";

    }












    bool isShowedCurrentScene = false;
    bool isShowedCurrentScene_GEM = false;

    public void Show(bool ByGem = false)
    {
        if (!GameHelper.EnablePayment)
        {
            LoadingManager.Instance.ENABLE = true;
            API.Instance.RequestCheckPaymentShop(CheckCompletePaymentShop);
        }
        else
        {
            ShowInfoShopInApp();
        }


        if (ByGem == false)
        {
            BtnNapChipClick();
        }
        else
        {
            BtnNapGemClick();
        }        

        panel.SetActive(true);

    }
    private void CheckCompletePaymentShop(string s)
    {
        Debug.Log("KIRKRIRKRIDfdsasfdafdafdafdaf===== " + s);
        LoadingManager.Instance.ENABLE = false;
        JSONNode node = JSONNode.Parse(s);
        int status = node["payment_status"].AsInt;
        if (status == 1)
        {
            GameHelper.EnablePayment = true;
            ReopenCardWhenCallService();
        }

        ShowInfoShopInApp();
    }
    public void ReopenCardWhenCallService()
    {
        if (!GameHelper.EnablePayment)
        {
            toggleCard.gameObject.SetActive(false);
            toggleSMS.gameObject.SetActive(false);

            toggleCard_GEM.gameObject.SetActive(false);
            toggleSMS_GEM.gameObject.SetActive(false);
        }
        else
        {


            if (GameHelper.CardPayment.card)
            {
                toggleCard.gameObject.SetActive(true);
                toggleCard_GEM.gameObject.SetActive(true);
            }
            if (GameHelper.CardPayment.sms)
            {
                toggleSMS.gameObject.SetActive(true);
                toggleSMS_GEM.gameObject.SetActive(true);
            }
        }
        //if (!MyInfo.SHOW_SHOP_FROM_SERVICE)
        //{
        //    toggleCard.gameObject.SetActive(true);

        // }
        if (MyInfo.PC_VERSION)
        {
            toggleCard.gameObject.SetActive(true);
            toggleIAP.gameObject.SetActive(false);
            toggleCard.isOn = true;
            toggleIAP.isOn = false;
            SetEnablePanelContent(toggleCard.isOn, toggleIAP.isOn, toggleSMS.isOn);


            toggleCard_GEM.gameObject.SetActive(true);
            toggleIAP_GEM.gameObject.SetActive(false);
            toggleCard_GEM.isOn = true;
            toggleIAP_GEM.isOn = false;
            SetEnablePanelContent_GEM(toggleCard_GEM.isOn, toggleIAP_GEM.isOn, toggleSMS_GEM.isOn);
        }
    }
    private void ShowInfoShopInApp()
    {
        int showed = PlayerPrefs.GetInt("isShowed", -1);
        if (showed != -1)
        {
            TitlePromotion.SetActive(true);
            Text TxtPromote = TitlePromotion.transform.Find("TxtPromote").GetComponent<Text>();
            TxtPromote.text = "X" + MyInfo.PROMOTION_IN_APP["value"];
        }

        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction += OnPuchased;
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.CallServiceGetProductInAppId();
        if (!isRequested || string.IsNullOrEmpty(jsonInit))
            return;
        Debug.Log("isShowedCurrentScene " + isShowedCurrentScene);
        if (!isShowedCurrentScene)
        {

            var data = MiniJSON.Json.Deserialize(jsonInit) as IDictionary;
            Debug.Log("INIT SHOP ________");
            foreach (DictionaryEntry entry in data)
            {
                if (entry.Key.ToString().Equals("card"))
                {
                    var dataCard = entry.Value as IDictionary;

                    InitCard(dataCard);
                }
                else if (entry.Key.ToString().Equals("sms"))
                {
                    var dataSms = entry.Value as IDictionary;

                    InitSms(dataSms);
                }
                else if (entry.Key.ToString().Equals("inapp"))
                {
                    var dataInApp = entry.Value as IDictionary;

                    InitInApp(dataInApp);
                }
            }
            isShowedCurrentScene = true;
        }


        // GEM 

        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction += OnPuchased_GEM;

        if (!isRequested_GEM || string.IsNullOrEmpty(jsonInit_GEM))
        {
            // Debug.LogWarning("isRequested_GEM = " + isRequested_GEM + "      string.IsNullOrEmpty(jsonInit_GEM) = " + string.IsNullOrEmpty(jsonInit_GEM) + "   jsonInit_GEM = " + jsonInit_GEM);
            return;
        }

        Debug.Log("isShowedCurrentScene_GEM " + isShowedCurrentScene_GEM);
        if (!isShowedCurrentScene_GEM)
        {

            var data = MiniJSON.Json.Deserialize(jsonInit_GEM) as IDictionary;
            Debug.Log("INIT SHOP_GEM ________");
            foreach (DictionaryEntry entry in data)
            {
                if (entry.Key.ToString().Equals("card"))
                {
                    var dataCard = entry.Value as IDictionary;
                    InitCard_GEM(dataCard);
                }
                else if (entry.Key.ToString().Equals("sms"))
                {
                    var dataSms = entry.Value as IDictionary;

                    InitSms_GEM(dataSms);
                }
                else if (entry.Key.ToString().Equals("inapp"))
                {
                    var dataInApp = entry.Value as IDictionary;

                    InitInApp_GEM(dataInApp);
                }
            }
            isShowedCurrentScene_GEM = true;
        }
    }

    public void Hide()
    {
        Alert.Hide();

        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction -= OnPuchased;

        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction -= OnPuchased_GEM;

        panel.SetActive(false);
    }

    #region CARD

    void InitCard(IDictionary _dataCard)
    {
        lstCardCost = lstCardChip = lstCardGold = lstCardProvider = "";

        List<string> ListChipDefaut = new List<string>();

        inputCardPin.contentType = InputField.ContentType.IntegerNumber;
        inputCardPin.characterLimit = 20;
        inputCardSerial.contentType = InputField.ContentType.IntegerNumber;
        inputCardSerial.characterLimit = 20;
        txtError.text = "";

        bool isFirstLoop = true;
        string lstCardCostUuTien="", lstCardChipUuTien="", lstCardGoldUuTien="";
        foreach (DictionaryEntry entryCard in _dataCard)
        {

            if (isFirstLoop)
            {
                var dataMobifone = entryCard.Value as IDictionary;
                foreach (DictionaryEntry entryMobi in dataMobifone)
                {

                    var dataValue = entryMobi.Value as IDictionary;

                    string chip = GetValue("chip", dataValue);

                    ListChipDefaut.Add(chip);

                    string gold = GetValue("gold", dataValue);
                    long cost = long.Parse(entryMobi.Key.ToString());
                    //if (cost == 50000||cost == 100000)
                    //{
                    //    lstCardCostUuTien += Utilities.GetStringMoneyByLong(cost) + "#";
                    //    lstCardChipUuTien += Utilities.GetStringMoneyByLong(long.Parse(chip)) + "#";
                    //    lstCardGoldUuTien += Utilities.GetStringMoneyByLong(long.Parse(gold)) + "#";
                    //}                    
                    //else{
                        lstCardCost += Utilities.GetStringMoneyByLong(long.Parse(entryMobi.Key.ToString())) + "#";
                        lstCardChip += Utilities.GetStringMoneyByLong(long.Parse(chip)) + "#";
                        lstCardGold += Utilities.GetStringMoneyByLong(long.Parse(gold)) + "#";
                    //}
                    
                }
                //lstCardCost = lstCardChipUuTien + lstCardCost;
                //lstCardChip = lstCardChipUuTien + lstCardChip;
                //lstCardGold = lstCardGoldUuTien + lstCardGold;

                lstCardCost = lstCardCost.Remove(lstCardCost.Length - 1);
                lstCardChip = lstCardChip.Remove(lstCardChip.Length - 1);
                lstCardGold = lstCardGold.Remove(lstCardGold.Length - 1);

                isFirstLoop = false;
            }


            lstCardProvider += entryCard.Key.ToString() + "#";
        }
        Debug.Log("List Provider: " + lstCardProvider);

        string[] valueCost = lstCardCost.Split('#');
        string[] valueChip = lstCardChip.Split('#');
        string[] valueGold = lstCardGold.Split('#');

        txtCardChip.text = "";
        //		txtCardGold.text = "";
        txtCardCost.text = "";

        

        int My_Id_Vip = MyInfo.MY_ID_VIP;
        long TyLePhanTram = 0;
        if (My_Id_Vip > 0)
        {
            GoiVip gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
          //  Debug.LogWarning(" TyLePhanTram ----------- " + gv.ThuongPhanTram);
            TyLePhanTram = long.Parse(gv.ThuongPhanTram);

            My_LoaiVip.text = "VIP " + My_Id_Vip.ToString();
            My_Phantram.text = gv.ThuongPhanTram + "%";
            TitleVIP.SetActive(true);
        }

        for (int i = 0; i < valueCost.Length; i++)
        {
            txtCardCost.text += valueCost[i] + "\n";
            txtCardChip.text += valueChip[i] + "\n";
            //			txtCardGold.text += valueGold [i] + "\n";
            // Debug.Log("txtCardCost.text = " + valueCost[i] + "   txtCardChip.text " + valueChip[i]);
            _ListCardCostChip[i].text = valueCost[i];
            ListCardChip[i].text = valueChip[i];

            if (My_Id_Vip > 0)
            {
                string t = ListChipDefaut[i];
                long TienTong = long.Parse(t);
                string txt = ChipThuongPhanTram(TyLePhanTram, TienTong);

                GameObject obj = ListGiftVipByCard[i];
                Text TxtVip = obj.transform.GetChild(0).gameObject.GetComponent<Text>();
                TxtVip.text = "+"+ txt;
                obj.SetActive(true);
            }

        }
        bool haveViettel = true;
        bool haveMobi = true;
        bool haveVina = true;
        if (int.Parse(GameHelper.dicConfig["Mobifone"]) == 0 || !GameHelper.CardPayment.cardMobi)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Mobifone")
                {
                    dropProvider.options.RemoveAt(i);
                    haveMobi = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Viettel"]) == 0 || !GameHelper.CardPayment.cardVietel)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Viettel")
                {
                    dropProvider.options.RemoveAt(i);
                    haveViettel = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Vinaphone"]) == 0 || !GameHelper.CardPayment.cardVina)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Vinaphone")
                {
                    dropProvider.options.RemoveAt(i);
                    haveVina = false;
                    break;
                }
            }
        }
        if (!haveMobi)
        {
            dropProvider.captionText.text = "Viettel";
            if (!haveViettel)
            {
                dropProvider.captionText.text = "Vinaphone";
                if (!haveVina)
                {
                    dropProvider.captionText.text = "";
                }
            }

        }

    }

    public string ChipThuongPhanTram(long tyle, long ChipTong)
    {
        string kq = "";
        long t = tyle * ChipTong / 100;
        kq = Utilities.GetStringMoneyByLong(t);
        return kq;
    }




    //InitCard_GEM



    void InitCard_GEM(IDictionary _dataCard)
    {
        lstCardCost_GEM = lstCardGem_GEM = lstCardProvider_GEM = "";

        /*
        inputCardPin.contentType = InputField.ContentType.IntegerNumber;
        inputCardPin.characterLimit = 20;
        inputCardSerial.contentType = InputField.ContentType.IntegerNumber;
        inputCardSerial.characterLimit = 20;
        txtError.text = "";
        */

        bool isFirstLoop = true;

        foreach (DictionaryEntry entryCard in _dataCard)
        {

            if (isFirstLoop)
            {
                var dataMobifone = entryCard.Value as IDictionary;
                foreach (DictionaryEntry entryMobi in dataMobifone)
                {

                    var dataValue = entryMobi.Value as IDictionary;

                    string gem = GetValue("gem", dataValue);
                    //  string gold = GetValue("gold", dataValue);

                    lstCardCost_GEM += Utilities.GetStringMoneyByLong(long.Parse(entryMobi.Key.ToString())) + "#";
                    lstCardGem_GEM += Utilities.GetStringMoneyByLong(long.Parse(gem)) + "#";

                }
                lstCardCost_GEM = lstCardCost_GEM.Remove(lstCardCost_GEM.Length - 1);
                lstCardGem_GEM = lstCardGem_GEM.Remove(lstCardGem_GEM.Length - 1);
                isFirstLoop = false;
            }


            lstCardProvider_GEM += entryCard.Key.ToString() + "#";
        }
        Debug.Log("lst CardProvider_GEM: " + lstCardProvider_GEM);

        string[] valueCost = lstCardCost_GEM.Split('#');
        string[] valueGem = lstCardGem_GEM.Split('#');


        //   txtCardChip.text = "";
        //		txtCardGold.text = "";
        //  txtCardCost.text = "";

        for (int i = 0; i < valueCost.Length; i++)
        {
            // txtCardCost.text += valueCost[i] + "\n";
            // txtCardChip.text += valueGem[i] + "\n";
            //			txtCardGold.text += valueGold [i] + "\n";
            // Debug.Log("txtCardCost.text = " + valueCost[i] + "   txtCardChip.text " + valueChip[i]);

            _ListCarCostdGem[i].text = valueCost[i];
            ListCardGem[i].text = valueGem[i];

           // Debug.Log("ListCardGem[i].text == " + ListCardGem[i].text);
        }



        Debug.Log("Hoan Thanh Shop Gem Card ");


        /*

        bool haveViettel = true;
        bool haveMobi = true;
        bool haveVina = true;
        if (int.Parse(GameHelper.dicConfig["Mobifone"]) == 0 || !GameHelper.CardPayment.cardMobi)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Mobifone")
                {
                    dropProvider.options.RemoveAt(i);
                    haveMobi = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Viettel"]) == 0 || !GameHelper.CardPayment.cardVietel)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Viettel")
                {
                    dropProvider.options.RemoveAt(i);
                    haveViettel = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Vinaphone"]) == 0 || !GameHelper.CardPayment.cardVina)
        {
            for (int i = 0; i < dropProvider.options.Count; i++)
            {
                if (dropProvider.options[i].text == "Vinaphone")
                {
                    dropProvider.options.RemoveAt(i);
                    haveVina = false;
                    break;
                }
            }
        }
        if (!haveMobi)
        {
            dropProvider.captionText.text = "Viettel";
            if (!haveViettel)
            {
                dropProvider.captionText.text = "Vinaphone";
                if (!haveVina)
                {
                    dropProvider.captionText.text = "";
                }
            }

        }
        */




    }













    private bool checkShowItemCard(string cardType)
    {
        if (cardType.ToLower() == "mobifone")
        {
            if (GameHelper.CardPayment.cardMobi) return true;
        }
        else if (cardType.ToLower() == "viettel")
        {
            if (GameHelper.CardPayment.cardVietel) return true;
        }
        else if (cardType.ToLower() == "vinaphone")
        {
            if (GameHelper.CardPayment.cardVina) return true;
        }
        return false;
    }

    bool _isRequestingCard = false;

    void ChargingCardOnClick()
    {
        //		Debug.Log (dropProvider.value);
        //
        Debug.Log("Khuong=PriceProvider=========" + CardPrice);
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        ClosePopupConfrim();

        if (_isRequestingCard)
            return;

        _isRequestingCard = true;

        /*
        int cardPrice = 0;
        cardPrice = int.Parse(CardPrice);

      //  int.TryParse(CardPrice, out cardPrice);
        Debug.Log("Giá 5  = " + cardPrice);
        if (cardPrice < 10000)
        {
            AlertController.api.showAlert("Vui lòng chọn mệnh giá thẻ cào");
            return;
        }
        */
        if (string.IsNullOrEmpty(inputCardPin.text) || string.IsNullOrEmpty(inputCardSerial.text))
        {
            _isRequestingCard = false;
            return;
        }

        string[] provider = lstCardProvider.Split('#');

        Debug.Log("Giá = " + CardPrice);
        Debug.Log("Loai = " + dropProvider.captionText.text.ToLower());
        Debug.Log("Max  = " + inputCardPin.text);
        Debug.Log("Seri  = " + inputCardSerial.text);


        panelProcessingPayment.gameObject.SetActive(true);
        API.Instance.RequestShopCard(
            dropProvider.captionText.text.ToLower(),
            CardPrice,
            inputCardPin.text,
            inputCardSerial.text,
            RspShopCard);
    }


    bool _isRequestingCard_GEM = false;

    void ChargingCardOnClick_GEM()
    {
        //		Debug.Log (dropProvider.value);
        //
        Debug.Log("Khuong=PriceProvider=========" + CardPrice);
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        ClosePopupConfrim();

        if (_isRequestingCard_GEM)
            return;

        _isRequestingCard_GEM = true;

        if (string.IsNullOrEmpty(inputCardPin.text) || string.IsNullOrEmpty(inputCardSerial.text))
        {
            _isRequestingCard_GEM = false;
            return;
        }

        string[] provider = lstCardProvider_GEM.Split('#');

        Debug.Log("Giá = " + CardPrice);
        Debug.Log("Loai = " + dropProvider.captionText.text.ToLower());
        Debug.Log("Max  = " + inputCardPin.text);
        Debug.Log("Seri  = " + inputCardSerial.text);


        panelProcessingPayment.gameObject.SetActive(true);
        API.Instance.RequestShopCard_GEM(
            dropProvider.captionText.text.ToLower(),
            CardPrice,
            inputCardPin.text,
            inputCardSerial.text,
            RspShopCard_GEM);
    }







    [SerializeField]
    private GameObject panelProcessingPayment = null;
    void RspShopCard(string s)
    {
        _isRequestingCard = false;

        Debug.Log("Nap card: " + s);

        JSONNode node = JSONNode.Parse(s);

        GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;
        panelProcessingPayment.gameObject.SetActive(false);

        ShowMessage(node["msg"].Value);
        //		Alert.Show (node ["msg"].Value, () => {
        if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
        {
            Alert.Show(GameHelper.IsUpgradeVip, Alert.Hide);
        }
        //		});

        bool success = node["status"].Value.ToString().Equals("1");
        if (success)
        {
            UpdateChip(
                long.Parse(node["items"]["chip"].Value),
                long.Parse(node["items"]["gold"].Value),
                int.Parse(node["items"]["vippoint"].Value)
            );


            callBackShopCard();
        }


        callBackShopCard();

    }





    void RspShopCard_GEM(string s)
    {
        _isRequestingCard_GEM = false;

        Debug.Log("Nap card: " + s);

        JSONNode node = JSONNode.Parse(s);

        GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;
        panelProcessingPayment.gameObject.SetActive(false);

        ShowMessage(node["msg"].Value);
        //		Alert.Show (node ["msg"].Value, () => {
        if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
        {
            Alert.Show(GameHelper.IsUpgradeVip, Alert.Hide);
        }
        //		});

        bool success = node["status"].Value.ToString().Equals("1");
        if (success)
        {
            /*
            UpdateChip(
                long.Parse(node["items"]["chip"].Value),
                long.Parse(node["items"]["gold"].Value),
                int.Parse(node["items"]["vippoint"].Value)
            );
            */
            UpdateChip_GEM(
                long.Parse(node["items"]["gem"].Value),
                long.Parse(node["items"]["gold"].Value),
                int.Parse(node["items"]["vippoint"].Value)
            );


            callBackShopCard();
        }


        callBackShopCard();

    }

    //	void ShowMessagePurchased(string _msg)
    //	{
    //		StartCoroutine (ShowMessageThread (_msg));
    //	}
    //	IEnumerator ShowMessageThread(string _msg, float time = 3)
    //	{
    //		txtNoti.text = _msg;
    //		txtNoti.transform.parent.gameObject.SetActive (true);
    //		yield return new WaitForSeconds (time);
    //		HideMessage ();
    //	}
    void ShowMessage(string _msg)
    {
        popupPurchased.Show(_msg, HideMessage);
    }
    void HideMessage()
    {
        popupPurchased.Hide();
    }

    #endregion

    #region SMS

    bool isClickedItemSms = false;

    void InitSms(IDictionary _dataSms)
    {
        foreach (Transform trsf in trsfSmsParentItem)
        {
            GameObject.Destroy(trsf.gameObject);
        }
        foreach (Transform trsf in trsfSmsParentProvider)
        {
            GameObject.Destroy(trsf.gameObject);
        }

        HeadLine = int.Parse(GetValue("headline", _dataSms));

        dictProviderData = new Dictionary<string, IDictionary>();
        dictProvider = new Dictionary<string, ItemSmsProviderView>();

        foreach (DictionaryEntry entrySms in _dataSms)
        {
            dictProviderData.Add(entrySms.Key.ToString(), entrySms.Value as IDictionary);
            //			dictProviderData.Add (entrySms.Key.ToString (), MiniJSON.Json.Deserialize (entrySms.Value.ToString ()) as IDictionary);
        }

        foreach (string key in dictProviderData.Keys)
        {
            var dataItems = dictProviderData[key] as IDictionary;

            if (dataItems == null) continue;
            if (!checkShowItemCard(key)) continue;
            GameObject itemProvider = Instantiate(objItemProvider) as GameObject;
            itemProvider.transform.SetParent(trsfSmsParentProvider);
            itemProvider.transform.localScale = Vector3.one;
            // Debug.LogError("KHUONGHAHAHAHA===== key:::: " + key);
            itemProvider.GetComponent<ItemSmsProviderView>().Init(key, ItemSmsProviderOnClick);
            dictProvider.Add(key, itemProvider.GetComponent<ItemSmsProviderView>());
        }

        int index = 1;
        foreach (string key in dictProviderData.Keys)
        {
            foreach (DictionaryEntry entryItem in dictProviderData[key])
            {
                GameObject item = Instantiate(objItemSMS) as GameObject;
                item.transform.SetParent(trsfSmsParentItem);
                item.transform.localScale = Vector3.one;

                item.GetComponent<ItemSmsView>().Init(
                    index,
                    entryItem.Key.ToString(),
                    GetValue("chip", entryItem.Value as IDictionary),
                    GetValue("gold", entryItem.Value as IDictionary),
                    GetValue("sms", entryItem.Value as IDictionary),
                    ItemSmsOnClick);
                index++;
            }
            break;
        }
    }


    // Sms_GEM

    void InitSms_GEM(IDictionary _dataSms)
    {/*
        foreach (Transform trsf in trsfSmsParentItem)
        {
            GameObject.Destroy(trsf.gameObject);
        }
        foreach (Transform trsf in trsfSmsParentProvider)
        {
            GameObject.Destroy(trsf.gameObject);
        }

        HeadLine = int.Parse(GetValue("headline", _dataSms));

        dictProviderData = new Dictionary<string, IDictionary>();
        dictProvider = new Dictionary<string, ItemSmsProviderView>();

        foreach (DictionaryEntry entrySms in _dataSms)
        {
            dictProviderData.Add(entrySms.Key.ToString(), entrySms.Value as IDictionary);
            //			dictProviderData.Add (entrySms.Key.ToString (), MiniJSON.Json.Deserialize (entrySms.Value.ToString ()) as IDictionary);
        }

        foreach (string key in dictProviderData.Keys)
        {
            var dataItems = dictProviderData[key] as IDictionary;

            if (dataItems == null) continue;
            if (!checkShowItemCard(key)) continue;
            GameObject itemProvider = Instantiate(objItemProvider) as GameObject;
            itemProvider.transform.SetParent(trsfSmsParentProvider);
            itemProvider.transform.localScale = Vector3.one;
            // Debug.LogError("KHUONGHAHAHAHA===== key:::: " + key);
            itemProvider.GetComponent<ItemSmsProviderView>().Init(key, ItemSmsProviderOnClick);
            dictProvider.Add(key, itemProvider.GetComponent<ItemSmsProviderView>());
        }

        int index = 1;
        foreach (string key in dictProviderData.Keys)
        {
            foreach (DictionaryEntry entryItem in dictProviderData[key])
            {
                GameObject item = Instantiate(objItemSMS) as GameObject;
                item.transform.SetParent(trsfSmsParentItem);
                item.transform.localScale = Vector3.one;

                item.GetComponent<ItemSmsView>().Init(
                    index,
                    entryItem.Key.ToString(),
                    GetValue("chip", entryItem.Value as IDictionary),
                    GetValue("gold", entryItem.Value as IDictionary),
                    GetValue("sms", entryItem.Value as IDictionary),
                    ItemSmsOnClick);
                index++;
            }
            break;
        }
        */
    }






    void ItemSmsProviderOnClick(string _s)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        CurrentProvider = _s;

        //		if (dictProvider [_s].EnableToggle)
        ToggleOnChange(true);
    }

    void ItemSmsOnClick(string _cost)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        IDictionary dataItems = dictProviderData[CurrentProvider];
        IDictionary dataDetail = GetDictChild(_cost, dataItems);


        string content = GetValue("sms", dataDetail);
        content = content.Replace(" ", "%20");

        Debug.Log(content + " TO " + HeadLine);

        string sContent = content;
        sContent = sContent.Replace("%20", " ") + "\ng?i " + HeadLine.ToString();

        Full.Show(sContent, Full.Hide, () => {

            isClickedItemSms = true;
            Application.OpenURL("sms:" + HeadLine + "?&body=" + content);

        });

    }

    // ------------------------ Tien  ------------------------------------















    // ------------------------ Tien  ------------------------------------

    #endregion

    #region In App

    void InitInApp(IDictionary _dataInApp)
    {
        Debug.Log("init in app");
        lstInAppCost = lstInAppChip = lstInAppGold = lstInAppIdAndroid = lstInAppIdIos = "";

        List<string> ListChipDefaut = new List<string>();



        foreach (Transform trsf in trsfInAppParent)
        {
            GameObject.Destroy(trsf.gameObject);
        }

        foreach (DictionaryEntry entryItemInApp in _dataInApp)
        {

            var dataItemChip = entryItemInApp.Value as IDictionary;

            lstInAppCost += (float.Parse(entryItemInApp.Key.ToString()) - 0.01f).ToString() + "#";

            foreach (DictionaryEntry entryItemChip in dataItemChip)
            {

                if (entryItemChip.Key.ToString().Equals("chip"))
                {
                    ListChipDefaut.Add(entryItemChip.Value.ToString());
                    lstInAppChip += Utilities.GetStringMoneyByLong(long.Parse(entryItemChip.Value.ToString())) + "#";
                }
                else if (entryItemChip.Key.ToString().Equals("gold"))
                {
                    lstInAppGold += Utilities.GetStringMoneyByLong(long.Parse(entryItemChip.Value.ToString())) + "#";
                }
                else if (entryItemChip.Key.ToString().Equals("android"))
                {
                    //					#if UNITY_ANDROID
                    lstInAppIdAndroid += entryItemChip.Value + "#";

                    //					#endif
                }
                else if (entryItemChip.Key.ToString().Equals("ios"))
                {
                    //					#if UNITY_IOS
                    lstInAppIdIos += entryItemChip.Value + "#";
                    //					#endif
                }
            }
        }

        lstInAppCost = lstInAppCost.Remove(lstInAppCost.Length - 1);
        lstInAppChip = lstInAppChip.Remove(lstInAppChip.Length - 1);
        lstInAppGold = lstInAppGold.Remove(lstInAppGold.Length - 1);

        lstInAppIdAndroid = lstInAppIdAndroid.Remove(lstInAppIdAndroid.Length - 1);
        lstInAppIdIos = lstInAppIdIos.Remove(lstInAppIdIos.Length - 1);

        string[] valueChip = lstInAppChip.Split('#');
        string[] valueGold = lstInAppGold.Split('#');
        string[] valueCost = lstInAppCost.Split('#');
        string[] valueIdAndroid = lstInAppIdAndroid.Split('#');
        string[] valueIdIos = lstInAppIdIos.Split('#');

        //Debug.LogError(lstInAppIdAndroid);
        //UM_InAppPurchaseManager.Instance.InAppProducts.Clear();

#if UNITY_ANDROID


        int My_Id_Vip = MyInfo.MY_ID_VIP;
        long TyLePhanTram = 0;
        if (My_Id_Vip > 0)
        {
            GoiVip gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
         //   Debug.LogWarning(" TyLePhanTram ----------- " + gv.ThuongPhanTram);
            TyLePhanTram = long.Parse(gv.ThuongPhanTram);
/*
            My_LoaiVip.text = "VIP " + My_Id_Vip.ToString();
            My_Phantram.text = gv.ThuongPhanTram + "%";
            TitleVIP.SetActive(true);*/
        }


        for (int i = 0; i < valueIdAndroid.Length; i++)
        {
            if (i < lstCardChip.Length && i < lstCardCost.Length)
            {

                GameObject item = Instantiate(objItemInApp) as GameObject;
                item.transform.SetParent(trsfInAppParent);
                item.transform.localScale = Vector3.one;

                string VipChip = "";


                if (My_Id_Vip > 0)
                {
                    string t = ListChipDefaut[i];
                    long TienTong = long.Parse(t);
                    string txt = ChipThuongPhanTram(TyLePhanTram, TienTong);

                    VipChip = txt;
                }



                item.GetComponent<ItemInAppView>().Init(i + 1, valueCost[i], valueChip[i], valueGold[i], valueIdAndroid[i], ItemInAppOnClick, VipChip);

                /*
                UM_InAppProduct p;

                p = new UM_InAppProduct();
                p.id = valueIdAndroid[i];
                p.IOSId = valueIdAndroid[i];
                p.AndroidId = valueIdAndroid[i];
                p.IsConsumable = true;

                UM_InAppPurchaseManager.Instance.InAppProducts.Add(p);*/

            }
        }

#else
        int My_Id_Vip = MyInfo.MY_ID_VIP;
        long TyLePhanTram = 0;
        if (My_Id_Vip > 0)
        {
            GoiVip gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
            //   Debug.LogWarning(" TyLePhanTram ----------- " + gv.ThuongPhanTram);
            TyLePhanTram = long.Parse(gv.ThuongPhanTram);
            /*
                        My_LoaiVip.text = "VIP " + My_Id_Vip.ToString();
                        My_Phantram.text = gv.ThuongPhanTram + "%";
                        TitleVIP.SetActive(true);*/
        }

        for (int i = 0; i < valueIdIos.Length; i++)
        {
			if (i < lstCardChip.Length && i < lstCardCost.Length)
            {

				GameObject item = Instantiate (objItemInApp) as GameObject;
				item.transform.SetParent (trsfInAppParent);
				item.transform.localScale = Vector3.one;


                string VipChip = "";


                if (My_Id_Vip > 0)
                {
                    string t = ListChipDefaut[i];
                    long TienTong = long.Parse(t);
                    string txt = ChipThuongPhanTram(TyLePhanTram, TienTong);

                    VipChip = txt;
                }


                item.GetComponent<ItemInAppView> ().Init (i + 1, valueCost [i], valueChip [i], valueGold [i], valueIdIos [i], ItemInAppOnClick, VipChip);
        /*
				UM_InAppProduct p;

				p = new UM_InAppProduct ();
				p.id = valueIdIos [i];
				p.IOSId = valueIdIos [i];
				p.AndroidId = valueIdIos [i];
				p.IsConsumable = true;

				UM_InAppPurchaseManager.Instance.InAppProducts.Add (p);*/
			}
		}
#endif
        //UM_InAppPurchaseManager.Instance.Init();
    }

    //INAPP GEM

    void InitInApp_GEM(IDictionary _dataInApp)
    {
        Debug.Log("init in app GEM");
        lstInAppCost_GEM = lstInAppGem_GEM = lstInAppIdAndroid_GEM = lstInAppIdIos_GEM = "";

        foreach (Transform trsf in trsfInAppParent_GEM)
        {
            GameObject.Destroy(trsf.gameObject);
        }

        foreach (DictionaryEntry entryItemInApp in _dataInApp)
        {

            var dataItemChip = entryItemInApp.Value as IDictionary;

            lstInAppCost_GEM += (float.Parse(entryItemInApp.Key.ToString()) - 0.01f).ToString() + "#";

            foreach (DictionaryEntry entryItemChip in dataItemChip)
            {

                if (entryItemChip.Key.ToString().Equals("gem"))
                {
                    lstInAppGem_GEM += Utilities.GetStringMoneyByLong(long.Parse(entryItemChip.Value.ToString())) + "#";
                }
                else if (entryItemChip.Key.ToString().Equals("android"))
                {
                    //					#if UNITY_ANDROID
                    lstInAppIdAndroid_GEM += entryItemChip.Value + "#";

                    //					#endif
                }
                else if (entryItemChip.Key.ToString().Equals("ios"))
                {
                    //					#if UNITY_IOS
                    lstInAppIdIos_GEM += entryItemChip.Value + "#";
                    //					#endif
                }
            }
        }

        lstInAppCost_GEM = lstInAppCost_GEM.Remove(lstInAppCost_GEM.Length - 1);
        lstInAppGem_GEM = lstInAppGem_GEM.Remove(lstInAppGem_GEM.Length - 1);
        // lstInAppGold = lstInAppGold.Remove(lstInAppGold.Length - 1);

        lstInAppIdAndroid_GEM = lstInAppIdAndroid_GEM.Remove(lstInAppIdAndroid_GEM.Length - 1);
        lstInAppIdIos_GEM = lstInAppIdIos_GEM.Remove(lstInAppIdIos_GEM.Length - 1);

        string[] valueGem = lstInAppGem_GEM.Split('#');
        //string[] valueGold = lstInAppGold_GEM.Split('#');
        string[] valueCost = lstInAppCost_GEM.Split('#');
        string[] valueIdAndroid = lstInAppIdAndroid_GEM.Split('#');
        string[] valueIdIos = lstInAppIdIos_GEM.Split('#');

        //Debug.LogError(lstInAppIdAndroid);
        //      UM_InAppPurchaseManager.Instance.InAppProducts.Clear();

#if UNITY_ANDROID
        for (int i = 0; i < valueIdAndroid.Length; i++)
        {
            if (i < lstCardChip.Length && i < lstCardCost.Length)
            {

                GameObject item = Instantiate(objItemInApp_GEM) as GameObject;
                item.transform.SetParent(trsfInAppParent_GEM);
                item.transform.localScale = Vector3.one;

                //       Debug.Log("444444444444444444444444444444444444444444444444444444444444444"+ i );

                item.GetComponent<ItemInApp_GEM_View>().Init(i, valueCost[i], valueGem[i], valueIdAndroid[i], ItemInAppGemOnClick);

/*
                UM_InAppProduct p;

                p = new UM_InAppProduct();
                p.id = valueIdAndroid[i];
                p.IOSId = valueIdAndroid[i];
                p.AndroidId = valueIdAndroid[i];
                p.IsConsumable = true;

                UM_InAppPurchaseManager.Instance.InAppProducts.Add(p);
                */
            }
        }

#else
		 for (int i = 0; i < valueIdAndroid.Length; i++)
        {
            if (i < lstCardChip.Length && i < lstCardCost.Length)
            {

                GameObject item = Instantiate(objItemInApp_GEM) as GameObject;
                item.transform.SetParent(trsfInAppParent_GEM);
                item.transform.localScale = Vector3.one;

                //       Debug.Log("444444444444444444444444444444444444444444444444444444444444444"+ i );

                item.GetComponent<ItemInApp_GEM_View>().Init(i, valueCost[i], valueGem[i], valueIdAndroid[i], ItemInAppGemOnClick);

        /*
                UM_InAppProduct p;

                p = new UM_InAppProduct();
                p.id = valueIdAndroid[i];
                p.IOSId = valueIdAndroid[i];
                p.AndroidId = valueIdAndroid[i];
                p.IsConsumable = true;

                UM_InAppPurchaseManager.Instance.InAppProducts.Add(p);
        */
            }
        }
#endif
        //UM_InAppPurchaseManager.Instance.Init();
    }




    void ItemInAppOnClick(string _productId)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Debug.Log("_productId-------------------------------------" + _productId);
        //UM_InAppPurchaseManager.Instance.Purchase(_productId);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(_productId, OnPuchased);
    }
    void ItemInAppGemOnClick(string _productId)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Debug.Log("_productId-------------------------------------" + _productId);
        //UM_InAppPurchaseManager.Instance.Purchase(_productId);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(_productId, OnPuchased_GEM);
    }
    [SerializeField]
    Text txtTest;

    void OnPuchased(string productId, string purchaseToken, bool isSuccess)
    {
        
        if (isSuccess)
        {

            //			UM_InAppPurchaseManager.Instance.GetProductById("").

            //if (_result.purchasedProduct != null)
                //Debug.Log(_result.purchasedProduct);
            //else if (_result.IOS_PurchaseInfo != null)
                //Debug.Log(_result.IOS_PurchaseInfo);
            string s = "";

#if UNITY_ANDROID
            //JSONNode node = JSONNode.Parse(_result.purchasedProduct.receipt);

            string store = "googleplay";

            string productID = productId;//node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;

            API.Instance.RequestPaymentInAppAndroid(store, productID, token, RspPaymentInApp);

#elif UNITY_IOS

//			JSONNode node = JSONNode.Parse(_result.IOS_PurchaseInfo.Receipt);
//
			string store = "appstore";
			string receipt = _result.IOS_PurchaseInfo.Receipt;

//			s += "TOKEN: " + token
//			+ "\n packageName " + productID + "\n";

//			s+= "AppUname: " + _result.IOS_PurchaseInfo.ApplicationUsername
//				+"\nProductID: " + _result.IOS_PurchaseInfo.ProductIdentifier
//				+"\nReceipt: " + _result.IOS_PurchaseInfo.Receipt
//				+"\nTransactionId: " + _result.IOS_PurchaseInfo.TransactionIdentifier
//				;

			API.Instance.RequestPaymentInAppIOS(store, receipt, RspPaymentInApp);

#endif

            //			Debug.Log (s);

            //			API.Instance.RequestPaymentInApp (store, productID, token, RspPaymentInApp);

            //			txtTest.text = s;
            //			Debug.Log (s);


            //			StartCoroutine (ShowMessageThread ("+" + _result.product.Description));

        }
        else
        {
			Alert.Show("Giao dịch không thành công.", () => {
                if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
                {
                    ShowPopupUpgradeVippoint();
                }
                else
                    Alert.Hide();
            });
        }
    }




    void OnPuchased_GEM(string productId, string purchaseToken, bool isSuccess)
    {
        
        if (isSuccess)
        {

            //			UM_InAppPurchaseManager.Instance.GetProductById("").

            //if (_result.Google_PurchaseInfo != null)
            //    Debug.Log(_result.Google_PurchaseInfo);
            //else if (_result.IOS_PurchaseInfo != null)
            //    Debug.Log(_result.IOS_PurchaseInfo);
            string s = "";

#if UNITY_ANDROID
            //JSONNode node = JSONNode.Parse(_result.Google_PurchaseInfo.originalJson);

            string store = "googleplay";

            string productID = productId;//node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;

            API.Instance.RequestPaymentInAppAndroid_GEM(store, productID, token, RspPaymentInApp_GEM);

#elif UNITY_IOS

//			JSONNode node = JSONNode.Parse(_result.IOS_PurchaseInfo.Receipt);
//
			string store = "appstore";
			string receipt = _result.IOS_PurchaseInfo.Receipt;

//			s += "TOKEN: " + token
//			+ "\n packageName " + productID + "\n";

//			s+= "AppUname: " + _result.IOS_PurchaseInfo.ApplicationUsername
//				+"\nProductID: " + _result.IOS_PurchaseInfo.ProductIdentifier
//				+"\nReceipt: " + _result.IOS_PurchaseInfo.Receipt
//				+"\nTransactionId: " + _result.IOS_PurchaseInfo.TransactionIdentifier
//				;

			API.Instance.RequestPaymentInAppIOS(store, receipt, RspPaymentInApp);

#endif

            //			Debug.Log (s);

            //			API.Instance.RequestPaymentInApp (store, productID, token, RspPaymentInApp);

            //			txtTest.text = s;
            //			Debug.Log (s);


            //			StartCoroutine (ShowMessageThread ("+" + _result.product.Description));

        }
        else
        {
			Alert.Show("Giao dịch không thành công.", () => {
                if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
                {
                    ShowPopupUpgradeVippoint();
                }
                else
                    Alert.Hide();
            });
        }
    }





    void RspPaymentInApp(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" RspPaymentInApp " + _json);
        if (node["status"].AsInt == 1)
        {
            MyInfo.CHIP = long.Parse(node["items"]["items"]["chip"].Value);
            MyInfo.GOLD = long.Parse(node["items"]["items"]["gold"].Value);
            MyInfo.VIPPOINT = int.Parse(node["items"]["items"]["vippoint"].Value);

            callBackShopCard();

            GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;

            Alert.Show(node["msg"].Value, () => {
                if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
                {
                    ShowPopupUpgradeVippoint();
                }
                else
                    Alert.Hide();
            });

        }
        else if (node["status"].AsInt == 0)
        {
            Alert.Show(node["sms"], Alert.Hide);
        }

    }

    void RspPaymentInApp_GEM(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" RspPaymentInApp_GEM " + _json);
        if (node["status"].AsInt == 1)
        {
            MyInfo.GEM = long.Parse(node["items"]["items"]["gem"].Value);
            MyInfo.GOLD = long.Parse(node["items"]["items"]["gold"].Value);
            MyInfo.VIPPOINT = int.Parse(node["items"]["items"]["vippoint"].Value);

            callBackShopCard();

            GameHelper.IsUpgradeVip = node["msg_upgrade_vip"].Value;

            Alert.Show(node["msg"].Value, () => {
                if (!string.IsNullOrEmpty(GameHelper.IsUpgradeVip))
                {
                    ShowPopupUpgradeVippoint();
                }
                else
                    Alert.Hide();
            });

        }
        else if (node["status"].AsInt == 0)
        {
            Alert.Show(node["sms"], Alert.Hide);
        }

    }







    #endregion

    #region UPDATE CHIP

    void UpdateChip(long _chip, long _gold, int _vippoint)
    {
        MyInfo.CHIP = _chip;
        MyInfo.GOLD = _gold;
        MyInfo.VIPPOINT = _vippoint;
    }


    void UpdateChip_GEM(long _gem, long _gold, int _vippoint)
    {
        MyInfo.GEM = _gem;
        MyInfo.GOLD = _gold;
        MyInfo.VIPPOINT = _vippoint;
    }



    #endregion

    void ShowPopupUpgradeVippoint()
    {
        string msg = GameHelper.IsUpgradeVip;

        if (!string.IsNullOrEmpty(msg))
        {
            Alert.Show(msg, () => {
                Alert.Hide();
                GameHelper.IsUpgradeVip = "";
            });

        }
    }

    void ToggleOnChange(bool _value)
    {
        if (!_value)
            return;
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        SetEnablePanelContent(toggleCard.isOn, toggleIAP.isOn, toggleSMS.isOn);
    }



    void ToggleOnChange_GEM(bool _value)
    {
        if (!_value)
            return;
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        SetEnablePanelContent_GEM(toggleCard_GEM.isOn, toggleIAP_GEM.isOn, toggleSMS_GEM.isOn);
    }




    void SetEnablePanelContent(bool _isCard, bool _isIAP, bool _isSMS)
    {
        panelCard.SetActive(_isCard);
        panelIAP.SetActive(_isIAP);
        panelSMS.SetActive(_isSMS);

        if (_isSMS)
        {
            //First, Open SMS
            //Default open first tap

            if (string.IsNullOrEmpty(CurrentProvider))
            {
                foreach (string key in dictProvider.Keys)
                {
                    //Neu provider null => gan' provider dau tien
                    //Open Content 
                    CurrentProvider = key;

                    break;
                }
            }

            //			Debug.Log ("crrProvider: " + CurrentProvider);
            dictProvider[CurrentProvider].EnableToggle = true;
        }
        
    }

    void SetEnablePanelContent_GEM(bool _isCard, bool _isIAP, bool _isSMS)
    {
        if (MyInfo.IS_APK_PURE)
        {
            _isIAP = false;
            _isCard = true;
        }
        
        panelCard_GEM.SetActive(_isCard);
        panelIAP_GEM.SetActive(_isIAP);
        panelSMS_GEM.SetActive(_isSMS);

        if (_isSMS)
        {
            //First, Open SMS
            //Default open first tap

            if (string.IsNullOrEmpty(CurrentProvider))
            {
                foreach (string key in dictProvider.Keys)
                {
                    //Neu provider null => gan' provider dau tien
                    //Open Content 
                    CurrentProvider = key;

                    break;
                }
            }

            //			Debug.Log ("crrProvider: " + CurrentProvider);
            dictProvider[CurrentProvider].EnableToggle = true;
        }
    }











    string GetValue(string _key, IDictionary _data)
    {
        foreach (DictionaryEntry entry in _data)
        {
            //			Debug.Log ("in: " + entry.Key);
            if (entry.Key.ToString().Equals(_key))
                return entry.Value.ToString();
        }
        return "";
    }
    string GetValueChild(string _keyChild, DictionaryEntry _entry)
    {
        var data = _entry.Value as IDictionary;
        foreach (DictionaryEntry entry in data)
        {
            if (entry.Key.ToString().Equals(_keyChild))
                return entry.Value.ToString();
        }
        return "";
    }
    IDictionary GetDictChild(string _key, IDictionary _data)
    {
        foreach (DictionaryEntry entry in _data)
        {
            if (entry.Key.ToString().Equals(_key))
                return entry.Value as IDictionary;
        }
        return null;
    }

    void OnApplicationPause(bool _resume)
    {
        //		if (_resume) {
        //			if (isClickedItemSms) {
        //				SFS.Instance.SendZoneRefreshData ();
        //			}
        //		}
        //		isClickedItemSms = false;
    }
}

public class ShopInfo
{
    public static Dictionary<long, long> dictSMS; //<cost, chip>
}