﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class PopupCreateRoom : MonoBehaviour {

    [SerializeField]
    private GameObject panel;
    [SerializeField]
    Slider sliderbet;
    [SerializeField]
    Text txtBetCurrent;
    [SerializeField]
    InputField inputPassword;
    [SerializeField]
    Button btnCancel, btnOK;

    [SerializeField]
    Text txtError;

    onCallBack cancelOnClick;
    onCallBackStringInt okOnClick;
    onCallBackInt valueBetChange;


    public bool IsEnable;

    public void Init(onCallBack _cancel, onCallBackStringInt _ok, onCallBackInt _betChange)
    {
        IsEnable = false;
        btnCancel.onClick.AddListener(CancelOnClick);
        btnOK.onClick.AddListener(OkOnClick);
        sliderbet.onValueChanged.AddListener(OnBetChange);

        cancelOnClick = _cancel;
        okOnClick = _ok;
        valueBetChange = _betChange;
    }

    public void Show()
    {
        IsEnable = true;
        panel.SetActive(true);
        sliderbet.value = 0;
        inputPassword.text = "";
    }
    public void ShowError(string _text)
	{
		txtError.text = _text;
	}
    public void Hide()
    {
        IsEnable = false;
        panel.SetActive(false);
    }
    public void ShowBetCurrent(string _text)
    {
        txtBetCurrent.text = _text;
    }

    void CancelOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        cancelOnClick();
    }
    void OkOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        okOnClick(inputPassword.text, (int)sliderbet.value);
    }
    void OnBetChange(float _value)
    {
        valueBetChange((int)_value);
    }
}
