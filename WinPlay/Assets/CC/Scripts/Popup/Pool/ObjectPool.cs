﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool{

    public List<GameObject> listPool, lstAllPreferent;
    private Transform layerContainer;
    private GameObject prefab;
    private int numPool;
    public ObjectPool(int numPool,Transform layerContainer, GameObject prefab,bool isSetParent = true)
    {
        this.numPool = numPool;
        this.layerContainer = layerContainer;
        this.prefab = prefab;
        listPool = new List<GameObject>();
        lstAllPreferent = new List<GameObject>();
        for (int i = 0; i < this.numPool; i++)
        {
            createItem(isSetParent);
            lstAllPreferent.Add(listPool[i]);
        }
    }

    private GameObject createItem(bool isSetParent)
    {
        Transform item = GameObject.Instantiate(prefab).transform;
        item.SetParent(layerContainer);
        item.localPosition = Vector3.one; //* 5000;
        item.localScale = Vector3.one;
        if (isSetParent == true)
        {
            item.gameObject.SetActive(false);
        }
        listPool.Add(item.gameObject);
        return item.gameObject;
    }

    public void AddPool(GameObject item,bool isSetParent = true)
    {
        if(listPool == null)
        {
            listPool = new List<GameObject>();
        }
        if (isSetParent == true)
        {
            item.SetActive(false);
        }
        item.transform.localPosition = Vector2.one; //* 5000;
        item.transform.SetParent(layerContainer);
        listPool.Add(item);
    }

    public GameObject GetPool(bool isSetParent = true)
    {
        if (listPool == null)
        {
            listPool = new List<GameObject>();
        }
        GameObject item = null;
        if (listPool.Count <= 0)
        {
            item = createItem(isSetParent);
        }
        item = listPool[0];

        if (isSetParent == true)
        {
            item.SetActive(true);
        }
        listPool.RemoveAt(0);
        return item;
    }
}
