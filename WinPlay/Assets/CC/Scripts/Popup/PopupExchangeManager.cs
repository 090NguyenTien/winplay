﻿using UnityEngine;
using System.Collections;
using BaseCallBack;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;

public class PopupExchangeManager : MonoBehaviour {


	[SerializeField]
	GameObject panel, panelChip, panelCard;
	[SerializeField]
	Button btnBack;
	[SerializeField]
	Button btnRule;
	[SerializeField]
	Toggle toggleCard, toggleChip;
	[SerializeField]
	Text txtNameUser, txtChipUser, txtGoldUser;
	[SerializeField]
	Image imgAvatar, imgBorder;

	bool isInited = false;
	static string CurrentProvider = "";
	[SerializeField]
	Text txtNoti;

	[SerializeField]
	Sprite sprViettel, sprMobifone, sprVinaphone;


	static bool isRequested = false;

	static string jsonInit = "";

	onCallBack backClick;
	onCallBackString callBackExchanged;

	long CHIP { get { return MyInfo.CHIP; } }
	long GOLD { get { return MyInfo.GOLD; } }
	int VIPPOINT { get { return MyInfo.VIPPOINT; } }
	string AvatarNAME { get { return MyInfo.AvatarName; } }

	public void Init(onCallBackString _onExchanged, onCallBack _onBack)
	{
		btnBack.onClick.AddListener (BtnBackOnClick);

		toggleCard.onValueChanged.AddListener (ToggleOnChange);
		toggleChip.onValueChanged.AddListener (ToggleOnChange);

		
		callBackExchanged = _onExchanged;
		backClick = _onBack;

		isClickedItemSms = false;


		txtNameUser.text = MyInfo.NAME;

		popupFull.Init ();

		if (isRequested) {
			return;
		}
//		Debug.Log(" +===++++ INIT EXCHANGE ++++===+ ");
		RequestAPI ();

	}

	public void InitHideCard(){
		toggleCard.gameObject.SetActive (false);
		toggleChip.isOn = true;

		panelCard.SetActive (false);
		panelChip.SetActive (true);
	}

	void BtnBackOnClick ()
	{
		backClick ();
	}

	//API Request Response
	void RequestAPI()
	{
		Debug.Log ("Request EXCHANGE!!!");
		API.Instance.RequestExchangeData (ResponseAPI);
	}
	public void ResponseAPI(){
		ResponseAPI (jsonInit);
	}
	void ResponseAPI(string _json)
	{
		Debug.Log ("JSON EXCHANGE:\n" + _json);
		jsonInit = _json;

		JSONNode nodeCard = JSONNode.Parse (_json) ["gold_to_card"];
		JSONNode nodeProvider = nodeCard ["provider"];

		JSONNode nodeCardValueByVipLevel = JSONNode.Parse (_json) ["vip_exchange_card"];

		enableCard = nodeProvider.Count > 0;

		if (enableCard) {

			lstCardProvider = new List<string> ();
			//Luu ds Nha mang
			for (int i = 0; i < nodeProvider.Count; i++) {
//				Debug.Log ("Add provider: " + nodeProvider [i].Value);
				lstCardProvider.Add (nodeProvider [i].Value);

			}

			string jsonRate = nodeCard ["rate"].ToString ();

			var dataRate = MiniJSON.Json.Deserialize (jsonRate) as IDictionary;


			lstCardValue = new List<int> ();
			lstCardGold = new List<int> ();
			lstCardValueByVipLevel = new List<int> ();

			int count = 0;
			foreach (DictionaryEntry entry in dataRate) {

//				if (nodeVippoint [count].AsInt <= VIPPOINT) {
				lstCardValue.Add (int.Parse (entry.Key.ToString ()));

				lstCardGold.Add (int.Parse (entry.Value.ToString ()));

				lstCardValueByVipLevel.Add (nodeCardValueByVipLevel [count].AsInt);

//				Debug.Log ("node[" + count.ToString () + "] = " + nodeCardValueByVipLevel [count].Value);
//				Debug.Log ("lst[" + count.ToString () + "] = " + lstVippointLevelRequire [count]);
//				}
				count++;
			}
		}

		JSONNode nodeChip = JSONNode.Parse (_json) ["gold_to_chip"];

		var dataChip = MiniJSON.Json.Deserialize (nodeChip.ToString()) as IDictionary;

		lstChipValue = new List<int> ();
		lstChipGold = new List<int> ();
		foreach (DictionaryEntry entry in dataChip) {
			if (entry.Key == null) {
				Debug.Log("entry NULL");
				continue;
			}
			lstChipGold.Add (int.Parse (entry.Key.ToString ()));
			lstChipValue.Add (int.Parse (entry.Value.ToString ()));
		}

		isRequested = true;

	}

	public void Show()
	{

		if (!isRequested && !GameHelper.IsChangeVippoint)
			return;

		if (!isInited || GameHelper.IsChangeVippoint) {
			InitCard ();
			InitChip ();
			isInited = true;

//			if (lstProvider.Count > 0) {
			if (true) {
				CurrentProvider = lstCardProvider [0];
				lstProvider [0].EnableToggle = true;

				toggleCard.isOn = true;
			}

			GameHelper.IsChangeVippoint = false;
		}

		ShowUserInfo (CHIP,
			GOLD, VIPPOINT, AvatarNAME);

		panel.SetActive (true);

		toggleCard.isOn = true;

		if (!GameHelper.EnableExchange)
			InitHideCard ();

	}

	public void Hide()
	{
		panel.SetActive (false);
	}

	#region CARD

	static List<string> lstCardProvider;
	static List<int> lstCardValue, lstCardGold, lstChipValue, lstChipGold;
	static List<int> lstCardValueByVipLevel;

	List<ItemCardProviderExchangeView> lstProvider;
	List<ItemCardValueExchangeView> lstItemCard;

	[SerializeField]
	GameObject objItemProvider;
	[SerializeField]
	GameObject objItemCard;

	#endregion

	#region CHIP

	[SerializeField]
	GameObject objItemChip;



	[SerializeField]
	Transform trsfChipParent, trsfCardParentProvider, trsfCardParentItem;

	#endregion

	bool enableChip, enableCard;

	bool EnableChip {
		get { return enableChip; }
	}
	bool EnableCard{
		get{ return enableCard; }
	}


	void ShowMessagePurchased(string _msg)
	{
		StartCoroutine (ShowMessageThread (_msg));
	}
	IEnumerator ShowMessageThread(string _msg, float time = 3)
	{
		txtNoti.text = _msg;
		txtNoti.transform.parent.gameObject.SetActive (true);
		yield return new WaitForSeconds (time);
		txtNoti.transform.parent.gameObject.SetActive (false);
	}


	#region EXCHANGE CARD

	bool isClickedItemSms = false;

	void InitCard()
	{
		//Xoa het item Card
		foreach (Transform trsf in trsfCardParentItem) {
			GameObject.Destroy (trsf.gameObject);
		}
		//Xoa het item NhaMang
		foreach (Transform trsf in trsfCardParentProvider) {
			GameObject.Destroy (trsf.gameObject);
		}
			
		lstProvider = new List<ItemCardProviderExchangeView> ();
		lstItemCard = new List<ItemCardValueExchangeView> ();

		//Khoi tao ra cac item NhaMang
		for (int i = 0; i < lstCardProvider.Count; i++) {
			
			GameObject itemProvider = Instantiate (objItemProvider) as GameObject;
			itemProvider.transform.SetParent (trsfCardParentProvider);
			itemProvider.transform.localScale = Vector3.one;

			lstProvider.Add (itemProvider.GetComponent<ItemCardProviderExchangeView> ());
			lstProvider[i]
				.Init (i, lstCardProvider[i], ItemCardProviderOnClick);

		}

		//Khoi tao ra cac item the cao
		for (int j = 0; j < lstCardValue.Count; j++) {
			GameObject itemCard = Instantiate (objItemCard) as GameObject;
			itemCard.transform.SetParent (trsfCardParentItem);
			itemCard.transform.localScale = Vector3.one;

			lstItemCard.Add (itemCard.GetComponent<ItemCardValueExchangeView> ());
			lstItemCard [j]
				.Init (j, lstCardValue [j], lstCardGold [j], GetSpriteProvider (CurrentProvider), ItemCardOnClick);

			int crrVipLevel = GameHelper.GetVipLevel (MyInfo.VIPPOINT);

//			Debug.Log ("Index : " + j);
//			Debug.Log ("Card: " + lstCardValue [j]);
//			Debug.Log ("CardLevel: " + lstCardValueByVipLevel [crrVipLevel - 1]);

			//JSON service không hợp lý
			// => Client tính bị phức tạp.
			// => đổi cấu trúc sẽ hợp lý hơn.

			if (crrVipLevel > 0) {
			
				if (lstCardValue [j] <= lstCardValueByVipLevel [crrVipLevel - 1]) {
					lstItemCard [j].HideLock ();
				} else {

					lstItemCard [j].ShowLock (j + 1);

					for (int k = 0; k < lstCardValueByVipLevel.Count; k++) {

						if (lstCardValueByVipLevel [k] >= lstCardValue [j]) {
							lstItemCard [j].ShowLock (k + 1);
//							Debug.Log ("++++++ INDEX - " + k + " +++++++");
//							Debug.Log ("++++++ " + lstCardValueByVipLevel [k] + " > " + lstCardValue [j] + " +++++++");
							break;
						}
					}
				}

			} else {
				for (int k = 0; k < lstCardValueByVipLevel.Count; k++) {

					if (lstCardValueByVipLevel [k] >= lstCardValue [j]) {
						lstItemCard [j].ShowLock (k + 1);
						//							Debug.Log ("++++++ INDEX - " + k + " +++++++");
						//							Debug.Log ("++++++ " + lstCardValueByVipLevel [k] + " > " + lstCardValue [j] + " +++++++");
						break;
					}
				}
			}
		

		}
	}

	void ItemCardProviderOnClick (int _index)
	{

		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		CurrentProvider = lstCardProvider[_index];
		ShowSpriteItemCard ();
	}
	[SerializeField]
	PopupFullManager popupFull;
	int CurrentValueCard;

	bool isRequestCard = false;

	void ItemCardOnClick (int _index)
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		CurrentValueCard = lstCardValue [_index];

		if (isRequestCard)
			return;
		
		isRequestCard = true;

		popupFull.Show ("Dùng " + Utilities.GetStringMoneyByLong(lstCardGold[_index]) + " Gold đổi thẻ cào " + Utilities.GetStringMoneyByLong(lstCardValue[_index]) + "." , 
			()=>{
				popupFull.Hide();
				isRequestCard = false;
			}, 
			RequestExchangeCard);
	}

	void RequestExchangeCard(){
		API.Instance.RequestExchangeCard (CurrentProvider, CurrentValueCard, RspExchangeCard);
	}

	void RspExchangeCard (string _json)
	{
		popupFull.Hide ();

		JSONNode node = JSONNode.Parse (_json);


		if (string.IsNullOrEmpty (node ["code"].Value)) {
			UpdateChip (long.Parse (node ["items"] ["chip"].Value), 
				long.Parse (node ["items"] ["gold"].Value),
				node ["items"] ["vippoint"].AsInt
			);

			callBackExchanged (node ["message"].Value);

		} else {

			ShowMessagePurchased (node ["message"].Value);

		}

		isRequestCard = false;
	}


	void ShowSpriteItemCard(){
		for (int i = 0; i < lstItemCard.Count; i++) {
			lstItemCard [i].ShowSpriteProvider (GetSpriteProvider (CurrentProvider));
		}
	}

	#endregion

	#region CHIP

	void InitChip()
	{
		//Xoa het item Chip
		foreach (Transform trsf in trsfChipParent) {
			GameObject.Destroy (trsf.gameObject);
		}


		//Khoi tao ra cac item NhaMang
		for (int i = 0; i < lstChipGold.Count; i++) {

			GameObject itemProvider = Instantiate (objItemChip) as GameObject;
			itemProvider.transform.SetParent (trsfChipParent);
			itemProvider.transform.localScale = Vector3.one;

			itemProvider.GetComponent<ItemChipExchangeView> ()
				.Init (i, lstChipValue [i], lstChipGold [i], ItemChipOnClick);

		}
	}

	bool isRequestChip = false;

	void ItemChipOnClick (int _index)
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		if (isRequestChip)
			return;

		isRequestChip = true;

		popupFull.Show ("Dùng " + Utilities.GetStringMoneyByLong(lstChipGold[_index]) + " Gold đổi " + Utilities.GetStringMoneyByLong(lstChipValue[_index]) + " chip.", () => {
			popupFull.Hide();
			isRequestChip = false;
		}, () => {

			API.Instance.RequestExchangeChip (lstChipGold [_index], RspExchangeChip);

		});
	}

	void RspExchangeChip (string _json)
	{
		isRequestChip = false;
		popupFull.Hide ();

		JSONNode node = JSONNode.Parse (_json);


		if (string.IsNullOrEmpty (node ["code"].Value)) {
			UpdateChip (long.Parse (node ["items"] ["chip"].Value), 
				long.Parse (node ["items"] ["gold"].Value),
				node ["items"] ["vippoint"].AsInt
			);

			callBackExchanged (node ["message"].Value);
		} else {
			ShowMessagePurchased (node ["message"].Value);

		}
	}
		

	#endregion

	#region UPDATE USER INFO

	void UpdateChip(long _chip, long _gold)
	{
		MyInfo.CHIP = _chip;
		MyInfo.GOLD = _gold;

		txtChipUser.text = Utilities.GetStringMoneyByLong (_chip);
		txtGoldUser.text = Utilities.GetStringMoneyByLong (_gold);

		SFS.Instance.SendZoneRefreshData ();
	}
	void UpdateChip(long _chip, long _gold, int _vipPoint)
	{
		MyInfo.CHIP = _chip;
		MyInfo.GOLD = _gold;
		MyInfo.VIPPOINT = _vipPoint;

		txtChipUser.text = Utilities.GetStringMoneyByLong (_chip);
		txtGoldUser.text = Utilities.GetStringMoneyByLong (_gold);

		imgBorder.sprite = DataHelper.GetVip (_vipPoint);

		SFS.Instance.SendZoneRefreshData ();
	}
	void ShowUserInfo(long _chip, long _gold, int _vipPoint, string _avtname)
	{

		txtChipUser.text = Utilities.GetStringMoneyByLong (_chip);
		txtGoldUser.text = Utilities.GetStringMoneyByLong (_gold);

		imgBorder.sprite = DataHelper.GetVip (_vipPoint);

		imgAvatar.sprite = DataHelper.GetAvatar (_avtname);
	}

	#endregion

	Sprite GetSpriteProvider(string _provider){

//		Debug.Log ("+++ Get Spr Provider: " + _provider);

		switch (_provider) {
		case "viettel":
			return sprViettel;
		case "mobifone":
			return sprMobifone;
		case "vinaphone":
			return sprVinaphone;
		}
		return null;
	}

	void ToggleOnChange (bool _value)
	{
		if (!_value)
			return;
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		SetEnablePanelContent (toggleCard.isOn, toggleChip.isOn);
//		SetEnablePanelContent (false, true);
	}

	void SetEnablePanelContent(bool _isCard, bool _isChip)
	{
		panelCard.SetActive (_isCard);
		panelChip.SetActive (_isChip);

		if (_isCard) {
			if (string.IsNullOrEmpty (CurrentProvider)) {
				
				CurrentProvider = lstCardProvider [0];
			
					lstProvider [0].EnableToggle = true;
					
			}
		}
	}

	string GetValue(string _key, IDictionary _data)
	{
		foreach (DictionaryEntry entry in _data) {
			//			Debug.Log ("in: " + entry.Key);
			if (entry.Key.ToString ().Equals (_key))
				return entry.Value.ToString ();
		}
		return "";
	}
	string GetValueChild(string _keyChild, DictionaryEntry _entry)
	{
		var data = _entry.Value as IDictionary;
		foreach (DictionaryEntry entry in data) {
			if (entry.Key.ToString ().Equals (_keyChild))
				return entry.Value.ToString ();
		}
		return "";
	}
	IDictionary GetDictChild(string _key, IDictionary _data)
	{
		foreach (DictionaryEntry entry in _data) {
			if (entry.Key.ToString ().Equals (_key))
				return entry.Value as IDictionary;
		}
		return null;
	}
}

