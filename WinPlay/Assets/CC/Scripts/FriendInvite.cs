﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendInvite : MonoBehaviour
{
    [SerializeField]
    Image ImgAvatar, ImgAvatarBorder;
    [SerializeField]
    GameObject TxtUserName, btnDongy, btnTuchoi;
    string uId, uName, avatar;
    int avatarBorder;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init(string _uId, string _uName, string _avatar, int _borderAvatar)
    {
        uId = _uId;
        uName = _uName;
        avatar = _avatar;
        avatarBorder = _borderAvatar;

        if (avatar != "")
        {
            //StartCoroutine(UpdateAvatarThread(avatar, ImgAvatar));
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, ImgAvatar));
            }
            else
            {
                //ImgAvatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, ImgAvatar));
            }
        }
        if (avatarBorder != -1)
        {
            if (DataHelper.GetBoderAvatar(avatarBorder.ToString()) != null)
            {
                this.ImgAvatarBorder.sprite = DataHelper.GetBoderAvatar(avatarBorder.ToString());
                this.ImgAvatarBorder.gameObject.SetActive(true);
                //this.avatarBoder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                // this.avatarBoder.gameObject.SetActive(false);
                //this.avatarBoder_Defaut.gameObject.SetActive(true);
            }
        }
    }
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;
        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }

    public void DongYKetBan()
    {
        btnDongy.SetActive(false);
        btnTuchoi.SetActive(false);
        SFS.Instance.AddFriendConfirm(uId, 1);
        Destroy(gameObject);
    }
    public void TuChoiKetBan()
    {
        btnDongy.SetActive(false);
        btnTuchoi.SetActive(false);
        SFS.Instance.AddFriendConfirm(uId, -1);
        Destroy(gameObject);
    }
}
