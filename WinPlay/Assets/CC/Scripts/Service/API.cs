﻿using UnityEngine;
using System.Collections;
using BaseCallBack;
using System.Collections.Generic;
using Facebook.Unity;
using System;

public class API : MonoBehaviour
{
    //winplay123!@#
    private static API instance;

    [SerializeField]
    private string API_KEY = "6y825Oei113X3vbz78Ck7Fh7k3xF68Uc0lki41GKs2Z73032T4z8m1I81648JcrY";

    public string getAPIKey()
    {
        return API_KEY;
    }
    string GP = "";
    public static string DomainSFS = "";
    public static int PortSFS;
    //	private int PortSfs = 9933;

    //const string MAIN_DOMAIN = "http://devapp.gamekt.club";
    const string MAIN_DOMAIN = "http://poker.pokewarm.com";

    public string domainAPI
    {
        get { return MAIN_DOMAIN + "/api/v1"; }
    }

    public string prefixAVBorder
    {
        get { return MAIN_DOMAIN + "/files/avatar_border/com.gamebai.tienlenmiennam.online/"; }
    }

    public string prefixAVT
    {
        get { return MAIN_DOMAIN + "/files/avatar/"; }
    }

  //  http://poker.pokewarm.com/files/avatar_border/com.gamebai.tienlenmiennam.online/1_min.PNG

    private string prefixImage
    {
        get { return MAIN_DOMAIN; }
    }

    public string DOMAIN
    {
        get { return MAIN_DOMAIN; }
    }

#if UNITY_ANDROID
    public const int Version = 30;
#elif UNITY_IOS
	public const int Version = 1;
#else
	public const int Version = 1;
#endif

#if UNITY_ANDROID
    public const int VersionUpdate = 1;
#elif UNITY_IOS
	public const int VersionUpdate = 1;
#else
	public const int VersionUpdate = 1;
#endif

    public static string PREFIX_AVT = "";
    public static string PREFIX_AVT_BORDER = "";

    public static API Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject obj = new GameObject("API");
                instance = obj.AddComponent<API>();
            }
            return instance;
        }
    }

    public string PrefixImage
    {
        get
        {
            return prefixImage;
        }
    }

    //  http://poker.pokewarm.com/files/avatar_border/com.gamebai.tienlenmiennam.online/1_min.PNG



  


    

    public string GetAvatarBorderUrl(string avatarId)
    {
        return prefixAVBorder + "/" + avatarId + "_min.PNG";
    }

    public string GetAvatarBorderShopUrl(string avatarId)
    {
        return prefixAVBorder + "/" + avatarId + ".PNG";
    }

    




    public void LoadShopBoderAvatar(string avatarId)
    {
        string url = GetAvatarBorderShopUrl(avatarId);

        StartCoroutine(GameHelper.Thread(url, (_spr) => {
            DataHelper.AddBoderAvatar_Shop(avatarId, _spr);
        }));
    }



    public void LoadBoderAvatar(string avatarId)
    {

        string url = GetAvatarBorderUrl(avatarId);

        StartCoroutine(GameHelper.Thread(url, (_spr) => {
            DataHelper.AddBoderAvatar(avatarId, _spr);
        }));
    }










    string Token
    {
        get { return MyInfo.TOKEN; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);

        instance = this;

        DontDestroyOnLoad(gameObject);
      //  GetGP();
        PREFIX_AVT = prefixAVT;
        PREFIX_AVT_BORDER = prefixAVBorder;

    }



    public void RequestSaveInfo(string _fullName, string _phone, string _email)
    {

    }

    public void RequestSignIn(string _uname, string _pass, onCallBackString _callBack, LoginType loginType)
    {
        Debug.Log("Request SignIn");
        string url = domainAPI + "/users/login";

        WWWForm form = new WWWForm();
        form.AddField("package_name", Application.identifier);
        if (loginType == LoginType.Facebook)
        {
            form.AddField("partner_type", (int)loginType);
            form.AddField("package_name", Application.identifier);
            form.AddField("accesstoken", AccessToken.CurrentAccessToken.TokenString);
            if (_uname.Length > 0)
            {
                form.AddField("username", _uname);
            }
            Debug.Log("fb Access token   " + AccessToken.CurrentAccessToken.TokenString);
            MyInfo.FB_TOKEN = AccessToken.CurrentAccessToken.TokenString;

        }
        else
        {
            form.AddField(UNAME, _uname);
            form.AddField(PASS, _pass);
        }

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestSignUp(string _uname, string _pass, onCallBackString _callBack)
    {

        string url = domainAPI + "/users/create";
        WWWForm form = new WWWForm();
        form.AddField(UNAME, _uname);
        form.AddField(PASS, _pass);
        form.AddField(DEVICE_ID, SystemInfo.deviceUniqueIdentifier);


        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    internal void RequestAdsFreeWheel(Func<object, object> p)
    {
        throw new NotImplementedException();
    }
    public void RequestInitChangeChip(onCallBackString _callBack)
    {
        string url = domainAPI + "/transfer/init";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        //Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    public void RequestChangeChip(string message, string uReceive_id, long moneyChange, onCallBackString _callBack)
    {
        string url = domainAPI + "/transfer/transfer";

        WWWForm form = new WWWForm();
        form.AddField("to_user_name", uReceive_id);
        form.AddField("money", moneyChange.ToString());
        form.AddField("msg", message);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        //Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestCheckPaymentShop(onCallBackString _callBack)
    {
        string url = domainAPI + "/users/checkpayment/";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestAchivementUser(int _gameID, onCallBackString _callBack)
    {
        string url = domainAPI + "/top/achievement/" + _gameID.ToString();

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    public void RequestBuyAvatarBorder(int _ID, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/buyavatarborder";

        WWWForm form = new WWWForm();
        form.AddField("avatar_border", _ID);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }






    public void RequestEvent(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/events/";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }













    public void RequestTopChip(onCallBackString _callBack)
    {
        string url = domainAPI + "/top/chip";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);


        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestTopNapTien(string mode, onCallBackString _callBack)
    {
        string url = domainAPI + "/top/payment";

        WWWForm form = new WWWForm();
        form.AddField("mode", mode);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);


        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestToInitPig(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/pigdata";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

      //  GetGP();
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestDapHeo(int id_Heo, onCallBackString _callBack)
    {
        string url = domainAPI + "/event/pighammer";

        WWWForm form = new WWWForm();

        form.AddField("id_heo", id_Heo);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);


        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    public void RequestGetDataBoderAvatar(onCallBackString _callBack)
    {
        string url = domainAPI + "/users/getavatarborder";

        WWWForm form = new WWWForm();
        form.AddField("", "");
        Debug.Log("Token " + Token);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }




    public void RequestTopWin(int _gameID, onCallBackString _callBack)
    {
        string url = domainAPI + "/top/achievement/" + _gameID.ToString();

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestTopWinEvent(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/topweek";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestShop(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/paymentrate";

        WWWForm form = new WWWForm();
#if UNITY_ANDROID
        form.AddField("os", "android");
#elif UNITY_IPHONE
        form.AddField ("os", "ios");
        form.AddField ("bundle_id", Application.identifier);
#endif

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);


        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    public void RequestShop_GEM(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/paymentgemrate";

        WWWForm form = new WWWForm();
#if UNITY_ANDROID
        form.AddField("os", "android");
#elif UNITY_IPHONE
        form.AddField ("os", "ios");
        form.AddField ("bundle_id", Application.identifier);
#endif

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);


        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }




    public void RequestNapTichLuy(onCallBackString _callBack)
    {
        string url = domainAPI + "/users/getpaymentaccumulation";
        WWWForm form = new WWWForm();
        form.AddField("", "");
        Debug.Log("TOKEN: " + Token);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void ReceiveNapTichLuy(int receive_index, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/receiveaccumulation";
        WWWForm form = new WWWForm();
        form.AddField("receive_index", receive_index);
        Debug.Log("TOKEN: " + Token);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestShopCard(string cardType, string cardPrice, string _pin, string _serial, onCallBackString _callBack)
    {
        Debug.Log("On Charging - " + cardType + " - " + cardPrice + " - " + _pin + " - " + _serial);
        //string url = domainAPI + "/payment/cardcharging";
        string url = domainAPI + "/payment/cardcharging2";


        WWWForm form = new WWWForm();
        form.AddField(CARD_PRICE, cardPrice);
        form.AddField(CARD_TYPE, cardType);
        form.AddField(CODE, _pin);
        form.AddField(SERIAL, _serial);

        Debug.Log("TOKEN: " + Token);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        Debug.Log(CARD_TYPE + " " + cardType + "   " + CODE + "    " + _pin + "    " + SERIAL + "   " + _serial);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    public void RequestShopCard_GEM(string cardType, string cardPrice, string _pin, string _serial, onCallBackString _callBack)
    {
        Debug.Log("On Charging - " + cardType + " - " + cardPrice + " - " + _pin + " - " + _serial);
        //string url = domainAPI + "/payment/cardcharging";
        string url = domainAPI + "/payment/gemCard";


        WWWForm form = new WWWForm();
        form.AddField(CARD_PRICE, cardPrice);
        form.AddField(CARD_TYPE, cardType);
        form.AddField(CODE, _pin);
        form.AddField(SERIAL, _serial);

        Debug.Log("TOKEN: " + Token);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        Debug.Log(CARD_TYPE + " " + cardType + "   " + CODE + "    " + _pin + "    " + SERIAL + "   " + _serial);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }







    public void RequestBuySpecwheelByCard(string cardType, string cardPrice, string _pin, string _serial, onCallBackString _callBack)
    {
        Debug.Log("On Charging - " + cardType + " - " + cardPrice + " - " + _pin + " - " + _serial);
        string url = domainAPI + "/payment/wheelcard";

        WWWForm form = new WWWForm();
        form.AddField(CARD_PRICE, cardPrice);
        form.AddField(CARD_TYPE, cardType);
        form.AddField(CODE, _pin);
        form.AddField(SERIAL, _serial);

        Debug.Log("TOKEN: " + Token);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        Debug.Log(CARD_TYPE + " " + cardType + "   " + CODE + "    " + _pin + "    " + SERIAL + "   " + _serial);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    internal void RequestLacQue(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/fortunetelling";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestPaymentBoiToanInAppAndroid(string _store, string _productID, string _productToken, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/xam";

        WWWForm form = new WWWForm();

        form.AddField("store", _store);
        form.AddField("product_id", _productID);
        form.AddField("package_id", Application.identifier);
        form.AddField("purchase_token", _productToken);
        Debug.Log(" _productID " + _productID);
        Debug.Log(" _productToken " + _productToken);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestMuaQueBoi(string cardType, string cardPrice, string _pin, string _serial, onCallBackString _callBack)
    {
        Debug.Log("On Charging - " + cardType + " - " + cardPrice + " - " + _pin + " - " + _serial);
        string url = domainAPI + "/payment/xamcard";

        WWWForm form = new WWWForm();
        form.AddField(CARD_PRICE, cardPrice);
        form.AddField(CARD_TYPE, cardType);
        form.AddField(CODE, _pin);
        form.AddField(SERIAL, _serial);

        Debug.Log("TOKEN: " + Token);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        Debug.Log(CARD_TYPE + " " + cardType + "   " + CODE + "    " + _pin + "    " + SERIAL + "   " + _serial);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestBuyPigHammerByCard(string hammerType, string cardType, string cardPrice, string _pin, string _serial, onCallBackString _callBack)
    {
        Debug.Log("On Charging - " + cardType + " - " + cardPrice + " - " + _pin + " - " + _serial);
        string url = domainAPI + "/payment/hammercard";

        WWWForm form = new WWWForm();
        form.AddField(CARD_PRICE, cardPrice);
        form.AddField(CARD_TYPE, cardType);
        form.AddField(CODE, _pin);
        form.AddField(SERIAL, _serial);
        form.AddField("hammer_type", hammerType);

        Debug.Log("TOKEN: " + Token);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);
        Debug.Log(CARD_TYPE + " " + cardType + "   " + CODE + "    " + _pin + "    " + SERIAL + "   " + _serial);
        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }









    public void RequestLuckyWheelData(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/data/freewheel";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestStartWheel(string adskey, onCallBackString _callBack)
    {
        //		Debug.Log ("REQUEST LUCKY: " + _type);
        string url = domainAPI + "/event/freewheelads";
        WWWForm form = new WWWForm();
        form.AddField("adsk", adskey);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    public void RequestGetHammerIron(string adskey, onCallBackString _callBack)
    {
        //		Debug.Log ("REQUEST LUCKY: " + _type);
        string url = domainAPI + "/event/rewardhammer";
        WWWForm form = new WWWForm();
        form.AddField("adsk", adskey);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }








    public void RequestCardWheelData(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/data/cardwheel";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestStartCardWheel(string _type, onCallBackString _callBack)
    {
        string url = domainAPI + "/event/cardwheel";

        WWWForm form = new WWWForm();
        form.AddField("type", _type);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestUpdateUserInfo(string _cmnd, string _phone, string _email, string _nameAvt, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/update";

        WWWForm form = new WWWForm();
        form.AddField(CMND, _cmnd);
        form.AddField(PHONE, _phone);
        form.AddField(EMAIL, _email);
        form.AddField(AVATAR, _nameAvt);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestUpdateUserAvatar(string _nameAvt, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/update";

        WWWForm form = new WWWForm();
        form.AddField(CMND, MyInfo.CMND);
        form.AddField(PHONE, MyInfo.PHONE);
        form.AddField(EMAIL, MyInfo.EMAIL);
        form.AddField(AVATAR, _nameAvt);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    public void RequestUpdateUserBorderAvatar(int IdBorder, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/chooseavatarborder";

        WWWForm form = new WWWForm();
        form.AddField("avatar_border", IdBorder);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }






    public void RequestUpdatePassword(string _oldPwd, string _newPass, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/update";

        WWWForm form = new WWWForm();
        form.AddField(OLD_PASS, _oldPwd);
        form.AddField(PASS, _newPass);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestReceiveInbox(string idInBox, onCallBackString _callBack)
    {
        string url = domainAPI + "/transfer/receiveTransfer";

        WWWForm form = new WWWForm();
        form.AddField("inbox_id", idInBox);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestInbox(onCallBackString _callBack)
    {
        string url = domainAPI + "/users/inbox";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestDeleteInbox(string id, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/removeinbox";

        WWWForm form = new WWWForm();
        form.AddField("remove_id", id);

        Dictionary<string, string> header = form.headers;
        //        header.Add(ApiKey, API_KEY);
        //        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestSignUpQuick(string deviceID, onCallBackString _callBack)
    {
        //string url = domainAPI + "/game/quicksignup";
        string url = domainAPI + "/users/quicklogin";

        WWWForm form = new WWWForm();
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        //		form.AddField ("pkg", "com.cc.winplay");
        form.AddField("pkg", "com.b3b.zinplay");
        //		Debug.Log("PKG: " + "com.cc.winplay");

#else
		form.AddField ("pkg", Application.identifier);
#endif
        form.AddField("device_id", deviceID);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        //		header.Add(TOKEN, Token);

        //		Debug.Log("ApiKey: " + API_KEY);
        //		Debug.Log("TOKEN: " + Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestInit(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/init";

        WWWForm form = new WWWForm();
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        //		form.AddField ("pkg", "com.cc.winplay");
        form.AddField("pkg", "com.b3b.zinplay");
        //		Debug.Log("PKG: " + "com.cc.winplay");

#else
		form.AddField ("pkg", Application.identifier);
#endif

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        //		header.Add(TOKEN, Token);

        //		Debug.Log("ApiKey: " + API_KEY);
        //		Debug.Log("TOKEN: " + Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestVippointConfig(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/vippoint";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestFogetPassword(string _mail, onCallBackString _callBack)
    {
        string url = domainAPI + "/game/forgotpass";

        WWWForm form = new WWWForm();
        form.AddField("email", _mail);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));

    }
    public void RequestExchangeData(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/exchange";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestExchangeCard(string _provider, int _cardValue, onCallBackString _callBack)
    {
        Debug.Log("__ EXCHANGE _ " + _provider + " + " + _cardValue);

        string url = domainAPI + "/event/exchangecard";

        WWWForm form = new WWWForm();
        form.AddField(PROVIDER, _provider);
        form.AddField(AMOUNT, _cardValue);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestExchangeChip(int _goldValue, onCallBackString _callBack)
    {
        Debug.Log("___ EXCHANGE _ " + _goldValue);
        string url = domainAPI + "/event/exchangechip";

        WWWForm form = new WWWForm();
        form.AddField(GOLD, _goldValue);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestConfigPayment(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/paymentcond";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestCheckMaintenance(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/check";

        WWWForm form = new WWWForm();
#if UNITY_ANDROID || UNITY_IOS
        form.AddField("pkg", Application.identifier);
#else
		form.AddField ("pkg", "com.cc.winplay");
#endif
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestPaymentInAppAndroid(string _store, string _productID, string _productToken, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/inapp";

        WWWForm form = new WWWForm();

        

        form.AddField("store", _store);
        form.AddField("product_id", _productID);
        form.AddField("package_id", Application.identifier);

        
        form.AddField("purchase_token", _productToken);


        
        Debug.Log(" _productID " + _productID);
        Debug.Log(" _productToken " + _productToken);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    internal void RequestGetBarGameData(onCallBackString _callBack)
    {
        string url = domainAPI + "/bargame/data";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    internal void RequestBarTaiXiu(int bet, int point, onCallBackString _callBack)
    {
        string url = domainAPI + "/bargame/lucky";

        WWWForm form = new WWWForm();
        form.AddField("bet", bet);
        form.AddField("point", point);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    internal void RequestRunBarGame(string bet_items, onCallBackString _callBack)
    {
        string url = domainAPI + "/bargame/go";

        WWWForm form = new WWWForm();
        form.AddField("bet_items", bet_items);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    internal void RequestGemToPoint(int gem, onCallBackString _callBack)
    {
        string url = domainAPI + "/bargame/gemtopoint";

        WWWForm form = new WWWForm();
        form.AddField("gem", gem);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    internal void RequestPointToGem(int bar_point, onCallBackString _callBack)
    {
        string url = domainAPI + "/bargame/pointtogem";

        WWWForm form = new WWWForm();
        form.AddField("bar_point", bar_point);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RequestPaymentInAppAndroid_GEM(string _store, string _productID, string _productToken, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/geminapp";

        WWWForm form = new WWWForm();

        form.AddField("store", _store);
        form.AddField("product_id", _productID);
        form.AddField("package_id", Application.identifier);
        form.AddField("purchase_token", _productToken);
        Debug.Log(" _productID " + _productID);
        Debug.Log(" _productToken " + _productToken);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    public void RequestPaymentWheelInAppAndroid(string _store, string _productID, string _productToken, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/wheel";

        WWWForm form = new WWWForm();

        form.AddField("store", _store);
        form.AddField("product_id", _productID);
        form.AddField("package_id", Application.identifier);
        form.AddField("purchase_token", _productToken);
        Debug.Log(" _productID " + _productID);
        Debug.Log(" _productToken " + _productToken);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    public void RequestPaymentHammerInAppAndroid(string _store, string _productID, string _productToken, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/hammer";

        WWWForm form = new WWWForm();

        form.AddField("store", _store);
        form.AddField("product_id", _productID);
        form.AddField("package_id", Application.identifier);
        form.AddField("purchase_token", _productToken);
        Debug.Log(" _productID " + _productID);
        Debug.Log(" _productToken " + _productToken);
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }




    public void RequestPaymentInAppIOS(string _store, string _receipt, onCallBackString _callBack)
    {
        string url = domainAPI + "/payment/inapp";

        WWWForm form = new WWWForm();

        form.AddField("store", _store);
        form.AddField("receipt", _receipt);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestInitSlot(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/pokerinit";
        WWWForm form = new WWWForm();
        form.AddField("", "");
        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestSpinSlot(int betMoney, onCallBackString _callBack)
    {
        string url = domainAPI + "/event/poker";
        WWWForm form = new WWWForm();
        form.AddField("bet_money", betMoney);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestGetProductInAppId(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/inappId";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestAds(int adstype, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/requestads";

        WWWForm form = new WWWForm();
        form.AddField("ads_type", adstype);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void RewardAds(string adskey, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/rewardads";

        WWWForm form = new WWWForm();
        form.AddField("adsk", adskey);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    internal void RequestGlobalAds(onCallBackString _callBack)
    {
        string url = domainAPI + "/users/requestglobalads";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    internal void RequestAdsFreeWheel(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/requestadsfreewheel";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }


    internal void RequestAdsFreeHammer(onCallBackString _callBack)
    {
        string url = domainAPI + "/event/requestadshammer";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }










    public void RewardGlobalAds(string adskey, onCallBackString _callBack)
    {
        string url = domainAPI + "/users/rewardglobalads";

        WWWForm form = new WWWForm();
        form.AddField("adsk", adskey);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void UseGiftCode(string code, onCallBackString _callBack)
    {
        string url = domainAPI + "/event/giftcode";

        WWWForm form = new WWWForm();
        form.AddField("code", code);

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add("token", Token);

        Debug.Log(Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    public void GetAvatarBorder(onCallBackString _callBack)
    {
        string url = domainAPI + "/game/getavatarborder";

        WWWForm form = new WWWForm();
        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }



    #region ARENA

    public void RequestArenaData(onCallBackString _callBack)
    {

        Debug.Log("ARENA DATA API:\n" + API_KEY);
        Debug.Log("ARENA DATA API:\n" + Token);
        string url = domainAPI + "/arena/data";

        WWWForm form = new WWWForm();

        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }
    public void RequestArenaRegister(onCallBackString _callBack)
    {
        string url = domainAPI + "/arena/regis";

        WWWForm form = new WWWForm();

        form.AddField("", "");

        Dictionary<string, string> header = form.headers;
        header.Add(ApiKey, API_KEY);
        header.Add(TOKEN, Token);

        StartCoroutine(APIThread(url, form.data, header, _callBack));
    }

    #endregion

    IEnumerator APIThread(string _url, byte[] _data, Dictionary<string, string> _header, onCallBackString _callBack)
    {
        Debug.Log("___ URL: " + _url);
        WWW www = new WWW(_url, _data, _header);
        yield return www;

        Debug.Log("Rsp API - " + _url + "\n" + www.text);

        _callBack(www.text);
    }

    IEnumerator APIThread(string _url, WWWForm _form, onCallBackString _callBack)
    {

        WWW www = new WWW(_url, _form);
        yield return www;

        Debug.Log("Rsp API - " + www.text);

        _callBack(www.text);

    }

    public const string ApiKey = "apikey";
    public const string AVATAR = "avatar";
    public const string EMAIL = "email";
    public const string EXPIRED = "expired_at";
    public const string FULL_NAME = "fullname";
    public const string ID = "id";
    public const string INFO = "info";
    public const string KEY = "key";
    public const string PHONE = "phone";
    public const string UNAME = "username";
    public const string OLD_PASS = "old_pwd";
    public const string PASS = "pwd";
    public const string DEVICE_ID = "device_id";
    public const string ITEM = "items";
    public const string GOLD = "gold";
    public const string CHIP = "chip";
    public const string GEM = "gem";
    public const string VIPPOINT = "vippoint";
    public const string TOKEN = "token";
    public const string CMND = "cmnd";
    public const string UNREAD_INBOX = "unread_inbox";

    public const string PROVIDER = "provider";
    public const string CARD_TYPE = "cardtype";
    public const string CARD_PRICE = "cardprice";
    public const string CODE = "code";
    public const string AMOUNT = "amount";
    public const string PIN = "pin";
    public const string SERIAL = "serial";
}
