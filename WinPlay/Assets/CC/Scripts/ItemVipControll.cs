﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemVipControll : MonoBehaviour {
    [SerializeField]
    Text TxtTenVip, TxtYeuCau, TxtThuongDatMoc, TxtThuongNgay, TxtThuongPhanTram;
    [SerializeField]
    Image ImgVip;
    [SerializeField]
    Sprite HinhSoHuu, HinhChuaDatDuoc;
    string _YeuCau = "";

    public void Init(string TenVip, string YeuCau, string ThuongDatMoc, string ThuongNgay, string ThuongPhanTram)
    {
        TxtTenVip.text = TenVip;
        _YeuCau = YeuCau;

        TxtYeuCau.text = "VIP Point: " +YeuCau;

        long Dm = long.Parse(ThuongDatMoc);
        string DM = Utilities.GetStringMoneyByLong(Dm);

        long ng = long.Parse(ThuongNgay);
        string NG = Utilities.GetStringMoneyByLong(ng);

        long pt = long.Parse(ThuongPhanTram);
        string PT = Utilities.GetStringMoneyByLong(pt);

        TxtThuongDatMoc.text = DM ;
        TxtThuongNgay.text = NG ;
        TxtThuongPhanTram.text = PT  + " %";
    }

    public void SoHuu()
    {
        ImgVip.sprite = HinhSoHuu;
    }

    public void ChuaDatDuoc()
    {
        ImgVip.sprite = HinhChuaDatDuoc;

        TxtYeuCau.text  = "<color=#928282FF>" + "VIP Point: " +_YeuCau + "</color>"; 
    }



}
