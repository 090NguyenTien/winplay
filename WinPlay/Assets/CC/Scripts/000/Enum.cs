﻿public enum CardType
{
    BICH, CHUON, RO, CO
}

public enum GameScene
{
    LoginScene,
    HomeSceneV2,
    WaitingRoom,
    TLMNScene,
	MBScene,
	XTScene,
	PhomScene,
    TaiXiuScene,
    DuaNguaScence,
    BaiCaoScence,
	BauCuaScene,
    //BoiToanScene,
    XengHoaQua,
    TaiXiuMiniScene,
    ChangeChipScene
}

public enum TLMNCardType
{
    SINGLE,//rac
    PAIR, // doi
    THREE_OF_A_KIND,//xam co
    STRAIGHT, //sanh
    FOUR_OF_A_KIND, // tu qui
    THREE_PAIR_SEQUENCE,//3 doi thong
    FOUR_PAIR_SEQUENCE,//4 doi thong
    NULL//null
}
public enum BaiCaoCardType
{
    SINGLE,//rac
    PAIR, // doi
    THREE_OF_A_KIND,//xam co
    STRAIGHT, //sanh
    FOUR_OF_A_KIND, // tu qui
    THREE_PAIR_SEQUENCE,//3 doi thong
    FOUR_PAIR_SEQUENCE,//4 doi thong
    NULL//null
}
public enum TLMNImediateWinType
{
    FOUR_2,//4 con heo
    DRAGON_STRAIGHT,//sanh rong
    SIX_PAIR,//sau doi
    FIVE_PAIR_SEQUENCE,//5 doi thong
    SAME_COLOR// 13 la dong chat
}