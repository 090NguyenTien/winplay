﻿using System;
using System.Globalization;

public class Utilities
{
    static long BILLION = 1000000000;
    static long MILLION = 1000000;
    static long THOUNSAND = 1000;

    public static string FormatVietnamCurrency(long money) {
        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
        string a = double.Parse(money.ToString()).ToString("#,###", cul.NumberFormat);
        return a;
    }
	public static string GetStringMoneyByLong(long money, bool getSign = false)
    {
        string signText = "";
        if (getSign == true)
        {
            if (money > 0)
            {
                signText = "+";
            }
        }
        if (money < 1000000)// < 1tr
        {
            return signText + string.Format("{0:#,##0}", money);
        }
        else
        {
            return signText + Math.Round(money / 1000000f, 2) + "M";
        }
    }


    public static string GetStringVipPointByLong(long money, bool getSign = false)
    {
        string signText = "";
        if (getSign == true)
        {
            if (money > 0)
            {
                signText = "+";
            }
        }
        return signText + string.Format("{0:#,##0}", money);
    }


    public static string GetStringMoneyByLongBigSmall(long money)
    {
        if (money < 1000000)// < 1tr
        {
            return Math.Round(money / 1000f, 2) + "K";
        }
        else
        {
            return Math.Round(money / 1000000f, 2) + "M";
        }
    }
    public static string GetStringMoneyByLong(int money)
	{
        if (money < 1000000)// < 1tr
        {
            return string.Format("{0:#,##0}", money);
        }
        else
        {
            return Math.Round(money / 1000000f, 2) + "M";
        }
	}
}
