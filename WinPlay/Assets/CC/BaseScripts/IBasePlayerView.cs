﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public interface IBasePlayerView {
	
//	void CountDownTime(float _time);
//	void CountDownTime(float _timeRemain, float _time);
//	void StopCountDownTime ();

	void ShowPlayer(BaseUserInfo userInfo);
	void HidePlayer();
	void UpdateChip(long _gold); 
}
