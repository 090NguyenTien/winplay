﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Sfs2X.Entities.Data;
using BaseCallBack;
using UnityEngine.UI;
using ConstMB;
using ViewMB;
using SimpleJSON;

namespace ControllerMB
{

    public class MBInGameController : MonoBehaviour
    {

        private static MBInGameController instance;
        public static MBInGameController Instance { get { return instance; } }


        [SerializeField]
        MBDataHelper DataHelper;
        [SerializeField]
        MBInGameView View;
        [SerializeField]
        MBCardView CardView;
        [SerializeField]
        MBPlayerView[] InfoAllView;
        [SerializeField]
        MBPlayerView[] InfoAllView_Enemy;
        [SerializeField]
        /// <summary>
        /// Quan ly hien thi ten chi + kqua chi + kq tien All
        /// </summary>
        MBRowView RowAllView;
        //	[SerializeField]
        //	MBResultView ResultView;
        [SerializeField]
        MBCountDownManager CountDown1;//, CountDown2;

        [SerializeField]
        PopupInviteManager popupInvite;
        [SerializeField]
        PopupAlertManager popupAlert;
        [SerializeField]
        PopupFullManager popupFull;
        [SerializeField]
        CountDownManager CountDown;

        //int[4 players] [13 cards]
        private int[][] HandCardsAll;   //HandCards 4 player, moi player 13 la bai
        private MBUserInfo[] InfoAll;

        private int UserPosSV = 0;

        /// <summary>
        /// -1 - Lung ___ 0 - Binh thuong ___ 1 - Win
        /// </summary>
        int STATUS = -2;
        /// <summary>
        /// -1 : None ___ 0 : Dang xep ___ 1 : Xep xong
        /// </summary>
        int SortState = -1;
        bool isHost;

        MBTypeImmediate TypeWin;
        MBType TypeRow1, TypeRow2, TypeRow3;

        /// <summary>
        /// Tien cuoc 1 chi
        /// </summary>
        long CASH_IN = 1000;
        enum TableState
        {
            Waiting,
            CountDown,
            Started,
            Result,
        }
        TableState State;
        /// <summary>
        /// < idSFS , positionClient >
        /// </summary>
        Dictionary<int, int> dictIdPos;

        public delegate void CallBack();
        CallBack resultComplete;
        SFS sfs;


        [SerializeField]
        GameObject ViTriBoomUser, ViTriBoom_1, ViTriBoom_2, ViTriBoom_3;
        [SerializeField]
        GameObject ItemBoom_1, ItemBoom_2, ItemBoom_3, ItemBoom_4, ItemBoom_5, ItemBoom_6;

        [SerializeField]
        GameObject ObjAvata_1, ObjAvata_2, ObjAvata_3;
        [SerializeField]
        GameObject ObjBtnInfo_1, ObjBtnInfo_2, ObjBtnInfo_3;

        private int sfsid_BiNem = -1;
        private int SfsLoaiBoom = -1;

        #region RESULT
        // idAllGroup # handCardsGroup # idFailGroup # idWinGroup # idNormalGroup # idDieGroup #
        //	kqChi1Group # kqChi2Group # kqChi3Group #
        //	kqChiAtGroup # kqTotalChiGroup # kqTotalChip
        int[] userIdAll;
        int[] userIdFailGroup;
        int[] userIdWinGroup;
        int[] userIdNormalGroup;
        int[] userIdDieGroup;
        int[][] handCardsGroup;
        int[] resultRow1Group, resultRow2Group, resultRow3Group;
        int[] resultChiAtGroup, resultRowTotalAll;
        long[] resultChipTotalAll;
        long[] resultChipTotalAllFinal;

        #endregion



        private bool WorkDone = true;
        private bool RunningSubCoroutine = false;
        private Queue<GamePacket> GamePacketQueue = new Queue<GamePacket>();
        private readonly object SyncLock = new object();
        private bool IsGettedGameRoomInfo = false;

        void Awake()
        {
            View.Init(this);
            CardView.Init(this);
            RowAllView.Init();
            Init();
        }

        float timePing = 10;
        void Update()
        {
            timePing -= Time.deltaTime;

            if (timePing < 0)
            {
                timePing = 10;
                sfs.Ping();
            }
            ObjBtnInfo_1.SetActive(ObjAvata_1.activeInHierarchy);
            ObjBtnInfo_2.SetActive(ObjAvata_2.activeInHierarchy);
            ObjBtnInfo_3.SetActive(ObjAvata_3.activeInHierarchy);

            ViTriBoom_1.SetActive(ObjAvata_1.activeInHierarchy);
            ViTriBoom_2.SetActive(ObjAvata_2.activeInHierarchy);
            ViTriBoom_3.SetActive(ObjAvata_3.activeInHierarchy);
        }



        void PlaySound(string _sound = SoundManager.BUTTON_CLICK)
        {
            //SoundManager.PlaySound (_sound);
        }

        void Init()
        {
            sfs = SFS.Instance;

            CountDown1.SetEnable(false);
            //						CountDown2.SetEnable (false);

            DataHelper.Init();
            isHost = false;
            State = TableState.Waiting;
            HandCardsAll = new int[4][];
            InfoAll = new MBUserInfo[4];
            dictIdPos = new Dictionary<int, int>();

            View.SetEnableBtnStartGame(false);
            CardView.SetEnableBigCard(false);

            for (int i = 0; i < 4; i++)
            {
                CardView.SetEnableHandCards(i, false);
                CardView.SetEnableResultRow(i, false);

                CardView.HideResultCashRow(i);
                InfoAll[i] = new MBUserInfo();
                InfoAll[i].IsJoined = false;

            }

            View.SetEnableBtnSortAgain(false);
            View.SetEnableBtnSorted(false);


            RemoveAllPlayer();

            RqGetRoomInfo();

            SortState = -1;

            popupInvite.Init();
            popupAlert.Init();
            popupFull.Init();

            IsGettedGameRoomInfo = false;

            CardView.ShowHandCardSmallSize();
            RowAllView.deactiveAllTextResult();


            LoadingManager.Instance.ENABLE = false;

            //PlaySound (SoundManager.ENTER_ROOM);
        }

        public void ExitOnClick()
        {
            PlaySound();

            if (State == TableState.Started && InfoAll[0].IsReady)
            {
                popupFull.Show(ConstText.NotifyExitMB, popupFull.Hide, RqExitRoom);
            }
            else
            {
                RqExitRoom();
            }
        }
        void RqExitRoom()
        {
            sfs.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));

            LoadingManager.Instance.ENABLE = true;
        }







        public void SendBomb()
        {
            GamePacket param = new GamePacket("bomb");

            param.Put("sfsid_nem", MyInfo.SFS_ID);
            param.Put("sfsid_binem", sfsid_BiNem);
            param.Put("bomb_type", SfsLoaiBoom);

            Debug.LogWarning("SendBomb -> sfsid_nem = " + MyInfo.SFS_ID + " sfsid_binem = " + sfsid_BiNem + " SfsLoaiBoom = " + SfsLoaiBoom);

            SFS.Instance.SendBoomRequest(param);
        }



        public void ResponseNemBoom(GamePacket param)
        {

            int sfsid_Nem = param.GetInt("sfsid_nem");
            int sfsid_BiNem = param.GetInt("sfsid_binem");
            int LoaiBoom = param.GetInt("bomb_type");
            MBPlayerView Enemy_Nem = new MBPlayerView();
            MBPlayerView Enemy_BiNem = new MBPlayerView();
            bool NemVaoUser = false;

            Debug.LogWarning("Vô ResponseNemBoom-----MyInfo.SFS_ID-" + MyInfo.SFS_ID + "     sfsid_Nem = " + sfsid_Nem + "  sfsid_BiNem = " + sfsid_BiNem + " LoaiBoom = " + LoaiBoom);

            if (sfsid_Nem != MyInfo.SFS_ID)
            {
                foreach (var item in InfoAllView_Enemy)
                {
                    if (item.sfsId == sfsid_Nem)
                    {
                        Enemy_Nem = item;
                    }
                }

                if (sfsid_BiNem == MyInfo.SFS_ID)
                {
                    NemVaoUser = true;
                }
                else
                {
                    foreach (var item in InfoAllView_Enemy)
                    {
                        if (item.sfsId == sfsid_BiNem)
                        {
                            Enemy_BiNem = item;
                        }
                    }
                }
                int ViTriSinh = Enemy_Nem.Id_ViTriBom;



                if (NemVaoUser == false)
                {
                    int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                    NemBoomControll(ViTriSinh, ViTriNem, LoaiBoom);
                }
                else
                {
                    NemBoomControll(ViTriSinh, 0, LoaiBoom);
                }

                // Debug.LogWarning("Enemy_Nem.Id_ViTriBom----- " + Enemy_Nem.Id_ViTriBom + " LoaiBoom - " + LoaiBoom);

            }
        }





        public void NemBoomControll(int ViTriSinh, int ViTriNem, int loaiBoom, int sfsidBiNem = -2)
        {

            Transform targetMove = null;
            GameObject MyBoom = null;

            #region Tao Bom

            if (loaiBoom == 0)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_1, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_1, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_1, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_1, ViTriBoom_3.transform) as GameObject;
                }
            }
            else if (loaiBoom == 1)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_2, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_2, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_2, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_2, ViTriBoom_3.transform) as GameObject;
                }
            }
            else if (loaiBoom == 2)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_3, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_3, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_3, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_3, ViTriBoom_3.transform) as GameObject;
                }
            }
            else if (loaiBoom == 3)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_4, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_4, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_4, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_4, ViTriBoom_3.transform) as GameObject;
                }
            }
            else if (loaiBoom == 4)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_5, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_5, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_5, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_5, ViTriBoom_3.transform) as GameObject;
                }
            }
            else if (loaiBoom == 5)
            {
                if (ViTriSinh == 0)
                {
                    MyBoom = Instantiate(ItemBoom_6, ViTriBoomUser.transform) as GameObject;
                }
                else if (ViTriSinh == 1)
                {
                    MyBoom = Instantiate(ItemBoom_6, ViTriBoom_1.transform) as GameObject;
                }
                else if (ViTriSinh == 2)
                {
                    MyBoom = Instantiate(ItemBoom_6, ViTriBoom_2.transform) as GameObject;
                }
                else if (ViTriSinh == 3)
                {
                    MyBoom = Instantiate(ItemBoom_6, ViTriBoom_3.transform) as GameObject;
                }
            }

            #endregion

            MyBoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            if (ViTriNem == 0)
            {
                targetMove = ViTriBoomUser.transform;
            }
            else if (ViTriNem == 1)
            {
                targetMove = ViTriBoom_1.transform;
            }
            else if (ViTriNem == 2)
            {
                targetMove = ViTriBoom_2.transform;
            }
            else if (ViTriNem == 3)
            {
                targetMove = ViTriBoom_3.transform;
            }

            ItemBoomControll MyItem = MyBoom.GetComponent<ItemBoomControll>();
            MyItem.Init(targetMove);



            if (ViTriSinh == 0)
            {
                sfsid_BiNem = sfsidBiNem;
                if (sfsid_BiNem != -2)
                {

                    SfsLoaiBoom = loaiBoom;
                    //  Debug.LogWarning("sfsid_Nem = "  + " sfsid_BiNem = " + sfsid_BiNem + " SfsLoaiBoom = " + SfsLoaiBoom);
                    MyItem.NemBoom(SendBomb, loaiBoom);
                }
            }
            else
            {
                MyItem.NemBoom(NemThuong, loaiBoom);
            }


        }
        void NemThuong()
        {

        }


        public int GetSfsIdEnemy(int id_ViTriBom)
        {
            int SfsId = -1;
            foreach (var item in InfoAllView_Enemy)
            {
                if (item.Id_ViTriBom == id_ViTriBom)
                {
                    SfsId = item.sfsId;
                    Debug.LogWarning("------------------- item.Id_ViTriBom = " + item.Id_ViTriBom + " SfsId = " + SfsId);
                    return SfsId;
                }
            }
            return SfsId;
        }


        #region PRIVATE - Table Info - Content

        void ShowTableInfo(GamePacket _param)
        {
            View.ShowTableInfo(_param.GetInt(ParamKey.ROOM_ID),
                _param.GetInt(ParamKey.HOST_ID),
                _param.GetLong(ParamKey.BET_MONEY));
        }


        #endregion

        #region KICK

        GamePacket packetKick;
        bool isKick;

        private void RspKickUser(GamePacket param)
        {
            int[] userList = param.GetIntArray(ParamKey.USER_LIST);
            foreach (int id in userList)
            {
                if (id == MyInfo.SFS_ID)
                {
					popupAlert.Show("Không đủ tiền chơi ván tiếp theo", () => {
                        sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
                    });
                }
                else
                {
                    PlayerExit(dictIdPos[id]);
                }
            }
        }

        #endregion

        #region INVITE

        public void BtnInviteOnClick()
        {
            PlaySound();
            popupInvite.Show(RqInvite, CloseInviteOnClick);
            RqInvite();
        }
        public void ItemPlayerInviteOnClick(int _id)
        {
            GamePacket param = new GamePacket(CommandKey.INVITE);
            param.Put(ParamKey.USER_ID, _id);
            SFS.Instance.SendRoomRequest(param);
        }
        void RqInvite()
        {
            GamePacket param = new GamePacket(CommandKey.GET_PLAYERS_INVITE);
            SFS.Instance.SendRoomRequest(param);
        }
        void RspGetPlayersInvite(GamePacket _param)
        {

            //			if (string.IsNullOrEmpty (userList))
            //				return;
            //						
            //			string[] infos = userList.Split ('$');
            //
            //			popupInvite.ShowItems (infos);

            popupInvite.ShowItems(_param);

            HandleNextWorkInQueue();
        }

        public void CloseInviteOnClick()
        {
            popupInvite.Hide();
        }

        #endregion

        #region ===== REQUEST =====

        void RqGetRoomInfo()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(CommandKey.GET_GAME_ROOM_INFO));
        }
        public void RqStartGame()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(CommandKey.START_GAME));
        }
        public void RqMoveCard(int _indexCard1, int _indexCard2)
        {
            //						Debug.Log ("Request Move Card");

            GamePacket param = new GamePacket(MBCommandKey.MOVE_CARD);
            param.Put(ParamKey.CARD_1, _indexCard1);
            param.Put(ParamKey.CARD_2, _indexCard2);

            SFS.Instance.SendRoomRequest(param);
        }
        public void RqFinishMove()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(MBCommandKey.FINISH_MOVE));
        }
        public void RqUnFinishMove()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(MBCommandKey.UNFINISH_MOVE));
        }

        #endregion

        #region ======== RESPONSE PUSH ========

        public void OnSFSResponse(GamePacket gp)
        {
            UnityEngine.Debug.Log("MB - " + gp.cmd + " - " + gp.param.ToJson());

            if (gp.cmd.Equals(CommandKey.USER_EXIT))
            {
                int userId = gp.GetInt(ParamKey.USER_ID);
                if (userId == MyInfo.SFS_ID)
                {
                    GamePacketQueue = new Queue<GamePacket>();
                    WorkDone = true;
                    RspExitGame(gp);

                    //PlaySound(SoundManager.EXIT_ROOM);
                    return;
                }
            }
            else if (gp.cmd.Equals(CommandKey.CountDownAtStartGame))
            {
                RspCountDown(gp);
                WorkDone = true;
                return;
            }
            else if (gp.cmd.Equals("bomb"))
            {
                ResponseNemBoom(gp);
                WorkDone = true;
                return;
            }


            GamePacketQueue.Enqueue(gp);


            if (WorkDone)
                HandleNextWorkInQueue();


        }
        private void HandleNextWorkInQueue()
        {
            WorkDone = true;
            if (GamePacketQueue.Count > 0)
                HandleGamePacketQueue(GamePacketQueue.Dequeue());

        }
        private void HandleGamePacketQueue(GamePacket gp)
        {
            if (!IsGettedGameRoomInfo)
            {
                //				print("in if 1");
                if (gp.cmd != CommandKey.WatcherGetGameRoomInfoOK && gp.cmd != CommandKey.GET_GAME_ROOM_INFO)
                {
                    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    return;
                }
            }
            Debug.Log("HandleGamePacketQueue " + gp.cmd + " WorkDone " + WorkDone);
            //if (!WorkDone)
            //    return;
            WorkDone = false;
            switch (gp.cmd)
            {
                case CommandKey.GET_GAME_ROOM_INFO:
                    //PlaySound (SoundManager.ENTER_ROOM);
                    RspRoomInfo(gp);
                    break;
                case CommandKey.NOTICE_JOIN_GAME_ROOM:
                    PushJoinGame(gp);
                    break;
                case CommandKey.START_GAME:
                    RspStartGame(gp);
                    break;
                case MBCommandKey.MOVE_CARD:
                    PlaySound(SoundManager.SELECT_CARD);
                    RspMoveCard(gp);
                    break;
                case MBCommandKey.FINISH_MOVE:
                    PlaySound(SoundManager.MB_XepBai);
                    NoticeFinishMove(gp);
                    break;
                case MBCommandKey.UNFINISH_MOVE:
                    NoticeUnFinishMove(gp);
                    break;
                case MBCommandKey.FINISH_GAME:
                    PlaySound(SoundManager.MB_FINISH);
                    PushFinishGame(gp);
                    break;
                case CommandKey.GET_PLAYERS_INVITE:
                    RspGetPlayersInvite(gp);
                    break;
                case CommandKey.USER_EXIT:
                    RspExitGame(gp);
                    break;
                case CommandKey.JOIN_GAME_LOBBY_ROOM:
                    GameHelper.ChangeScene(GameScene.WaitingRoom);
                    break;
                case CommandKey.CountDownAtStartGame:
                    RspCountDown(gp);
                    break;
                case CommandKey.KickUser:
                    packetKick = gp;
                    isKick = true;
                    break;
            }
        }

        void RspCountDown(GamePacket param)
        {
            int second = param.GetInt(ParamKey.SecondsUntilStartGame);
            CountDown.InitNew((float)second);

            if (State != TableState.Result)
                CountDown.Show();

            HandleNextWorkInQueue();
        }

        public void OnModeratorMessage(string _msg)
        {
            JSONNode node = JSONNode.Parse(_msg);
            if (node["is_level_up"].AsInt == 1)
            {
                InfoAllView[0].txtLevel.text = node["cur_level"];
                //InfoAllView[0].level = node["cur_level"].AsInt;
            }

        }
        void RspRoomInfo(GamePacket param)
        {

            RowAllView.InitBet(CASH_IN);
            ShowTableInfo(param);

            CASH_IN = param.GetLong(ParamKey.BET_MONEY);

            string[] infoPlayers = param.GetString(ParamKey.USER_INFO).Split('$');


            InfoAll = new MBUserInfo[4];

            for (int pos = 0; pos < 4; pos++)
            {
                if (int.Parse(infoPlayers[pos].Split('#')[0]) == MyInfo.SFS_ID)
                {
                    UserPosSV = pos;
                    //				Debug.Log ("UserPosSV: " + pos);
                    break;
                }
            }

            bool isPlaying = false;

            //SV pos => client Pos
            for (int i = 0; i < 4; i++)
            {
                if (infoPlayers.Length <= i) break;
                if (!string.IsNullOrEmpty(infoPlayers[i]))
                {

                    MBUserInfo user = CastUserInfoRsp(infoPlayers[i], i);
                    user.IsReady = infoPlayers[i].Split('#')[5].Equals("1");
                    user.IsJoined = true;
                    dictIdPos[user.IdSFS] = user.ClientPos;
                    //Thong tin la User
                    if (user.IdSFS == MyInfo.SFS_ID)
                    {
                        AddPlayer(0, user);
                        //					InfoAll [0] = user;
                        //					InfoAllView [0].ShowPlayer (user);
                    }
                    //Thong tin la Player
                    else
                    {
                        AddPlayer(user.ClientPos, user);
                        //					InfoAll [user.ClientPos] = user;
                        //					InfoAllView [user.ClientPos].ShowPlayer (user);
                    }
                    if (user.IsReady)
                        isPlaying = user.IsReady;

                }
                //				else
                //					InfoAllView [i].HidePlayer ();
                //					Debug.Log ("Index player NULL - " + i);
            }

            int hostID = param.GetInt(ParamKey.HOST_ID);
            isHost = hostID == InfoAll[0].IdSFS;
            View.SetEnableBtnStartGame(isHost);

            if (isPlaying)
            {
                State = TableState.Started;
                View.ShowTableState(GetMsgState(State));
            }
            else
            {
                State = TableState.Waiting;
                View.ShowTableState(GetMsgState(State));
            }


            IsGettedGameRoomInfo = true;
            HandleNextWorkInQueue();
        }
        void PushJoinGame(GamePacket _param)
        {
            PlaySound(SoundManager.ENTER_ROOM);
            string userInfo = _param.GetString(ParamKey.USER_INFO);
            int posSV = _param.GetInt(ParamKey.POSITION);


            MBUserInfo newUser = CastUserInfoPush(userInfo, posSV);
            if (State == TableState.Started || State == TableState.Result)
                newUser.IsReady = true;

            newUser.IsReady = false;
            newUser.IsJoined = true;

            idAnother = newUser.IdSFS;

            dictIdPos[newUser.IdSFS] = newUser.ClientPos;

            AddPlayer(newUser.ClientPos, newUser);
            //		InfoAll [newUser.ClientPos] = newUser;
            //		InfoAllView [newUser.ClientPos].ShowPlayer (InfoAll [newUser.ClientPos]);

            //		ShowCountDown ();

            if (isHost && State == TableState.Waiting)
                View.SetEnableBtnStartGame(true);

            HandleNextWorkInQueue();
        }
        void RspExitGame(GamePacket _param)
        {

            int pos = dictIdPos[_param.GetInt(ParamKey.USER_ID)];
            int hostID = _param.GetInt(ParamKey.HOST_ID);
            isHost = hostID == MyInfo.SFS_ID;
            if (State == TableState.Waiting)
            {

                View.SetEnableBtnStartGame(isHost);
            }

            PlayerExit(pos);

            for (int i = 1; i < 4; i++)
                if (InfoAll[i] != null)
                {
                    //***
                    if (WorkDone)
                        HandleNextWorkInQueue();

                    return;
                }

            CountDown.Stop();

            HandleNextWorkInQueue();
        }
        void PlayerExit(int _position)
        {
            if (_position == 0)
            {
                sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            }
            else
            {
                //				Debug.LogError ("POS : " + _position + " - OUT ROOM");
                InfoAll[_position].IsReady = false;

                InfoAll[_position].IsJoined = false;
                if (State != TableState.Result)
                {
                    //					HandCardsAll [_position] = null;

                    InfoAllView[_position].HidePlayer();

                    InfoAllView[_position].SetEnableStateSorting(false);
                    InfoAllView[_position].SetEnableStateSorted(false);

                    CardView.SetEnableHandCards(_position, false);

                }
            }
            HandleNextWorkInQueue();
        }

        void RspStartGame(GamePacket _param)
        {
            CountDown.Stop();
            View.HideTableState();

            State = TableState.Started;
            int[] cardList = _param.GetIntArray(ParamKey.CARD_LIST);

            for (int i = 0; i < 4; i++)
            {
                RowAllView.SetEnableValueFail(i, false);
                RowAllView.HideValueRows(i);
                RowAllView.HideValueRows(i);

                if (InfoAll[i] != null)
                {
                    if (InfoAll[i].IsJoined)
                        InfoAll[i].IsReady = true;

                    Debug.Log("START GAME - Pos joined: " + i + " ___ " + InfoAll[i].IsJoined);
                }

            }


            HandCardsAll[0] = cardList;

            DealCard();

            View.SetEnableBtnStartGame(false);

            View.SetEnableBtnSortAgain(false);
            View.SetEnableBtnSorted(true);
        }
        void RspMoveCard(GamePacket _param)
        {
            string[] newCards = _param.GetString(ParamKey.CARD_LIST).Split(',');

            HandCardsAll[0] = CastInt(newCards);

            CardView.ShowBigCards(HandCardsAll[0]);

            MBTypeImmediate win = CheckBigWin(HandCardsAll[0]);
            if (win != MBTypeImmediate.NULL)
            {
                SetValueWinBigCards(win);
                Debug.Log("MAU BINH - " + win);

                HandleNextWorkInQueue();

                return;
            }

            ShowValueRowsBigCards();

            HandleNextWorkInQueue();
        }
        void NoticeFinishMove(GamePacket _param)
        {

            SortState = 1;
            int sfsId = _param.GetInt(ParamKey.USER_ID);

            if (sfsId == MyInfo.SFS_ID)
            {

                //								CountDown1.SetEnable (false);
                //								CountDown2.SetEnable (true);
                CardView.SetEnableBackCard(0, false);
                CardView.SetEnableBigCard(false);
                CardView.SetEnableHandCards(0, true);
                ShowValueHandCards(0, STATUS);

                View.SetEnableBtnSortAgain(true);
            }
            else if (dictIdPos.ContainsKey(sfsId))
            {
                int pos = dictIdPos[sfsId];

                CardView.SetEnableSorting(pos, false);
                CardView.SetEnableSorted(pos, true);
            }

            HandleNextWorkInQueue();
        }
        void NoticeUnFinishMove(GamePacket _param)
        {
            SortState = 0;
            int sfsId = _param.GetInt(ParamKey.USER_ID);

            if (sfsId == MyInfo.SFS_ID)
            {

                //								CountDown1.SetEnable (true);
                //								CountDown2.SetEnable (false);

                ShowBigCards();
                CardView.SetEnableHandCards(0, false);

            }
            else if (dictIdPos.ContainsKey(sfsId))
            {
                int pos = dictIdPos[sfsId];
                CardView.SetEnableSorting(pos, true);
                CardView.SetEnableSorted(pos, false);
            }

            HandleNextWorkInQueue();
        }

        int idAnother;
        void PushFinishGame(GamePacket _param)
        {
            CountDown1.SetEnable(false);
            //						CountDown2.SetEnable (false);
            View.HideTableState();
            State = TableState.Result;
            //			CountDown.OnComplete ();

            //		if (SortState == 0)
            //			ShowValueHandCards (0, STATUS);
            CardView.SetEnableBigCard(false);

            SortState = -1;
            for (int i = 0; i < 4; i++)
            {

                CardView.SetEnableHandCards(i, false);
                CardView.SetEnableSorted(i, false);
                CardView.SetEnableSorting(i, false);

                CardView.SetEnableBackCard(i, true);

                //								CardView.SetEnableHandCards (i, 1, false);
                //								CardView.SetEnableHandCards (i, 2, false);
                //								CardView.SetEnableHandCards (i, 3, false);

                RowAllView.SetEnableValueFail(i, false);
                RowAllView.HideValueRows(i);
                if (InfoAll[i] != null)
                    Debug.Log("POS JOINED: " + InfoAll[i].IsJoined + " ===== " + i);
            }


            string[] resultAll = _param.GetString(ParamKey.RESULT_GAME).Split('#');

            SaveInfoResult(resultAll);

            CardView.ShowHandCardBigSize();

            PhaseWinLose();


            View.SetEnableBtnSortAgain(false);
            View.SetEnableBtnSorted(false);

        }


        // (0) idAllGroup # (1) handCardsGroup # (2) idFailGroup # 
        //	(3) idWinGroup # (4) idNormalGroup # (5) kqChi1Group # 
        //	(6) kqChi2Group # (7) kqChi3Group #	(8) kqChiAtGroup # 
        // (9) idDieGroup #(10) kqTotalChiGroup # (11) kqTotalChip
        void SaveInfoResult(string[] _resultAll)
        {
            //mang 4 sfsIDs co trong van choi
            userIdAll = CastInt(_resultAll[0].Split('$'));
            //		//Mang gom sfsId 4 players
            string[] _handCardsAll = _resultAll[1].Split('$');
            //			Debug.Log ("handAll: " + _handCardsAll.Length);
            for (int i = 0; i < _handCardsAll.Length; i++)
            {
                //			Debug.Log ("i = " + _handCardsAll[i]);


                int[] _handCards = CastInt(_handCardsAll[i].Split(','));



                //			Debug.Log (_handCards == null);
                //			Debug.Log (_handCards.Length);

                //			for (int j = 0; j < _handCards.Length; j++) {
                //				_handCards [j] = Card (_handCards [j]);
                //			}

                int _posClient = dictIdPos[userIdAll[i]];
                HandCardsAll[_posClient] = _handCards;
            }

            //sfsIDs Binh lung
            if (!string.IsNullOrEmpty(_resultAll[2]))
                userIdFailGroup = CastInt(_resultAll[2].Split('$'));
            else
                userIdFailGroup = new int[0];

            //sfsIDs Mau Binh
            if (!string.IsNullOrEmpty(_resultAll[3]))
                userIdWinGroup = CastInt(_resultAll[3].Split('$'));
            else
                userIdWinGroup = new int[0];

            //sfsIDs Binh thuong
            if (!string.IsNullOrEmpty(_resultAll[4]))
                userIdNormalGroup = CastInt(_resultAll[4].Split('$'));
            else
                userIdNormalGroup = new int[0];


            //kq 3 chi cua sfsIDs Normal
            if (!string.IsNullOrEmpty(_resultAll[5]))
                resultRow1Group = CastInt(_resultAll[5].Split('$'));
            else
                resultRow1Group = new int[0];

            if (!string.IsNullOrEmpty(_resultAll[6]))
                resultRow2Group = CastInt(_resultAll[6].Split('$'));
            else
                resultRow2Group = new int[0];

            if (!string.IsNullOrEmpty(_resultAll[7]))
                resultRow3Group = CastInt(_resultAll[7].Split('$'));
            else
                resultRow3Group = new int[3];

            //kq Chi At cua userIdAll
            if (!string.IsNullOrEmpty(_resultAll[8]))
                resultChiAtGroup = CastInt(_resultAll[8].Split('$'));
            else
                resultChiAtGroup = new int[0];


            //sfsIDs SapHam
            if (!string.IsNullOrEmpty(_resultAll[9]))
                userIdDieGroup = CastInt(_resultAll[9].Split('$'));
            else
                userIdDieGroup = new int[0];


            //kq Chi cuoi cung cua All players
            if (!string.IsNullOrEmpty(_resultAll[10]))
                resultRowTotalAll = CastInt(_resultAll[10].Split('$'));
            else
                resultRowTotalAll = new int[0];

            //kq Chip cuoi cung cua All players
            if (!string.IsNullOrEmpty(_resultAll[11]))
                resultChipTotalAll = CastLong(_resultAll[11].Split('$'));
            else
                resultChipTotalAll = new long[0];

            //kq sau khi +/- tien All players
            if (!string.IsNullOrEmpty(_resultAll[12]))
                resultChipTotalAllFinal = CastLong(_resultAll[12].Split('$'));
            else
                resultChipTotalAllFinal = new long[0];
            //		resultChipTotalAll = new long[]{ 9999999, -9999999,9999999,-9999999 };

        }

        const float TIME_PHASE = 5f;

        void PhaseWinLose()
        {
            Debug.Log(" ==== On WIN LOSE ");

            PlaySound(SoundManager.MB_SoChi);
            bool isWait = false;

            if (userIdWinGroup != null && userIdWinGroup.Length > 0)
            {
                isWait = true;
                for (int i = 0; i < userIdWinGroup.Length; i++)
                {
                    if (dictIdPos.ContainsKey(userIdWinGroup[i]))
                    {
                        int posClient = dictIdPos[userIdWinGroup[i]];


                        List<BaseCardInfo> handCards = GetHandCards(HandCardsAll[posClient]);

                        string s = "";
                        for (int j = 0; j < 13; j++)
                        {
                            s += MBGameHelper.GoodID(HandCardsAll[posClient][j]) + " - ";
                        }

                        MBTypeImmediate typeWin = MBUtils.getTypeImmediateWin(handCards);
                        if (typeWin == MBTypeImmediate.NULL)
                        {

                            string s2 = "";
                            for (int k = 0; k < 13; k++)
                            {
                                s2 += MBGameHelper.GoodID(handCards[k].Id) + " - ";
                            }
                            typeWin = MBUtils.getTypeThreeSpecial(handCards);
                        }


                        RowAllView.ShowValueWin(posClient, typeWin);


                        RowAllView.Enable(posClient, true);

                        CardView.SetEnableBackCard(posClient, false);

                        CardView.SetEnableHandCards(posClient, true);
                        CardView.SetEnableHandCards(posClient, 1, true);
                        CardView.SetEnableHandCards(posClient, 2, true);
                        CardView.SetEnableHandCards(posClient, 3, true);

                        CardView.ShowHandCardsPlayers(HandCardsAll[posClient], posClient);
                    }
                }
            }

            if (userIdFailGroup != null && userIdFailGroup.Length > 0)
            {
                isWait = true;
                for (int i = 0; i < userIdFailGroup.Length; i++)
                {
                    if (dictIdPos.ContainsKey(userIdFailGroup[i]))
                    {
                        int posClient = dictIdPos[userIdFailGroup[i]];
                        Debug.Log("POS FAIL: " + posClient);

                        CardView.SetEnableHandCards(posClient, true);
                        List<BaseCardInfo> handCards = GetHandCards(HandCardsAll[posClient]);
                        CardView.ShowHandCardsPlayers(HandCardsAll[posClient], posClient);
                        //					RowAllView.ShowValueWin (pos, MBUtils.getTypeImmediateWin (handCards));

                        CardView.SetEnableBackCard(posClient, false);

                        RowAllView.Enable(posClient, true);
                        RowAllView.SetEnableValueFail(posClient, true);
                    }
                }
            }

            Wait(isWait, PhaseRow1);
        }
        void PhaseTotal()
        {
            //			Debug.Log (" ==== On TOTAL ");
            if (userIdAll != null && userIdAll.Length > 0)
            {
                for (int i = 0; i < userIdAll.Length; i++)
                {
                    if (dictIdPos.ContainsKey(userIdAll[i]))
                    {

                        int posClient = dictIdPos[userIdAll[i]];
                        if (InfoAll[posClient] != null)
                        {

                            CardView.ShowHandCardsPlayers(HandCardsAll[posClient], posClient);
                            CardView.SetEnableBackCard(posClient, false);

                            //Hien thi tien bay len cuoi cung
                            CardView.ShowResultCashRow(posClient, resultChipTotalAll[i], TIME_PHASE);

                            if (InfoAll[posClient].IsJoined)
                            {
                                InfoAll[posClient].Chip = resultChipTotalAllFinal[i];
                                InfoAllView[posClient].UpdateChip(InfoAll[posClient].Chip);
                            }
                            //							InfoAll [i].Gold += resultChipTotalAll [i];
                            //							InfoAllView [i].UpdateGold (InfoAll [i].Gold);

                            CardView.ShowAlphaRow(posClient);
                            for (int j = 0; j < 13; j++)
                            {

                                CardView.ShowAlphaCard(
                                    posClient,
                                    j,
                                    false);
                            }
                            RowAllView.ShowResultRow(posClient, resultRowTotalAll[i], resultChipTotalAll[i], 5);
                            RowAllView.showAllResultPerRow(posClient);

                            if (posClient == 0)
                            {
                                PlaySound(resultChipTotalAll[i] > 0 ?
                                    SoundManager.WIN : SoundManager.LOSE);
                            }
                        }
                    }
                }
            }
            if (userIdDieGroup != null && userIdDieGroup.Length > 0)
            {
                for (int i = 0; i < userIdDieGroup.Length; i++)
                {
                    if (dictIdPos.ContainsKey(userIdDieGroup[i]))
                    {

                        int posClient = dictIdPos[userIdDieGroup[i]];
                        if (InfoAll[posClient] != null)
                        {

                            bool isShow = true;
                            for (int k = 0; k < userIdFailGroup.Length; k++)
                            {
                                if (userIdFailGroup[k] == userIdDieGroup[i])
                                    isShow = false;
                            }
                            CardView.SetEnableDie(posClient, isShow);
                        }
                    }
                }
            }
            MyInfo.CHIP = InfoAll[0].Chip;
            Wait(true, OnFinishResult);
        }
        void PhaseRow1()
        {
            PlaySound(SoundManager.MB_SoChi);
            bool isWait = false;

            if (userIdNormalGroup != null && userIdNormalGroup.Length > 1)
            {
                isWait = true;
                for (int i = 0; i < userIdNormalGroup.Length; i++)
                {
                    int posClient = dictIdPos[userIdNormalGroup[i]];

                    TypeRow1 = MBUtils.getTypeRow(GetRow1(HandCardsAll[posClient]));

                    RowAllView.Enable(posClient, true);

                    bool isShowRow = true;
                    if (userIdFailGroup.Length > 0)
                    {
                        for (int j = 0; j < userIdFailGroup.Length; j++)
                        {
                            if (userIdFailGroup[j] == userIdNormalGroup[i])
                                isShowRow = false;
                        }
                    }
                    if (isShowRow)
                    {
                        ShowValueHandCards(posClient, 2);//set 2 de ko hien gi ca

                        RowAllView.ShowValueRow(posClient, 1, TypeRow1);
                        CardView.setCardLighting(posClient, 1, true);

                        CardView.SetEnableHandCards(posClient, true);
                        CardView.SetEnableHandCards(posClient, 1, true);
                        CardView.SetEnableHandCards(posClient, 2, true);
                        CardView.SetEnableHandCards(posClient, 3, true);

                        RowAllView.HideValueRow(posClient, 2);

                        CardView.SetOrderRow(posClient, 1);

                        //CardView.SetEnableHandCards(posClient, 1, true);

                        CardView.SetEnableBackCard(posClient, 1, false);
                    }
                    //				RowAllView.ShowValueRow (posClient, 1, TypeRow1);

                    //			CardView.ShowResultCashRow(posClient, CASH_IN * resultRow1Group[i]);

                    int resultRow1 = resultRow1Group[i];

                    RowAllView.ShowResultRow(posClient, resultRow1, resultRow1 * CASH_IN, 1);
                }
            }

            Wait(isWait, PhaseRow2);
        }
        void PhaseRow2()
        {
            PlaySound(SoundManager.MB_SoChi);
            bool isWait = false;


            if (userIdNormalGroup != null && userIdNormalGroup.Length > 1)
            {
                isWait = true;
                for (int i = 0; i < userIdNormalGroup.Length; i++)
                {
                    int posClient = dictIdPos[userIdNormalGroup[i]];

                    TypeRow2 = MBUtils.getTypeRow(GetRow2(HandCardsAll[posClient]));

                    RowAllView.Enable(posClient, true);

                    bool isShowRow = true;
                    if (userIdFailGroup.Length > 0)
                        for (int j = 0; j < userIdFailGroup.Length; j++)
                        {
                            if (userIdFailGroup[j] == userIdNormalGroup[i])
                                isShowRow = false;
                        }
                    if (isShowRow)
                    {

                        RowAllView.ShowValueRow(posClient, 2, TypeRow2);
                        RowAllView.HideValueRow(posClient, 1);
                        CardView.setCardLighting(posClient, 2, true);
                        CardView.setCardLighting(posClient, 1, false);

                        CardView.SetOrderRow(posClient, 2);

                        CardView.SetEnableHandCards(posClient, 2, true);

                        CardView.SetEnableBackCard(posClient, 2, false);
                    }

                    int resultRow2 = resultRow2Group[i];

                    RowAllView.ShowResultRow(posClient, resultRow2, resultRow2 * CASH_IN, 2);
                }
            }

            Wait(isWait, PhaseRow3);

        }
        void PhaseRow3()
        {
            PlaySound(SoundManager.MB_SoChi);
            bool isWait = false;


            if (userIdNormalGroup != null && userIdNormalGroup.Length > 1)
            {
                isWait = true;
                for (int i = 0; i < userIdNormalGroup.Length; i++)
                {
                    int posClient = dictIdPos[userIdNormalGroup[i]];

                    TypeRow3 = MBUtils.getTypeRow(GetRow3(HandCardsAll[posClient]));

                    RowAllView.Enable(posClient, true);


                    bool isShowRow = true;
                    if (userIdFailGroup.Length > 0)
                    {
                        for (int j = 0; j < userIdFailGroup.Length; j++)
                        {
                            if (userIdFailGroup[j] == userIdNormalGroup[i])
                                isShowRow = false;
                        }
                    }
                    if (isShowRow)
                    {
                        //Debug.Log ("Pos Row3 ___ " + posClient);


                        CardView.setCardLighting(posClient, 2, false);
                        //CardView.SetEnableBackCard (posClient, 2, true);
                        CardView.SetEnableBackCard(posClient, 3, true);
                        CardView.setCardLighting(posClient, 3, true);

                        CardView.SetOrderRow(posClient, 1);

                        CardView.SetOrderRow(posClient, 3);

                        RowAllView.HideValueRow(posClient, 1);
                        RowAllView.HideValueRow(posClient, 2);

                        RowAllView.ShowValueRow(posClient, 3, TypeRow3);

                        CardView.SetEnableBackCard(posClient, 3, false);

                    }

                    int resultRow3 = resultRow3Group[i];

                    RowAllView.ShowResultRow(posClient, resultRow3, resultRow3 * CASH_IN, 3);
                }
            }
            Wait(isWait, PhaseChiAt);

        }
        void PhaseChiAt()
        {
            PlaySound(SoundManager.MB_SoChi);
            bool isWait = false;

            if (userIdAll != null && userIdAll.Length > 1)
                isWait = true;
            for (int i = 0; i < userIdAll.Length; i++)
            {
                int posClient = dictIdPos[userIdAll[i]];

                CardView.SetOrderRow(posClient, 3);
                CardView.SetOrderRow(posClient, 2);
                CardView.SetOrderRow(posClient, 1);

                CardView.setCardLighting(posClient, 1, true);
                CardView.setCardLighting(posClient, 2, true);
                CardView.setCardLighting(posClient, 3, true);

                CardView.SetEnableBackCard(posClient, false);
                //			RowAllView.SetEnableValueFail (posClient, false);

                //			ShowValueHandCards (posClient);
                if (userIdNormalGroup.Length == 0)
                {
                    CardView.SetEnableHandCards(posClient, true);
                    CardView.ShowHandCardsPlayers(HandCardsAll[posClient], posClient);
                }
                CardView.SetEnableBackCard(posClient, false);

                //			RowAllView.Enable (posClient, false);
                //			RowAllView.ShowValueRow (posClient, 1, MBUtils.getTypeRow (GetHandCards (HandCardsAll [posClient])));
                RowAllView.HideValueRows(posClient);

                //								CardView.SetEnableBackCard (posClient, true);
                CardView.ShowAlphaRow(posClient);

                bool isShow = true;

                if (userIdFailGroup.Length > 0)
                    for (int k = 0; k < userIdFailGroup.Length; k++)
                    {
                        if (userIdFailGroup[k] == userIdAll[i])
                            isShow = false;
                    }

                if (isShow)
                {
                    for (int j = 0; j < HandCardsAll[posClient].Length; j++)
                    {

                        //												CardView.SetEnableBackCard (posClient,
                        //														j,
                        //														(HandCardsAll [posClient] [j] % 13 != 0));
                        CardView.ShowAlphaCard(
                            posClient,
                            j,
                            (HandCardsAll[posClient][j] % 13 != 0));
                    }
                }

                int resultRowAt = resultChiAtGroup[i];

                RowAllView.ShowResultRow(posClient, resultRowAt, resultRowAt * CASH_IN, 4);

                //			CardView.ShowResultCashRow (posClient, CASH_IN * resultRow3Group [i]);
            }

            Wait(isWait, PhaseTotal);
        }

        void OnFinishResult()
        {
            Debug.Log(" ==== On RESULT FINISH " + isKick);


            CardView.ShowHandCardSmallSize();
            RowAllView.deactiveAllTextResult();
            State = TableState.Waiting;
            View.ShowTableState(GetMsgState(State));

            if (isKick)
            {
                RspKickUser(packetKick);
                isKick = false;
            }

            int count = 0;
            for (int i = 0; i < 4; i++)
            {

                if (i != 0)
                {
                    if (InfoAll[i] != null)
                    {

                        InfoAll[i].IsReady = false;

                        if (!InfoAll[i].IsJoined)
                        {
                            Debug.Log("OnFinish - pos " + i + " _ not READY");

                            InfoAllView[i].HidePlayer();


                            InfoAllView[i].SetEnableStateSorting(false);
                            InfoAllView[i].SetEnableStateSorted(false);

                            count++;
                        }
                    }
                    else
                        count++;
                }


                CardView.SetEnableHandCards(i, false);
                RowAllView.HideValueWin(i);
                RowAllView.HideResultRow(i);
                RowAllView.SetEnableValueFail(i, false);

                CardView.SetEnableHandCards(i, false);
                CardView.SetEnableDie(i, false);

                View.SetEnableBtnSortAgain(false);
                View.SetEnableBtnSorted(true);

                View.SetEnableBtnStartGame(isHost);
            }

            if (count < 3)
                CountDown.Show();
            HandleNextWorkInQueue();
        }

        #endregion

        #region DEAL CARD

        void DealCard()
        {
            //			CardView.SetEnableCardDeal (true);
            CardView.StartCoroutine(CardView.DealCard(InfoAll, DealCardComplete));
        }
        void DealCardComplete()
        {
            //			CountDown.Show (60);
            CardView.SetEnableCardDeal(false);
            CardView.SetEnableHandCards(0, false);

            CountDown1.Show(60);
            //						CountDown2.Show (60);

            for (int i = 1; i < InfoAll.Length; i++)
            {
                if (InfoAll[i] != null)
                {
                    if (InfoAll[i].IsReady)
                    {
                        CardView.SetEnableHandCards(InfoAll[i].ClientPos, true);
                        CardView.SetEnableBackCard(InfoAll[i].ClientPos, true);

                        CardView.SetEnableSorted(i, false);
                        CardView.SetEnableSorting(i, true);
                    }
                }
            }

            CardView.SetEnableBigCard(true);
            CardView.ShowBigCards(HandCardsAll[0]);

            //Ktra toi trang
            MBTypeImmediate win = CheckBigWin(HandCardsAll[0]);
            if (win != MBTypeImmediate.NULL)
            {
                SetValueWinBigCards(win);


                HandleNextWorkInQueue();

                return;
            }
            ShowValueRowsBigCards();

            CardView.ResetAllBigCards();

            HandleNextWorkInQueue();
        }

        #endregion

        #region HandCards Players


        private void ShowValueWinAll(int _position, MBTypeImmediate _type)
        {
            RowAllView.ShowValueWin(_position, _type);

            RowAllView.HideValueRows(_position);
            RowAllView.SetEnableValueFail(_position, false);
        }
        private void ShowValueRowsAll(int _position, MBType _row1, MBType _row2, MBType _row3)
        {
            RowAllView.ShowValueRows(_position, _row1, _row2, _row3);

            RowAllView.SetEnableValueFail(_position, false);
            RowAllView.HideValueWin(_position);
        }
        /// <summary>
        /// HandCards
        /// </summary>
        private void ShowValueFailAll(int _position)
        {
            RowAllView.SetEnableValueFail(_position, true);

            RowAllView.HideValueRows(_position);
            RowAllView.HideValueWin(_position);
        }

        #endregion


        /// <summary>
        /// Shows the value hand cards.
        /// </summary>
        /// <param name="_position">Position.</param>
        /// <param name="_status">-1 : LUNG ___ 0 : Normal ___ 1 : WIN</param>
        public void ShowValueHandCards(int _position, int _status)
        {
            CardView.ShowHandCardsPlayers(HandCardsAll[_position], _position);

            RowAllView.Enable(_position, true);

            if (_status == 1)
            {
                ShowValueWinAll(_position, TypeWin);
            }
            else if (_status == 0)
            {
                ShowValueRowsAll(_position, TypeRow1, TypeRow2, TypeRow3);
            }
            else if (_status == -1)
            {
                ShowValueFailAll(_position);
            }
        }
        public void ShowBigCards()
        {
            RowAllView.Enable(0, false);

            CardView.SetEnableBigCard(true);
            CardView.SetEnableHandCards(0, false);

            CardView.ShowBigCards(HandCardsAll[0]);

            View.SetEnableBtnSorted(true);
            View.SetEnableBtnSortAgain(false);
        }

        #region BIG CARDS

        public void DragCardSuccess(int _indexFirst, int _indexSecond, bool isSort = true, bool isShowAnim = true)
        {
            //Debug.Log ("End Drag Success");
            ExchangeCardUser(_indexFirst, _indexSecond);


            //Cap nhat lai View
            CardView.ShowBigCard(_indexFirst, HandCardsAll[0][_indexFirst]);
            CardView.ShowBigCard(_indexSecond, HandCardsAll[0][_indexSecond]);

            CardView.ChangeBigCards(_indexFirst, _indexSecond, isShowAnim);

            //if (isSort == true)
            //{
            //    CardView.sortBigCard(_indexFirst, _indexSecond);
            //}

            RqMoveCard(_indexFirst, _indexSecond);

            Debug.Log("drqag card completeeeeeeeeeeee");
            //		ShowValueRow ();
        }

        /// <summary>
        /// Hien thi gia tri 3 chi trong BigCard
        /// </summary>
        private void ShowValueRowsBigCards()
        {
            string s = "";
            for (int i = 0; i < 13; i++)
            {
                s += MBGameHelper.GoodID(HandCardsAll[0][i]).ToString() + " - ";
            }
            //			Debug.Log ("3 sanh 111: " + s);
            //Ktra toi trang (3 thung 3 sanh)
            MBTypeImmediate win = CheckRowsSpecial(HandCardsAll[0]);
            if (win != MBTypeImmediate.NULL)
            {
                SetValueWinBigCards(win);
                return;
            }

            MBType typeRow3 = MBUtils.getTypeRow(
                GetRow3(HandCardsAll[0]));

            MBType typeRow2 = MBUtils.getTypeRow(
                GetRow2(HandCardsAll[0]));

            MBType typeRow1 = MBUtils.getTypeRow(
                GetRow1(HandCardsAll[0]));

            bool isOK = CheckRowsValue(typeRow1, typeRow2, typeRow3);

            if (isOK)
            {
                SetValueRowsBigCards(typeRow1, typeRow2, typeRow3);
            }
            else
                SetValueFailBigCards();
        }

        #region checkValue

        MBTypeImmediate CheckBigWin(int[] _handCards)
        {
            return MBUtils.getTypeImmediateWin(
                GetHandCards(_handCards));
        }
        /// <summary>
        /// Ktra 3 thung 3 sanh
        /// </summary>
        MBTypeImmediate CheckRowsSpecial(int[] _handCards)
        {
            string s = "";
            for (int i = 0; i < _handCards.Length; i++)
            {
                s += MBGameHelper.GoodID(_handCards[i]).ToString() + " - ";
            }
            //			Debug.Log ("3 sanh 222: " + s);
            return MBUtils.getTypeThreeSpecial(GetHandCards(_handCards));
        }
        bool CheckRowsValue(MBType typeRow1, MBType typeRow2, MBType typeRow3)
        {
            //		Debug.Log (typeRow1 + " - " + typeRow2 + " - " + typeRow3);
            //Binh lung
            if ((int)typeRow1 < (int)typeRow2 ||
                    (int)typeRow1 < (int)typeRow3 ||
                        (int)typeRow2 < (int)typeRow3)
            {

                //				Debug.Log (" Bung linh r?i ");

                return false;
            }

            //Neu row duoi < row tren => false
            if (MBUtils.compare(GetRow1(HandCardsAll[0]),
                    GetRow2(HandCardsAll[0])) == -1)
            {

                //				Debug.Log (" Xui quá, chi 1 nh? hon chi 2 :( ");

                return false;
            }
            if (MBUtils.compare(GetRow2(HandCardsAll[0]),
                GetRow3(HandCardsAll[0])) == -1)
            {

                //				Debug.Log (" Xui quá, chi 2 nh? hon chi 3 :( ");
                return false;
            }
            STATUS = 0;
            return true;
        }

        #endregion

        #region Set Value BigCards (in HandCards) User

        void SetValueWinBigCards(MBTypeImmediate _typeWin)
        {
            STATUS = 1;
            TypeWin = _typeWin;

            CardView.SetEnableValueRows(false);
            CardView.SetEnableRowsSpecial(false);
            CardView.SetEnableValueFail(false);

            CardView.SetEnableBigWin(true);
            CardView.ShowBigWin(_typeWin);
        }
        void SetValueRowsBigCards(MBType _row1, MBType _row2, MBType _row3)
        {
            //			Debug.Log ("Set ValueRows BigCards");

            STATUS = 0;

            TypeRow1 = _row1;
            TypeRow2 = _row2;
            TypeRow3 = _row3;

            CardView.SetEnableRowsSpecial(false);
            CardView.SetEnableBigWin(false);
            CardView.SetEnableValueFail(false);

            CardView.SetEnableValueRows(true);
            CardView.ShowValueBigCards(_row1, _row2, _row3);

        }
        void SetValueFailBigCards()
        {
            Debug.Log("Set FAIL BigCards");
            STATUS = -1;

            CardView.SetEnableValueRows(false);
            CardView.SetEnableBigWin(false);
            CardView.SetEnableRowsSpecial(false);

            CardView.SetEnableValueFail(true);
        }

        #endregion

        #endregion

        #region PRIVATE - Players Info

        void AddPlayer(int _position, MBUserInfo _user)
        {
            InfoAll[_position] = new MBUserInfo();
            InfoAll[_position] = _user;

            InfoAll[_position].IsJoined = true;
            InfoAllView[_position].ShowPlayer(_user);
        }
        void RemovePlayer(int _position)
        {
            InfoAll[_position] = new MBUserInfo();
            InfoAll[_position].IsJoined = false;
            HandCardsAll[_position] = null;
            InfoAllView[_position].HidePlayer();
        }
        void RemoveAllPlayer()
        {
            //			InfoAll = new MBUserInfo[4];

            for (int i = 0; i < 4; i++)
            {
                InfoAll[i].IsJoined = false;
                InfoAllView[i].HidePlayer();
                HandCardsAll[i] = new int[13];
            }
        }

        #endregion

        #region CHAT

        public void BtnChatOnClick()
        {
            PlaySound();
            PopupChatManager.instance.Show();
        }

        public void OnPublicMsg(string _msg)
        {
            int idUser = int.Parse(_msg.Split('#')[0]);

            //Get Position by ID
            int position = dictIdPos[idUser];


            PopupChatManager.instance.RspChat(position, _msg);


        }

        #endregion

        #region COUNTDOWN

        void ShowCountDown()
        {
            //			CountDown.Show (10, HideCountDown);
        }
        void HideCountDown()
        {
            //			CountDown.OnComplete ();
        }

        #endregion

        #region Utils

        int[] CastInt(string[] _arr)
        {
            try
            {
                return Array.ConvertAll<string, int>(_arr, int.Parse);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        long[] CastLong(string[] _arr)
        {
            try
            {
                return Array.ConvertAll<string, long>(_arr, long.Parse);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        string GetMsgState(TableState _state)
        {
            switch (_state)
            {
                case TableState.Waiting:
                    return ConstText.WaitingNewMatch;
                case TableState.Started:
                    return ConstText.MatchShowing;
                case TableState.Result:
                    return "Dang hien thi ket qua, chuan bi qua van moi";
            }
            return "Noname";
        }
        void ExchangeCardUser(int _indexFirst, int _indexSecond)
        {
            int card1 = HandCardsAll[0][_indexFirst];

            HandCardsAll[0][_indexFirst] = HandCardsAll[0][_indexSecond];
            HandCardsAll[0][_indexSecond] = card1;

            //BaseCardInfo mbCard1 = BaseCardInfo.Get (HandCardsAll [0] [_indexSecond]);
            //BaseCardInfo mbCard2 = BaseCardInfo.Get (HandCardsAll [0] [_indexFirst]);

            //			Debug.Log ("Card1: " + mbCard2.Value + " - " + mbCard2.Type);
            //			Debug.Log ("Card2: " + mbCard1.Value + " - " + mbCard1.Type);

        }
        /// <summary>
        /// Phai cap nhat posUserSV truoc
        ///   - \n  - input _posSV de cap nhat _posClient
        /// </summary>
        MBUserInfo CastUserInfoRsp(string _param, int _posSV)
        {
            MBUserInfo info = new MBUserInfo();

            string[] infoUser = _param.Split('#');

            info.IdSFS = int.Parse(infoUser[0]);
            info.IdUser = infoUser[1];
            info.Name = infoUser[2];
            info.Chip = long.Parse(infoUser[3]);
            info.Avatar = infoUser[4];
            info.ClientPos = PosClient(_posSV);


            info.Vip_collec = infoUser[6];

            info.Level = int.Parse(infoUser[7]);

            info.Bor_Avatar = infoUser[8];

     //       info.Gem = long.Parse(infoUser[9]);


            return info;
        }
        MBUserInfo CastUserInfoPush(string _param, int _posSV)
        {
            MBUserInfo info = new MBUserInfo();

            string[] infoUser = _param.Split('#');

            info.IdSFS = int.Parse(infoUser[0]);
            info.IdUser = infoUser[1];
            info.Name = infoUser[2];
            info.Chip = long.Parse(infoUser[3]);
            info.Avatar = infoUser[4];

            info.ClientPos = PosClient(_posSV);

            info.Vip_collec = infoUser[5];
            info.Level = int.Parse(infoUser[6]);
            info.Bor_Avatar = infoUser[7];
   //         info.Gem = long.Parse(infoUser[8]);

            return info;
        }

        /// <summary>
        /// Phai co UserPosSV truoc
        /// </summary>
        int PosClient(int _posSV)
        {
            return (_posSV + 4 - UserPosSV) % 4;
        }

        List<BaseCardInfo> GetRow1(int[] _handCards)
        {
            List<BaseCardInfo> result = new List<BaseCardInfo>();

            for (int i = 0; i < 5; i++)
            {
                result.Add(BaseCardInfo.Get(_handCards[i]));
            }

            return result;
        }
        List<BaseCardInfo> GetRow2(int[] _handCards)
        {
            List<BaseCardInfo> result = new List<BaseCardInfo>();

            for (int i = 5; i < 10; i++)
            {
                result.Add(BaseCardInfo.Get(_handCards[i]));
            }

            return result;
        }
        List<BaseCardInfo> GetRow3(int[] _handCards)
        {
            List<BaseCardInfo> result = new List<BaseCardInfo>();

            for (int i = 10; i < 13; i++)
            {
                result.Add(BaseCardInfo.Get(_handCards[i]));
            }

            return result;
        }
        List<BaseCardInfo> GetHandCards(int[] _handCards)
        {
            List<BaseCardInfo> result = new List<BaseCardInfo>();
            for (int i = 0; i < 13; i++)
            {
                result.Add(BaseCardInfo.Get(_handCards[i]));
            }

            return result;
        }

        #endregion
        onCallBack onFinishPhase;
        void Wait(bool _isDelay, onCallBack _onFinish = null, float _time = TIME_PHASE)
        {
            //			Debug.Log ("___ WAIT - " + _isDelay + " - " + TIME_PHASE + " giay");
            StartCoroutine(ActionDelay(_isDelay, _onFinish, _time));
        }
        IEnumerator ActionDelay(bool _isDelay, onCallBack _onFinish = null, float _time = TIME_PHASE)
        {
            if (!_isDelay)
                _time = 0;
            onFinishPhase = _onFinish;
            yield return new WaitForSeconds(_time);
            if (onFinishPhase != null)
                onFinishPhase();
        }

        int Card(int _newID)
        {
            int rank = _newID / 10;
            int suite = _newID % 10;

            int id = suite * 13 + (rank % 14) - 1;

            return id;
        }
        void LogValue(List<BaseCardInfo> _cards)
        {
            string s = "Value : " + MBUtils.getTypeRow(_cards).ToString();
            for (int i = 0; i < _cards.Count; i++)
            {
                s += " - " + _cards[i].Id;
            }

            Debug.Log(s);
        }

        public void OnConnectionLost()
        {
            popupAlert.Show(ConstText.ErrorConnection, () => {
                GameHelper.ChangeScene(GameScene.LoginScene);
            });
        }

    }
}