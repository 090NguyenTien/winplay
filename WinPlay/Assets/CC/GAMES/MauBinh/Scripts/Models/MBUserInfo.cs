﻿using UnityEngine;
using System.Collections;

public class MBUserInfo : BaseUserInfo {

		/// <summary>
	/// 0 : None - 1 : Sorting - 2 : Sorted 
	/// </summary>
	/// <value>The state sort.</value>
	public int StateSort {
		get;
		set;
	}
}
