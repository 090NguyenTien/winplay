﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horse : MonoBehaviour {

    public GameObject horse;
    public HorseController horseController;
    private int rank=0;
    private string horseName;
    private bool isRun = false;
    int valueRandom = 3;
    private Animator animator;
    private int stand;
    private int normalRun;
    private float TargetPoint = 4f;
    private float InscreasePoint = 0f;
    private float TargetSlowPoint = 3.5f;
    bool paused;
    private float increaseSpeed=1f;
    float startPositionX;
    bool timeInscreaseSpeed=false;
    public int ValueRandom
    {
        get
        {
            return valueRandom;
        }

        set
        {
            valueRandom = value;
        }
    }

    void Awake()
    {
        animator = horse.GetComponent<Animator>();
        startPositionX = horse.transform.localPosition.x; //+ horse.GetComponent<SpriteRenderer>().bounds.size.x;
    }
    // Use this for initialization
    void Start () {
        
        //stand = Animator.StringToHash("HorseStand");
        //normalRun = Animator.StringToHash("HorseNormalRun");        
    }
	
	// Update is called once per frame
	void Update () {
        if (!isRun) return;
        float posX = horse.transform.position.x;
        float speed;
        int rand = UnityEngine.Random.Range(1, valueRandom);
        if (!timeInscreaseSpeed)
        {
            if (posX < InscreasePoint)
            {
                if (rank == 1 || rank == 2) speed = UnityEngine.Random.Range(-1f, 3f);
                else speed = UnityEngine.Random.Range(-1f, 3f);
                if (rand != 1) speed = 0.1f;
                //speed = Random.Range(0.1f, 0.5f);
            }
            else
            {
                if (rank == 1) speed = UnityEngine.Random.Range(2.7f, 4f);
                else if (rank == 2) speed = UnityEngine.Random.Range(2.7f, 4f);
                else speed = UnityEngine.Random.Range(1f, 2.6f);
            }
        }
        else
        {
            if (rank == 1) speed = UnityEngine.Random.Range(2.7f, 4f);
            else if (rank == 2) speed = UnityEngine.Random.Range(2.7f, 4f);
            else speed = UnityEngine.Random.Range(1f, 2.6f);
        }
        
        if (posX >= TargetPoint)
        {           
            if (rank == 1)
            {
                if (horseController.countHorseComplete == 0)
                {
                    horseController.countHorseComplete++;
                }
            }
            else if (rank == 2)
            {   
                //if (horseController.countHorseComplete==0) return;
                if (posX >= 3.8f)
                {
                    if (horseController.countHorseComplete == 1) horseController.HorseWinCompleteRace();//Ngua Thu 2 cán đích                    
                    Time.timeScale = 1.0f;//tang toc do ngua chay sau khi 2 con ngua da ve dich
                }
            }
            else
            {               
                if (posX >= 3.8f)
                {
                    if (horseController.countHorseComplete < 1) return;
                    if (horseController.countHorseComplete == 2)
                    {
                        StartCoroutine(horseController.ShowResultEndRace());
                    }
                }
            }
        }
        if (horseController.countHorseComplete > 1) speed = UnityEngine.Random.Range(2f, 5f); ;
        //if (rand != 1) speed = 0.1f;
        // else //(Random.Range(1, valueRandom) == 1)
        //{           
        //if (posX < -1)
        //{
        //    speed = Random.Range(1f, 3f);
        //}else if(posX < 1){
        //    speed = Random.Range(1f, 5f);
        //}
        //else
        //{
        //if(rank==1||rank==2) speed = Random.Range(3f, 7f);
        //else speed = Random.Range(1f, 2f);
        //}            
        // }
        horse.transform.Translate((new Vector2(Time.deltaTime * speed * increaseSpeed, 0)));
        if (rank == 1)
        {
            //Debug.LogError("Time.deltaTime * speed" + Time.deltaTime * speed);
           // if (posX >= TargetSlowPoint)//set slow motion game
            //{
                //Time.timeScale = .8f;
                //valueRandom = 1;
                //if (!paused)
                //{
                //    Time.timeScale = .3f;
                //    paused = true;
                //}
                //else
                //{
                //    Time.timeScale = .6f;
                //    paused = false;
                //}
           // }
            if (posX >= 1) horseController.showTargetLine();//hien thi vach đích
        }
    }
    public void Init(int rank, string horseName)
    {
        this.rank = rank;
        this.horseName = horseName;        
        calculatorTargetPointByRank();
    }
    private void calculatorTargetPointByRank()
    {
        switch (rank)
        {
            case 1:
                TargetPoint = 4.0f;
                TargetSlowPoint = 3.8f;
                break;
            case 2:
                TargetPoint = UnityEngine.Random.Range(1.2f, 2.5f);
                break;
            case 3:
                TargetPoint = UnityEngine.Random.Range(1.2f, 2.5f);
                break;
            case 4:
                TargetPoint = UnityEngine.Random.Range(1.2f, 2.5f);
                break;
            case 5:
                TargetPoint = UnityEngine.Random.Range(1.2f, 2.5f);
                break;
            case 6:
                TargetPoint = UnityEngine.Random.Range(1.2f, 2.5f);
                break;
           
           
           
        }
    }
    public void Run()
    {
        timeInscreaseSpeed = false;
        valueRandom = 5;
        Time.timeScale = 1f;
        //horse.transform.Translate(new Vector3(startPositionX, 0));        
        horse.transform.localPosition = new Vector2(startPositionX, horse.transform.localPosition.y);
        StartCoroutine(RunAfterFewSecond());
        StartCoroutine(InscreaseSpeedHorse());
    }

    private IEnumerator InscreaseSpeedHorse()
    {
        yield return new WaitForSeconds(15f);
        timeInscreaseSpeed = true;
        yield return null;
    }

    public void Stop()
    {
        if(animator) animator.SetBool("stand", true);
        isRun = false;
    }
    private IEnumerator RunAfterFewSecond()
    {       
        yield return new WaitForSeconds(3f);
        isRun = true;
        animator.SetBool("stand", false);
        yield return null;
    }
}
