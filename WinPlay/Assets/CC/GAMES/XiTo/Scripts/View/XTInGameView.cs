﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ControllerXT;
using DG.Tweening;
using ConstXT;

namespace ViewXT {
		
	public class XTInGameView : MonoBehaviour {

		XTInGameController Controller;

		[SerializeField]
		private Text txtNameId, txtBet, txtNameGame, txtState,
				txtBetTotal, txtBetCurrent;
		[SerializeField]
		private Button btnFold, btnCall, btnCheck, btnRaiseParent,
				btnRaise, btnRaiseAll, btnRaiseMulti2, btnRaisePart2, btnRaisePart4,
				btnStartGame;
		[SerializeField]
		private Transform trsfPanelRaises;
		[SerializeField]
		private GameObject BetGameCurrent;

				[SerializeField]
				private List<Button> btnInvites;
		// Use this for initialization
		//	void Awake () {
		//		Controller = new MBInGameController ();
		//		Controller.Init (this);
		//	}
		[SerializeField]
		Button btnExit;


		void BtnChatOnClick()
		{
			Controller.BtnChatOnClick ();
		}
		public void Init(XTInGameController _controller)
		{
			Controller = _controller;

			btnFold.onClick.AddListener (FoldOnClick);
			btnCall.onClick.AddListener (CallOnClick);
			btnCheck.onClick.AddListener (CheckOnClick);
			btnRaiseParent.onClick.AddListener (RaiseParentOnClick);
			btnRaise.onClick.AddListener (RaiseOnClick);
			btnRaiseAll.onClick.AddListener (RaiseAllOnClick);
			btnRaiseMulti2.onClick.AddListener (RaiseMulti2OnClick);
			btnRaisePart2.onClick.AddListener (RaisePart2OnClick);
			btnRaisePart4.onClick.AddListener (RaisePart4OnClick);
	
			for (int i = 0; i < btnInvites.Count; i++) {
				btnInvites [i].onClick.AddListener (InviteOnClick);
			}

			HideBetGameCurrent ();

			isOpenRaise = false;


			btnExit.onClick.AddListener (ExitOnClick);
		}

		void ExitOnClick()
		{
			Controller.ExitOnClick ();
		}
				
		void InviteOnClick ()
		{
				Controller.BtnInviteOnClick ();
		}

		public void ShowTableInfo(int _roomId, int _hostID, long _bet)
		{
			txtNameId.text = "Bàn số: " + _roomId.ToString();
			txtBet.text = "Mức cược: " + Utilities.GetStringMoneyByLong (_bet);
			txtNameGame.text = ConstText.XiTo;
		}

		public void ShowBetGameCurrent(long _chipTotal, long _chipCurrent)
		{
			BetGameCurrent.SetActive (true);
			txtBetTotal.text = Utilities.GetStringMoneyByLong (_chipTotal);
			txtBetCurrent.text = Utilities.GetStringMoneyByLong (_chipCurrent);
		}

		public void HideBetGameCurrent()
		{
			BetGameCurrent.SetActive (false);
		}

		#region TABLE STATE

		public void ShowTableState(string _msg)
		{
			txtState.gameObject.SetActive (true);
			txtState.text = _msg;
		}
		public void HideTableState()
		{
			txtState.gameObject.SetActive (false);
		}

		#endregion

		#region Button Groups

		void StartGameOnClick()
		{
			Controller.RqStartGame ();
		}

		public void SetEnableBtnActions(bool _enable)
		{
			
			btnFold.gameObject.SetActive (_enable);
			btnCall.gameObject.SetActive (_enable);
			btnCheck.gameObject.SetActive (_enable);
			btnRaiseParent.gameObject.SetActive (_enable);
			btnRaise.gameObject.SetActive (_enable);
			btnRaiseAll.gameObject.SetActive (_enable);
			btnRaiseMulti2.gameObject.SetActive (_enable);
			btnRaisePart2.gameObject.SetActive (_enable);
			btnRaisePart4.gameObject.SetActive (_enable);
		}
		public void SetActiveBtnCall(bool _enable, bool _interact = true)
		{
			btnCall.interactable = _interact;
			btnCall.gameObject.SetActive (_enable);
		}
		public void SetActiveBtnCheck(bool _enable, bool _interact = true)
		{
			btnCheck.interactable = _interact;
			btnCheck.gameObject.SetActive (_enable);
		}
		public void SetActiveBtnRaise(bool _enable, bool _interact = true)
		{
			btnRaise.interactable = _interact;
			btnRaise.gameObject.SetActive (_enable);
		}
		public void SetActiveBtnPart4(bool _enable){
			btnRaisePart4.interactable = _enable;
		}
		public void SetActiveRaiseUp(bool _enable){
			btnRaiseParent.interactable = _enable;
		}

		public void SetEnableBtnStartGame(bool _enable)
		{
//			btnStartGame.gameObject.SetActive (_enable);
		}



		void FoldOnClick ()
		{
			Controller.FoldOnClick ();
		}

		void CallOnClick ()
		{
			Controller.CallOnClick ();
		}

		void CheckOnClick()
		{
			Controller.CheckOnClick ();
		}

		bool isOpenRaise;
		const float TIME_MOTION = .15f;

		public void RaiseParentOnClick ()
		{
			if (!isOpenRaise)
				ShowRaiseParent ();
			else
				HideRaiseParent ();
		}
		public void ShowRaiseParent()
		{
			Tween tw = trsfPanelRaises.DOScaleY (1, TIME_MOTION).OnComplete (() => {
				trsfPanelRaises.localScale = Vector3.one;
			});
			tw.SetEase (Ease.OutBack);
			isOpenRaise = true;

			trsfPanelRaises.gameObject.SetActive (isOpenRaise);
		}
		public void HideRaiseParent()
		{
			Tween tw = trsfPanelRaises.DOScaleY (0, TIME_MOTION).OnComplete (() => 
            {
				trsfPanelRaises.localScale = new Vector3 (1, 0, 1);

				trsfPanelRaises.gameObject.SetActive (isOpenRaise);
			});
			tw.SetEase (Ease.InBack);
			isOpenRaise = false;
		}

		void RaiseOnClick()
		{
			Controller.RaiseOnClick ();
		}

		void RaiseAllOnClick ()
		{
			Controller.RaiseAllOnClick ();
		}

		void RaiseMulti2OnClick ()
		{
			Controller.RaiseMulti2OnClick ();
		}

		void RaisePart2OnClick ()
		{
			Controller.RaisePart2OnClick ();
		}

		void RaisePart4OnClick ()
		{
			Controller.RaisePart4OnClick ();
		}

		#endregion

		public void FinishMove(int i)
		{
			Debug.Log("iii : " + i);
		}

	}

}