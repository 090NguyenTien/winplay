﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using BaseCallBack;
using HelperXT;
using ConstXT;

namespace ViewXT
{

    public class XTPlayerView : BasePlayerView
    {
        [SerializeField]
        RectTransform rectTxtChipResult;
        [SerializeField]
        private Text txtChipResult, txtAction;
        [SerializeField]
        Image imgAction;
        [SerializeField]
        private Image imgValueHandCards;
        [SerializeField]
        private GameObject panelChip, Win, Lose;

        [SerializeField]
        PopupInfoEnemy InfoEnemy;


        [SerializeField]
        public int Id_ViTriBom;

        const float TIME_GOLD_RESULT = 4; //Thgian kqua tien bay len

        #region Extend

        public override void ShowPlayer(BaseUserInfo userInfo)
        {
            panelChip.SetActive(true);
            txtChip.gameObject.SetActive(true);
            txtDisName.gameObject.SetActive(true);

            sfsId = userInfo.IdSFS;
            Gem = userInfo.Gem;
            txtChip.text = Utilities.GetStringMoneyByLong(userInfo.Chip);
            txtDisName.text = userInfo.Name;
            txtLevel.text = DataHelper.GetStringVipByStringVipPoint(userInfo.Vip_collec);//doi level thanh vip ben Chinh//userInfo.Level.ToString();
            Debug.LogWarning("txtLevel.text=====" + userInfo.Level + "       Vip_collect======= " + userInfo.Vip_collec + "       BorAvatar========= " + userInfo.Bor_Avatar);
            imgAvatar.gameObject.SetActive(true);

            this.Vip = DataHelper.GetStringVipByStringVipPoint(userInfo.Vip_collec);

            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(false);

            int id_Bor = int.Parse(userInfo.Bor_Avatar);
            string my_id_Bor = userInfo.Bor_Avatar;

            if (id_Bor != -1)
            {
                if (DataHelper.GetBoderAvatar(my_id_Bor) != null)
                {
                    imgBorderAvt.sprite = DataHelper.GetBoderAvatar(my_id_Bor);

                    imgBorderAvt.gameObject.SetActive(true);
                    imgBorderAvt_Defaut.gameObject.SetActive(false);
                }
                else
                {
                    imgBorderAvt.gameObject.SetActive(false);
                    imgBorderAvt_Defaut.gameObject.SetActive(true);
                }
            }
            else
            {
                imgBorderAvt.gameObject.SetActive(false);
                imgBorderAvt_Defaut.gameObject.SetActive(true);
            }
            imgBorderAvt.transform.localScale = new Vector2(1.1f, 1.1f);
            // imgBorderAvt.gameObject.SetActive (true);
            LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            //if (loginType != LoginType.Facebook) {
            //	imgAvatar.sprite = DataHelper.GetAvatar (userInfo.Avatar);
            //} else {
            //	StartCoroutine (UpdateAvatarThread (userInfo.Avatar, this.imgAvatar));
            //}

            // load avatar facebook -- khuong
            if (MyInfo.SFS_ID == sfsId)
            {
                if (loginType != LoginType.Facebook)
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
                else
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
            }
            else
            {
                if (userInfo.Avatar.Contains("http") == true)
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
                else
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
            }



            //			imgBorderAvt.gameObject.SetActive (
            //				true);
            //			imgBorderAvt.sprite = DataHelper.GetVip (userInfo.VipPoint);

            //			StartCoroutine (UpdateAvatarThread (userInfo.Avatar));
        }
        //public override IEnumerator UpdateAvatarThread (string _url)
        //{
        //	Texture2D mainImage;
        //	WWW www = new WWW(_url);
        //	yield return www;

        //	mainImage = www.texture;
        //	Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

        //	imgAvatar.sprite = spr;
        //}

        public override void HidePlayer()
        {
            imgAvatar.gameObject.SetActive(false);
            //imgBorderAvt.sprite = DataHelper.GetVip (0);
            imgBorderAvt.gameObject.SetActive(false);
            imgBorderAvt_Defaut.gameObject.SetActive(false);
            panelChip.gameObject.SetActive(false);
            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(true);
            txtDisName.gameObject.SetActive(false);
        }
        public override void UpdateChip(long _chip)
        {
            txtChip.text = Utilities.GetStringMoneyByLong(_chip);
        }

        #endregion

        public void ShowChipResult(bool _isWin, long _gold)
        {
            txtChipResult.color = new Color(1, 1, 1, 1);
            txtChipResult.gameObject.SetActive(true);
            rectTxtChipResult.anchoredPosition = new Vector2(0, 50);
            //			txtChipResult.transform.localPosition = new Vector3 (0, 50, 0);

            if (_isWin)
            {
                //				Debug.Log ("ShowChipResult - " + _isWin + " - " + _gold);
                txtChipResult.text = "+" + Utilities.GetStringMoneyByLong(_gold);
                txtChipResult.color = Color.yellow;


            }
            else
            {
                txtChipResult.text = Utilities.GetStringMoneyByLong(_gold);
                txtChipResult.color = Color.gray;
            }
            rectTxtChipResult.DOAnchorPos(new Vector2(0, 100), TIME_GOLD_RESULT).OnComplete(() => {
                txtChipResult.gameObject.SetActive(false);
            });

            //			txtChipResult.DOFade (.25f, TIME_GOLD_RESULT);
        }

        public void ShowValueHandCards(XTType _type)
        {
            imgValueHandCards.sprite = XTDataHelper.instance.GetSprRow(_type);
            imgValueHandCards.gameObject.SetActive(true);
        }

        public void HideValueHandCards()
        {
            imgValueHandCards.gameObject.SetActive(false);
        }

        public void SetEnableWin(bool _enable)
        {
            //			Win.SetActive (_enable);
        }
        public void SetEnableLose(bool _enable)
        {
            //			Lose.SetActive (_enable);
        }

        public void ShowAction(XTAction _action)
        {
            //			txtAction.text = _name;
            //			txtAction.gameObject.SetActive (true);

            imgAction.gameObject.SetActive(true);
            txtAction.text = XTDataHelper.instance.GetTextAction(_action);
            txtAction.color = GetColorAction(_action);
            //			imgAction.sprite = XTDataHelper.instance.GetSprRaise (_action);


        }

        Color GetColorAction(XTAction _action)
        {

            switch (_action)
            {
                case XTAction.Call:
                    return Color.cyan;
                case XTAction.Check:
                    return Color.cyan;
                case XTAction.Fold:
                    return Color.red;
                case XTAction.Raise:
                    return Color.green;
                case XTAction.RaiseAll:
                    return Color.red;
                case XTAction.RaiseMulti2:
                    return Color.green;
                case XTAction.RaisePart4:
                    return Color.green;
                case XTAction.RaisePart2:
                    return Color.green;
            }
            return Color.white;
        }
        public void HideAction()
        {
            //			txtAction.gameObject.SetActive (false);
            imgAction.gameObject.SetActive(false);
        }


        public void NemBomVaoEnemy(int vitri)
        {
            if (this.imgAvatar.gameObject.activeInHierarchy == true)
            {
                Sprite Ava = this.imgAvatar.sprite;
                Sprite Khung = this.imgBorderAvt.sprite;
                string ten = this.txtDisName.text;
                string vip = this.Vip;
                string level = this.txtLevel.text;
                string chip = this.txtChip.text;
                string gem = Utilities.GetStringMoneyByLong(this.Gem);

                Debug.Log("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem + " sfsId = " + this.sfsId);
                InfoEnemy.Init(userId, Ava, Khung, ten, vip, level, chip, gem, vitri);
            }
        }


        #region COUNTDOWN

        [SerializeField]
        GameObject panelCoutDown;
        [SerializeField]
        Image imgTime;

        onCallBack onCompleteCountDown;

        Coroutine countDownThread;

        float timeCurrent, timeTotal;
        int part;

        bool isStart = false;

        void Update()
        {
            if (isStart)
            {
                int _time = (int)timeCurrent;
                timeCurrent -= Time.deltaTime;

                if (_time < 5)
                {
                    if (_time > timeCurrent)
                    {
                        SoundManager.PlaySound(SoundManager.COUNT_DOWN);
                    }
                }

                imgTime.fillAmount = Mathf.Lerp(fillFrom, fillTo, timeCurrent / timeTotal);

                if (part != (int)timeCurrent)
                {
                    part = (int)timeCurrent;
                }

                if (timeCurrent < 0)
                {
                    OnCompleteCountDown();
                }
            }
        }
        Tween tweenColor;
        private const float fillFrom = 0.1f, fillTo = 0.9f;
        public void ShowCountDown(int _timeTotal, onCallBack _onComplete = null)
        {
            panelCoutDown.SetActive(true);
            imgTime.fillAmount = fillTo;
            //imgTime.color = Color.green;

            if (tweenColor != null)
                tweenColor.Pause();

            //tweenColor = imgTime.DOColor (Color.red, _timeTotal);
            //tweenColor.Play ();

            //			imgTime.color = Color.white;
            timeTotal = timeCurrent = _timeTotal + 1;

            onCompleteCountDown = _onComplete;

            isStart = true;
        }
        public void ShowCountDown(int _timeCurrent, int _timeTotal, onCallBack _onComplete = null)
        {
            panelCoutDown.SetActive(true);
            imgTime.fillAmount = Mathf.Lerp(fillFrom, fillTo, _timeCurrent / _timeTotal);
            timeTotal = _timeTotal;
            timeCurrent = _timeCurrent;

            onCompleteCountDown = _onComplete;

            isStart = true;
        }

        public void OnCompleteCountDown()
        {
            isStart = false;

            if (countDownThread != null)
            {
                StopCoroutine(countDownThread);
                countDownThread = null;
            }

            panelCoutDown.SetActive(false);
        }

        IEnumerator CountDownThread(int _second)
        {
            panelCoutDown.SetActive(true);

            timeCurrent = timeTotal = _second;

            //		for (int i = _second; i > 0; i--) {
            //
            //			txtTime.text = i.ToString ();
            //
            //			yield return new WaitForSeconds (1);
            //		}
            yield return new WaitForSeconds(_second);

            countDownThread = null;
            onCompleteCountDown();

            panelCoutDown.SetActive(false);

        }

        #endregion
    }
}