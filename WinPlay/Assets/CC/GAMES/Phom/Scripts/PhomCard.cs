﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using DG.Tweening;

public class PhomCard : MonoBehaviour {

    // Info
    public int Id { get; set; }
    public int Value { get; set; }
    public CardType Type { get; set; }
    private bool IsAnimatingNotice;
    
    // UI
    public Image NoticeMarkImg;
    public Image MeldMarkImg;
    public Image BackgroundImg;
    public Image ValueImg;
    public Image SmallTypeImg;
    public Image BigTypeImg;
    public Image BackImg;
    public Image MaskImg;

    // Color set
    private List<Color32> colors;

    void Awake()
    {
        colors = new List<Color32>();
        colors.Add(PhomColor.MeldBorderColor1);
        colors.Add(PhomColor.MeldBorderColor2);
        colors.Add(PhomColor.MeldBorderColor3);
    }

    public void OnClick()
    {
        if (PhomRoomController.Instance.IsAutoPickingCard)
        {
            print("IsAutoPickingCard");
            return;
        }

        PhomRoomController.Instance.PlayerSlot.gameObject.SendMessage(
            "PickCard", this, SendMessageOptions.DontRequireReceiver);
    }

    public void ShowMeldMark(int meldOrder)
    {
        MeldMarkImg.color = colors[meldOrder - 1];
        MeldMarkImg.gameObject.SetActive(true);
    }

    public void HideMeldMark()
    {
        MeldMarkImg.gameObject.SetActive(false);
    }

    public void SetMaskColor(Color32 color)
    {
        transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = color;
    }

    public IEnumerator AnimateGoUpAndDownNoticeMark()
    {
        float timeLeft = PhomConst.TURN_TIME * 2;
        while (timeLeft > 0)
        {
            timeLeft -= 0.6f;
            NoticeMarkImg.gameObject.transform.DOLocalMoveY(190f, 0.3f, true);
            yield return new WaitForSeconds(0.3f);
            NoticeMarkImg.gameObject.transform.DOLocalMoveY(160f, 0.3f, true);
            yield return new WaitForSeconds(0.3f);
        }
    }
}