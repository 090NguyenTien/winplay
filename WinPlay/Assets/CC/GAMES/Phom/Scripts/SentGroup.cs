﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SentGroup {

    public int TakenSlotId { get; set; }
    public List<PhomCard> UpperSentCards { get; set; }
    public List<PhomCard> LowerSentCards { get; set; }
    public List<PhomCard> NewUpperLaidDownCards { get; set; }
    public List<PhomCard> NewLowerLaidDownCards { get; set; }

    public SentGroup()
    {
        TakenSlotId = PhomConst.INVALID_ID;
        UpperSentCards = new List<PhomCard>();
        LowerSentCards = new List<PhomCard>();
        NewUpperLaidDownCards = new List<PhomCard>();
        NewLowerLaidDownCards = new List<PhomCard>();
    }
}