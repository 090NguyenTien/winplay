﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;

public class TLMNEnemyPlayer : TLMNBasePlayer
{
    private const int CARD_WIDTH = 56;
    private const int CARD_HEIGHT = 40;

    public GameObject enemyInfo;

    [HideInInspector]
    public bool actived = false;
    [SerializeField]
    private Sprite inviteIcon;
    private Vector3 moneyTextPos;

    [SerializeField]
    PopupInfoEnemy InfoEnemy;
    [SerializeField]
    public int Id_ViTriBom;

    void Awake()
    {
        avatar.gameObject.GetComponent<Button>().onClick.AddListener(() => SendInviteRequest());
        moneyTextPos = moneyText.transform.localPosition;
    }

    public void UpdateTime(float newValue)
    {
        timerImage.fillAmount = newValue;
    }

    private void SendInviteRequest()
    {
        if (!actived)
            controller.SendMessage("BtnInviteOnClick", SendMessageOptions.DontRequireReceiver);
    }

    Tween tweenColor;
    public override void CountDownTime()
    {
        iTween.Stop(gameObject);
        timerBackground.gameObject.SetActive(true);
        timerImage.fillAmount = fillTo;
        focusPlayer.SetActive(true);

        //timerImage.color = Color.green;

        if (tweenColor != null)
            tweenColor.Pause();

        //tweenColor = timerImage.DOColor (Color.red, 15f);
        tweenColor.Play();

        TLMNRoomController.Instance.EnemyTurnPlaying();
        Hashtable hash = new Hashtable();
        hash.Add("from", fillTo);
        hash.Add("to", fillFrom);
        hash.Add("time", 15f);
        hash.Add("onupdate", "UpdateTime");
        hash.Add("oncomplete", "StopCountDownTime");
        iTween.ValueTo(gameObject, hash);
    }

    public override void StopCountDownTime()
    {
        focusPlayer.SetActive(false);
        iTween.Stop(gameObject, true);
        timerBackground.gameObject.SetActive(false);
    }

    public override void SendCard(List<Card> hitCards)
    {
        foreach (Card card in hitCards)
        {
            card.setBackCardActive(false);
            card.transform.position = sampleCard.transform.position;
        }
        TLMNUtilities.SortCardsList(hitCards);
        controller.SortSendCards(hitCards);
    }

    public override void SetInfo(string[] info)
    {
        enemyInfo.SetActive(true);
        sfsId = int.Parse(info[0]);
        userId = info[1];
        chip = long.Parse(info[3]);

        string userName = info[2];
        string avatar = info[4];
        string vip_collect = info[5];
        this.level = int.Parse(info[6]);

        string Bor_Avatar = info[7];

        //txtLevel.text = info[6];
        Debug.LogWarning("txtLevel.text=====" + info[6] + "            vip collect ======== " + vip_collect + "  bor_avatar =========== " + Bor_Avatar);

      //  this.gem = long.Parse(info[8]);

        this.Vip = DataHelper.GetStringVipByStringVipPoint(vip_collect);
        txtLevel.text = DataHelper.GetStringVipByStringVipPoint(Vip);//doi level thanh vip ben Chinh//
        int id_Bor = int.Parse(Bor_Avatar);

        if (id_Bor != -1)
        {
            if (DataHelper.GetBoderAvatar(Bor_Avatar) != null)
            {
                this.avatarBoder.sprite = DataHelper.GetBoderAvatar(Bor_Avatar);
                this.avatarBoder.gameObject.SetActive(true);

                this.avatarBoder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                this.avatarBoder.gameObject.SetActive(false);
                this.avatarBoder_Defaut.gameObject.SetActive(true);
            }

        }
        else
        {
            this.avatarBoder.gameObject.SetActive(false);
            this.avatarBoder_Defaut.gameObject.SetActive(true);
        }
        avatarBoder.transform.localScale = new Vector2(1.1f, 1.1f);
        displayName.text = userName;
        gold.text = Utilities.GetStringMoneyByLong(chip);

        this.avatar.gameObject.SetActive(true);
        //this.avatar.transform.GetChild (0).gameObject.SetActive (true);
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        if (MyInfo.SFS_ID == sfsId)
        {
            if (loginType != LoginType.Facebook)
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
            else
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
        }
        else
        {
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
            else
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
        }


        // this.avatarBoder_Defaut.gameObject.SetActive (true);
        //this.avatarBoder.sprite = DataHelper.GetVip (int.Parse (vip));
        actived = true;
        if (inviteBtn != null)
            inviteBtn.gameObject.SetActive(false);
    }

    public void SortCard()
    {
        string name = gameObject.name;
        Vector3 origin = cardGroup.transform.position;
        float distanceX = CARD_WIDTH - 20;
        float x;

        if (name.Equals("Enemy1"))
            x = origin.x - cardGroup.transform.childCount * distanceX;
        else if (name.Equals("Enemy2"))
            x = origin.x - cardGroup.transform.childCount / 2 * distanceX;
        else
            x = origin.x;

        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);
        hash.Add("islocal", true);

        foreach (Transform tf in cardGroup.transform)
        {
            hash["position"] = new Vector3(x, origin.y);
            iTween.MoveTo(tf.gameObject, hash);
            Card cardShow = tf.GetComponent<Card>();

            if (cardShow != null)
            {
                cardShow.setBackCardActive(false);
            }

            x += distanceX;
        }
    }

    public override void Reset()
    {
        moneyText.text = "";
        moneyText.gameObject.transform.localPosition = moneyTextPos;
        immediateWinImage.gameObject.SetActive(false);
        rankImage.enabled = false;
        sampleCard.SetActive(false);
        Vector3 deckPosition = new Vector3(-400, -400);
        List<Transform> cards = new List<Transform>();
        foreach (Transform tf in cardGroup.transform)
        {
            cards.Add(tf);
        }
        foreach (Transform tf in cards)
        {
            tf.SetParent(controller.deck.transform);
            tf.localScale = Vector3.one;
            tf.localPosition = deckPosition;
        }
    }

    public void ExitRoom()
    {
        actived = false;
        enemyInfo.SetActive(false);
        sampleCard.SetActive(false);
        avatar.sprite = inviteIcon;
		avatar.gameObject.SetActive(false);
        avatarBoder_Defaut.gameObject.SetActive(false);
        avatarBoder.gameObject.SetActive(false);
        if (inviteBtn != null)
			inviteBtn.gameObject.SetActive(true);
        displayName.text = "";
        moneyText.text = "";
        gold.text = "";
        sfsId = -1;
    }




    public void NemBomVaoEnemy(int vitri)
    {
		if (actived == true)
        {

            Sprite Ava = this.avatar.sprite;
            Sprite Khung = this.avatarBoder.sprite;
            string ten = this.displayName.text;
            string vip = this.Vip;
            string level = this.level.ToString();
            string chip = Utilities.GetStringMoneyByLong(this.chip);
            string gem = Utilities.GetStringMoneyByLong(this.gem);

            Debug.Log("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem);
            InfoEnemy.Init(userId, Ava, Khung, ten, vip, level, chip, gem, vitri);

        }
        //  controller.NemBoomControll(0, 1);

    }

    //    private IEnumerator SetAvatar(string avatarUrl)
    //    {
    //        Texture2D mainImage;
    //        WWW www = new WWW(avatarUrl);
    //        yield return www;
    //
    //        mainImage = www.texture;
    //        Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    //
    //        avatar.sprite = spr;
    //    }
}
