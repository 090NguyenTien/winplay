﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class TLMNUtilities
{
    public static TLMNCardType GetCardType(List<Card> cards)
    {
        if (cards == null)
            return TLMNCardType.NULL;
        if (cards.Count >= 3 && IsStraight(cards))
            return TLMNCardType.STRAIGHT;

        switch (cards.Count)
        {
            case 0:
                return TLMNCardType.NULL;
            case 1:
                return TLMNCardType.SINGLE;
            case 2:
                if (IsPair(cards))
                    return TLMNCardType.PAIR;
                break;
            case 3:
                if (IsThreeOfAKind(cards))
                    return TLMNCardType.THREE_OF_A_KIND;
                else if (IsStraight(cards))
                    return TLMNCardType.STRAIGHT;
                break;
            case 4:
                if (IsFourOfAKind(cards))
                    return TLMNCardType.FOUR_OF_A_KIND;
                else if (IsStraight(cards))
                    return TLMNCardType.STRAIGHT;
                break;
            case 6:
                if (IsThreePairSequence(cards))
                    return TLMNCardType.THREE_PAIR_SEQUENCE;
                else if (IsStraight(cards))
                    return TLMNCardType.STRAIGHT;
                break;
            case 8:
                if (IsFourPairSequence(cards))
                    return TLMNCardType.FOUR_PAIR_SEQUENCE;
                break;
        }
        return TLMNCardType.NULL;
    }

    public static bool IsPair(List<Card> cards)
    {
        if (cards.Count != 2)
            return false;
        if (cards[0].Value == cards[1].Value)
            return true;
        return false;
    }

    public static bool IsThreeOfAKind(List<Card> cards)
    {
        if (cards.Count != 3)
            return false;
        if (cards[0].Value == cards[1].Value && cards[1].Value == cards[2].Value)
            return true;
        return false;
    }

    public static bool IsFourOfAKind(List<Card> cards)
    {
        if (cards.Count != 4)
            return false;
        if (cards[0].Value == cards[1].Value && cards[1].Value == cards[2].Value && cards[2].Value == cards[3].Value)
            return true;
        return false;
    }

    public static bool IsStraight(List<Card> cards)
    {
        if (cards.Count < 3)
            return false;
        SortCardsList(cards);
        if (cards[cards.Count - 1].Value > 14)
            return false;
        Card temp = cards[0];
        for(int i = 1; i < cards.Count - 1; i++)
        {
            if (temp.Value + 1 == cards[i].Value)
                temp = cards[i];
            else
                return false;
        }
        return true;
    }

    public static bool IsThreePairSequence(List<Card> cards)
    {
        if (cards.Count != 6)
            return false;
        SortCardsList(cards);
        if (cards[0].Value + 1 != cards[2].Value || cards[2].Value + 1 != cards[4].Value)
            return false;
        for(int i = 0; i < 6; i+=2)
        {
            if (cards[i].Value != cards[i + 1].Value)
                return false;
        }
        return true;
    }

    public static bool IsFourPairSequence(List<Card> cards)
    {
        if (cards.Count != 8)
            return false;
        SortCardsList(cards);
        if (cards[0].Value + 1 != cards[2].Value || cards[2].Value + 1 != cards[4].Value || cards[4].Value + 1 != cards[6].Value)
            return false;
        for (int i = 0; i < 8; i += 2)
        {
            if (cards[i].Value != cards[i + 1].Value)
                return false;
        }
        return true;
    }

    //hint ho tro nguoi choi chon bai
    public static void showHintCard(Dictionary<int, Card> playerCards, List<Card> listCardPlaying, bool isNewRound)
    {
        TLMNCardType cardTypePlaying = GetCardType(listCardPlaying); //loai card dang danh tren ban

        foreach (KeyValuePair<int, Card> item in playerCards)
        {
            item.Value.setCanUse(false);
        }

        if (isNewRound == true)//
        {
            foreach (KeyValuePair<int, Card> item in playerCards)
            {
                item.Value.setCanUse(true);
            }
            //Debug.LogError("toi luot minh ma ra luot moi ne");
        }
        else//ko phai luot moi
        {
            //Debug.LogError("toi luot minh danh roi nek");

            if (cardTypePlaying == TLMNCardType.SINGLE)
            {
                foreach (KeyValuePair<int, Card> item in playerCards)
                {
                    if (item.Value.Value > listCardPlaying[0].Value || (item.Value.Value == listCardPlaying[0].Value && (int)item.Value.Type > (int)listCardPlaying[0].Type))//bai nho hon
                    {
                        item.Value.setCanUse(true);
                    }
                }

                if (listCardPlaying[0].Value == 15) // la heo
                {
                    List<Card> lstFourOfAKind = GetAllFourOfAKindHint(playerCards.Values.ToList());
                    foreach (Card item in lstFourOfAKind)
                    {
                        item.setCanUse(true);
                    }

                    showPairSequence(playerCards.Values.ToList(), 3);
                }

            }
            else if (cardTypePlaying == TLMNCardType.PAIR)
            {
                listCardPlaying = listCardPlaying.OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();

                List<Card> lstPairCard = GetAllPairHint(playerCards.Values.ToList(), listCardPlaying.Count, listCardPlaying[listCardPlaying.Count - 1]);

                foreach (Card item in lstPairCard)
                {
                    item.setCanUse(true);
                }
                if (listCardPlaying[0].Value == 15)//la doi heo
                {
                    List<Card> lstFourOfAKind = GetAllFourOfAKindHint(playerCards.Values.ToList());
                    foreach (Card item in lstFourOfAKind)
                    {
                        item.setCanUse(true);
                    }

                    listCardPlaying = listCardPlaying.OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();

                    List<Card> lst4PairSequenceCard = GetAllPairHint(playerCards.Values.ToList(), 4, listCardPlaying[0]);

                    foreach (Card item in lst4PairSequenceCard)
                    {
                        item.setCanUse(true);
                    }
                }
                
            }
            else if (cardTypePlaying == TLMNCardType.THREE_PAIR_SEQUENCE || cardTypePlaying == TLMNCardType.FOUR_PAIR_SEQUENCE)
            {
                showPairSequence(playerCards.Values.ToList(), listCardPlaying.Count);
                if (cardTypePlaying == TLMNCardType.THREE_PAIR_SEQUENCE)//neu 3 doi thong  thi check them case tu qui
                {
                    listCardPlaying = listCardPlaying.OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();

                    List<Card> lstPairCard = GetAllPairHint(playerCards.Values.ToList(), 4, listCardPlaying[0]);

                    foreach (Card item in lstPairCard)
                    {
                        item.setCanUse(true);
                    }
                }
            }
            else if (cardTypePlaying == TLMNCardType.THREE_OF_A_KIND || cardTypePlaying == TLMNCardType.FOUR_OF_A_KIND)
            {
                listCardPlaying = listCardPlaying.OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();

                List<Card> lstPairCard = GetAllPairHint(playerCards.Values.ToList(), listCardPlaying.Count, listCardPlaying[listCardPlaying.Count - 1]);

                foreach (Card item in lstPairCard)
                {
                    item.setCanUse(true);
                }

                if (cardTypePlaying == TLMNCardType.FOUR_OF_A_KIND)//la tu quy => check them case 4 doi thong an tu quy
                {
                    List<Card> lst4PairSequenceCard = GetAllPairHint(playerCards.Values.ToList(), 4, listCardPlaying[0]);

                    foreach (Card item in lst4PairSequenceCard)
                    {
                        item.setCanUse(true);
                    }
                }

            }
            else if (cardTypePlaying == TLMNCardType.STRAIGHT)
            {
                listCardPlaying = listCardPlaying.OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();
                Card cardBiggest = listCardPlaying[listCardPlaying.Count - 1];
                List<List<Card>> allStraight = GetAllStraight(playerCards.Values.ToList(), listCardPlaying.Count);
                int numOfStraight = listCardPlaying.Count;
                for (int i = 0; i < allStraight.Count; i++)
                {
                    Debug.LogWarning("max straight card: " + allStraight[i][numOfStraight - 1].Value.ToString() + "   " + allStraight[i][numOfStraight - 1].Type.ToString());

                    if (allStraight[i][numOfStraight - 1].Value >  cardBiggest.Value || 
                        (allStraight[i][numOfStraight - 1].Value >= cardBiggest.Value && (int)allStraight[i][numOfStraight - 1].Type > (int)cardBiggest.Type) )
                    {
                        for (int k = 0; k < numOfStraight; k++)
                        {
                            allStraight[i][k].setCanUse(true);
                        }
                    }
                }
            }

        }
    }

    private static void showPairSequence(List<Card> playerCard, int sequenceLeng)
    {
        List<List<Card>> allPairSequence = GetAllPairSequence(playerCard, sequenceLeng);
        for (int i = 0; i < allPairSequence.Count; i++)
        {
            for (int k = 0; k < allPairSequence[i].Count; k++)
            {
                allPairSequence[i][k].setCanUse(true);
            }
        }
    }

    private static List<Card> GetAllFourOfAKindHint(List<Card> playerCard)
    {
        List<Card> result = new List<Card>();
        for (int i = 3; i < 14 + 1; i++) // tinh tu 3 den at
        {
            List<Card> lstCardFourKind = playerCard.Where(x => x.Value == i).ToList();
            if (lstCardFourKind.Count >= 4)
            {
                for (int k = 0; k < lstCardFourKind.Count; k++)
                {
                    result.Add(lstCardFourKind[k]);
                }
            }
        }

        return result;
    }

    private static List<Card> GetAllPairHint(List<Card> playerCard, int numPairSequence, Card cardMax)
    {
        List<Card> result = new List<Card>();
        for (int i = cardMax.Value; i <  15 + 1; i++)//lay tu heo dem len
        {
            List<Card> lstPair = playerCard.Where(x => x.Value == i).OrderBy(x => x.Value).ThenBy(x => (int)x.Type).ToList();//lay ra so luong cay gia tri <i> co trong bai
            if (lstPair.Count >= numPairSequence)// >= so luong can lay
            {
                if (lstPair[lstPair.Count - 1].Value > cardMax.Value || (lstPair[lstPair.Count - 1].Value == cardMax.Value && (int)lstPair[lstPair.Count - 1].Type > (int)cardMax.Type))
                {
                    for (int k = 0; k < lstPair.Count; k++)
                    {
                        result.Add(lstPair[k]);
                    }
                }
            }
        }
        return result;
    }

    public static bool CheckSendAbility(Dictionary<int, Card> playerCards, List<Card> cardsBefore, List<Card> cardsAfter, bool isNewRound, bool isFirstGame)
    {
        TLMNCardType cardTypeBefore = GetCardType(cardsBefore);
        TLMNCardType cardTypeAfter = GetCardType(cardsAfter);

        if (isNewRound)
        {
            if (isFirstGame)
            {
                int minCardId = cardsAfter[0].Id;
                foreach(KeyValuePair<int, Card> pair in playerCards)
                {
                    if (pair.Value.Value == playerCards[minCardId].Value)
                    {
                        if ((int)pair.Value.Type < (int)playerCards[minCardId].Type)
                            minCardId = pair.Key;
                    }
                    else if (pair.Value.Value < playerCards[minCardId].Value)
                        minCardId = pair.Key;
                }
                SortCardsList(cardsAfter);
                if (cardTypeAfter != TLMNCardType.NULL && playerCards[minCardId] == cardsAfter[0])
                    return true;
            }
            else if (cardTypeAfter != TLMNCardType.NULL)
                return true;
            else
                return false;
        }
        if (cardTypeBefore != cardTypeAfter)
        {
            if(cardTypeBefore == TLMNCardType.SINGLE)
            {
                if (cardsBefore[0].Value != 15)
                    return false;
                if (cardTypeAfter == TLMNCardType.THREE_PAIR_SEQUENCE || cardTypeAfter == TLMNCardType.FOUR_OF_A_KIND
                    || cardTypeAfter == TLMNCardType.FOUR_PAIR_SEQUENCE)
                    return true;
            }
            else if(cardTypeBefore == TLMNCardType.PAIR)
            {
                if (cardsBefore[0].Value != 15)
                    return false;
                if (cardTypeAfter == TLMNCardType.FOUR_OF_A_KIND || cardTypeAfter == TLMNCardType.FOUR_PAIR_SEQUENCE)
                    return true;
            }
            else if (cardTypeBefore == TLMNCardType.THREE_PAIR_SEQUENCE)
            {
                if (cardTypeAfter == TLMNCardType.FOUR_OF_A_KIND || cardTypeAfter == TLMNCardType.FOUR_PAIR_SEQUENCE)
                    return true;
            }
            else if(cardTypeBefore == TLMNCardType.FOUR_OF_A_KIND)
            {
                if (cardTypeAfter == TLMNCardType.FOUR_PAIR_SEQUENCE)
                    return true;
            }
        }
        else
        {
            if (cardTypeBefore == TLMNCardType.STRAIGHT && cardsBefore.Count != cardsAfter.Count)
                return false;

            SortCardsList(cardsBefore);
            SortCardsList(cardsAfter);

            Card lastBefore = cardsBefore[cardsBefore.Count - 1];
            Card lastAfter = cardsAfter[cardsAfter.Count - 1];

            if (lastBefore.Value < lastAfter.Value)
            {
                return true;
            }
            else if (lastBefore.Value == lastAfter.Value && (int)lastBefore.Type < (int)lastAfter.Type)
            {
                return true;
            }
        }
        return false;
    }

    public static List<List<Card>> GetAllPairSequence(List<Card> playerCard, int sequenceLeng)
    {
        List<List<Card>> resultAllPair = new List<List<Card>>();

        for (int i = 0; i < playerCard.Count; i++)
        {
            List<Card> lstPair = getPairSequence(playerCard, playerCard[i], sequenceLeng);
            if (lstPair != null)
            {
                resultAllPair.Add(lstPair);
            }
        }
        return resultAllPair;
    }

    private static List<Card> getPairSequence(List<Card> playerCard, Card minCard, int sequenceLeng)
    {
        //playerCard was sortedawdwa
        List<Card> result = new List<Card>();
        if (minCard.Value <= 2 || minCard.Value >= 15)//nho hon 3 va la con heo
        {
            return null;
        }

        int curPairValue = minCard.Value;
        for (int i = 0; i < sequenceLeng; i++)
        {
            List<Card> lstPair = playerCard.Where(x => x.Value == curPairValue).ToList();
            if (lstPair.Count >= 2)//neu co 2 la
            {
                for (int k = 0; k < 2; k++)//lay 2 la thoi
                {
                    result.Add(lstPair[k]);
                }
                ++curPairValue;//tang len xet cay tiep theo
            }
            else//ko du doi
            {
                break;
            }
        }

        if (result.Count / 2 == sequenceLeng)
        {
            return result;
        }
        else
        {
            return null;
        }
    }

    public static List<List<Card>> GetAllStraight(List<Card> playerCard, int straightLeng)
    {
        List<List<Card>> resultAllStraight = new List<List<Card>>();

        for (int i = 0; i < playerCard.Count; i++)
        {
           List< List<Card>> listStraight = getStraight(playerCard, playerCard[i], straightLeng);
            if (listStraight != null)
            {
                for (int k = 0; k < listStraight.Count; k++)
                {
                    resultAllStraight.Add(listStraight[k]);
                }
               
            }
        }
        //Debug.LogError("co tat ca " + resultAllStraight.Count.ToString() + "  sanh");
        return resultAllStraight;
    }

    private static List<List<Card>> getStraight(List<Card> playerCard, Card minCard, int straightLeng)
    {
        //playerCard was sorted
        Dictionary<int, List<Card>> dicStraingCardPrepair = new Dictionary<int, List<Card>>();//danh sach cac card co the ghep sanh

        if (minCard.Value <= 2 || minCard.Value + straightLeng > 15)//nho hon 3 va la con heo
        {
            return null;
        }

        int preIndex = playerCard.IndexOf(minCard);

        int minCardValue = minCard.Value;

        List<Card> lstFirstcard = new List<Card>();
        lstFirstcard.Add(minCard);
        dicStraingCardPrepair.Add(0, lstFirstcard);

        
        for (int i = 1; i < straightLeng; i++)//chay tu 1 vi 0 add thang luon goi
        {
            ++minCardValue;//tang value len 1 de tim cay co the ghep tiep
            List<Card> lstNextCard = playerCard.Where(x => x.Value == minCardValue).ToList();
            if (lstNextCard.Count > 0)
            {
                dicStraingCardPrepair.Add(i , lstNextCard);
            }
            else
            {
                break;//ko co cay tiep theo dung luon
            }
        }
        //for (int i = preIndex + 1; i < playerCard.Count; i++)
        //{
        //    if (straightCount >= straightLeng)
        //    {
        //        break;
        //    }
        //    if (playerCard[preIndex].Value == playerCard[i].Value)//neu cung gia tri thi bo qua luon
        //    {
        //        if (minCard.Value != playerCard[i].Value)//cay o giua co the ghep sanh dc nua
        //        {
        //            if (dicEqualCard.ContainsKey(straightCount) == false)//chua co all
        //            {
        //                List<Card> lstEqualcard = new List<Card>();
        //                lstEqualcard.Add(playerCard[i]);
        //                dicEqualCard.Add(straightCount, lstEqualcard);
        //            }
        //            else
        //            {
        //                dicEqualCard[straightCount].Add(playerCard[i]);
        //            }
        //        }
        //        continue;
        //    }
        //    else if (playerCard[preIndex].Value + 1 == playerCard[i].Value && playerCard[i].Value < 15)//2 cay lien ke nek && ko phai la heo
        //    {
        //        preIndex = i;
        //        result.Add(playerCard[i]);
        //        ++straightCount;
        //    }
        //    else //cach xa nhau qua
        //    {
        //        break;
        //    }
        //}

        if (dicStraingCardPrepair.Count == straightLeng)//co the ghep sanh
        {
            List<int> lstCount = new List<int>();
            for (int i = 0; i < straightLeng; i++)
            {
                lstCount.Add(dicStraingCardPrepair[i].Count);
            }

            List<List<Card>> result = new List<List<Card>>();
            List<Card> lstStraight;
            int countStraightDown = straightLeng - 1;

            int countWhile = 100;
            while (lstCount[0] > 0 && countWhile > 0)
            {
                --countWhile;
                lstStraight = new List<Card>();
                for (int i = 0; i < straightLeng; i++)
                {
                    int cardIndex = lstCount[i] - 1;
                    
                    lstStraight.Add(dicStraingCardPrepair[i][cardIndex]);
                }
                //tang giam cay cuoi
                bool isDowned = false;
                for (int i = straightLeng - 1; i > countStraightDown; i--)
                {
                    if (lstCount[i] > 1)
                    {
                        lstCount[i] = lstCount[i] - 1;
                        isDowned = true;
                        if (lstCount[i] <= 1)
                        {
                            for (int k = i + 1; k < straightLeng; k++)
                            {
                                lstCount[k] = dicStraingCardPrepair[k].Count;
                            }
                        }
                        break;
                    }
                }
                if (isDowned == false)
                {
                    for (int i = countStraightDown; i > 0; i--)
                    {
                        if (lstCount[i] > 1)
                        {
                            countStraightDown = i;
                            lstCount[countStraightDown] = lstCount[countStraightDown] - 1;

                            for (int k = countStraightDown + 1; k < straightLeng; k++)
                            {
                                lstCount[k] = dicStraingCardPrepair[k].Count;
                            }
                            isDowned = true;
                            break;
                        }
                    }
                    if (isDowned == false)
                    {
                        lstCount[0] = 0;
                       
                    }
                    
                    
                }
                else
                {
                    //do nothing
                }


                //if (i > 1)
                //    {
                //        lstCount[i - 1] = lstCount[i - 1] - 1;
                //        for (int k = i; k < straightLeng; k++)
                //        {
                //            lstCount[k] = dicStraingCardPrepair[k].Count;
                //        }
                //    }
                //    else
                //    {
                //        lstCount[i] = lstCount[i] - 1;
                //        for (int k = i + 1; k < straightLeng; k++)
                //        {
                //            lstCount[k] = dicStraingCardPrepair[k].Count;
                //        }
                //    }



                result.Add(lstStraight);

            }
            return result;
        }
        else
        {
            return null;
        }
    }
    public static void SortCardsList(List<Card> cards)
    {
        cards.Sort(delegate (Card x, Card y)
        {
            if (x.Value == y.Value)
                return ((int)x.Type).CompareTo((int)y.Type);
            return x.Value.CompareTo(y.Value);
        });
    }

    public static bool ContainFourPairInSequence(this List<Card> cards)
    {
        if (cards.Count < 8)
            return false;
        List<int> pairValue = new List<int>();
        List<Card> passCards = new List<Card>();
        foreach(Card card in cards)
        {
            if (passCards.Contains(card))
                continue;
            int count = 0;
            foreach(Card _card in cards)
            {
                if (card == _card || passCards.Contains(_card))
                    continue;
                if (card.Value == _card.Value)
                {
                    passCards.Add(_card);
                    count++;
                }
            }
            if (count >= 1)
                pairValue.Add(card.Value);
            passCards.Add(card);
        }
        if (pairValue.Count < 4)
            return false;
        foreach(int i in pairValue)
        {
            if (pairValue.Contains(i + 1) && pairValue.Contains(i + 2) && pairValue.Contains(i + 3))
                return true;
        }
        return false;
    }

    public static bool ContainFourOfAKind(List<Card> cards)
    {
        foreach (Card card in cards)
        {
            int count = 0;
            foreach (Card _card in cards)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    count++;
            }
            if (count == 3)
                return true;
        }
        return false;
    }

    public static bool ContainThreePairInSequence(List<Card> cards)
    {
        if (cards.Count < 6)
            return false;
        List<int> pairValue = new List<int>();
        List<Card> passCards = new List<Card>();
        foreach (Card card in cards)
        {
            if (passCards.Contains(card))
                continue;
            int count = 0;
            foreach (Card _card in cards)
            {
                if (card == _card || passCards.Contains(_card))
                    continue;
                if (card.Value == _card.Value)
                {
                    passCards.Add(_card);
                    count++;
                }
            }
            if (count >= 1)
                pairValue.Add(card.Value);
            passCards.Add(card);
        }
        if (pairValue.Count < 3)
            return false;
        foreach (int i in pairValue)
        {
            if (pairValue.Contains(i + 1) && pairValue.Contains(i + 2))
                return true;
        }
        return false;
    }

    public static bool ContainStraight(List<Card> cards)
    {
        foreach(Card card in cards)
        {
            if (card.Value == 15)
                continue;
            int count = 0;
            Card previousCard = card;
            foreach(Card _card in cards)
            {
                if (card == _card)
                    continue;
                if (previousCard.Value + 1 != _card.Value || _card.Value == 15)
                    break;
                count++;
                previousCard = _card;
            }
            if (count >= 2)
                return true;
        }
        return false;
    }

    public static bool ContainThreeOfAKind(List<Card> cards)
    {
        foreach(Card card in cards)
        {
            int count = 0;
            foreach (Card _card in cards)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    count++;
            }
            if (count >= 3)
                return true;
        }
        return false;
    }

    public static bool ContainPair(List<Card> cards)
    {
        foreach(Card card in cards)
        {
            int count = 0;
            foreach(Card _card in cards)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    count++;
            }
            if (count >= 1)
                return true;
        }
        return false;
    }

    public static List<Card> FindFourOfAKind(List<Card> cards)
    {
        List<Card> result = new List<Card>();
        List<int> valueOfFour = new List<int>();
        foreach(Card card in cards)
        {
            int count = 0;
            foreach(Card _card in cards)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    count++;
            }
            if (count == 3)
                valueOfFour.Add(card.Value);
        }
        foreach(Card card in cards)
        {
            if (valueOfFour.Contains(card.Value))
                result.Add(card);
        }
        return result;
    }

    public static List<Card> FindThreePairInSequence(List<Card> cards)
    {
        List<Card> result = new List<Card>();
        List<int> pairValue = new List<int>();
        List<Card> passCards = new List<Card>();
        List<int> pairOfThree = new List<int>();
        foreach (Card card in cards)
        {
            if (passCards.Contains(card))
                continue;
            int count = 0;
            foreach (Card _card in cards)
            {
                if (card == _card || passCards.Contains(_card))
                    continue;
                if (card.Value == _card.Value)
                {
                    passCards.Add(_card);
                    count++;
                }
            }
            if (count >= 1)
                pairValue.Add(card.Value);
            passCards.Add(card);
        }
        foreach (int i in pairValue)
        {
            if (pairOfThree.Contains(i))
                continue;
            if (pairValue.Contains(i + 1) && pairValue.Contains(i + 2))
            {
                pairOfThree.Add(i);
                pairOfThree.Add(i + 1);
                pairOfThree.Add(i + 2);
            }
        }
        foreach(Card card in cards)
        {
            int _count = 0;
            foreach(Card _card in result)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    _count++;
            }
            if (pairOfThree.Contains(card.Value) && _count < 2)
                result.Add(card);
        }
        return result;
    }

    public static List<Card> FindFourPairInSequence(List<Card> cards)
    {
        List<Card> result = new List<Card>();
        List<int> pairValue = new List<int>();
        List<Card> passCards = new List<Card>();
        List<int> pairOfThree = new List<int>();
        foreach (Card card in cards)
        {
            if (passCards.Contains(card))
                continue;
            int count = 0;
            foreach (Card _card in cards)
            {
                if (card == _card || passCards.Contains(_card))
                    continue;
                if (card.Value == _card.Value)
                {
                    passCards.Add(_card);
                    count++;
                }
            }
            if (count >= 1)
                pairValue.Add(card.Value);
            passCards.Add(card);
        }
        foreach (int i in pairValue)
        {
            if (pairOfThree.Contains(i))
                continue;
            if (pairValue.Contains(i + 1) && pairValue.Contains(i + 2) && pairValue.Contains(i + 3))
            {
                pairOfThree.Add(i);
                pairOfThree.Add(i + 1);
                pairOfThree.Add(i + 2);
                pairOfThree.Add(i + 3);
            }
        }
        foreach (Card card in cards)
        {
            int _count = 0;
            foreach (Card _card in result)
            {
                if (card == _card)
                    continue;
                if (card.Value == _card.Value)
                    _count++;
            }
            if (pairOfThree.Contains(card.Value) && _count < 2)
                result.Add(card);
        }
        return result;
    }

    public static List<Card> FindTwo(List<Card> cards)
    {
        List<Card> result = new List<Card>();
        foreach(Card card in cards)
        {
            if (card.Value == 15)
                result.Add(card);
        }
        return result;
    }

}
