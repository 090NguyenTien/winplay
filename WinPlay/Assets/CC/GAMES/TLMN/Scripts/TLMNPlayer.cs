﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DG.Tweening;

public class TLMNPlayer : TLMNBasePlayer {

    private const int CARD_WIDTH = 100;

    public GameObject buttonsGroup;
    public GameObject passBtn;
	public GameObject quickUnselectBtn;

    [HideInInspector]
    public Dictionary<int, Card> cards;
    [HideInInspector]
    public List<Card> currentSelectCards;
    [HideInInspector]
    //public bool isHost = false;
    //[HideInInspector]
    public int serverPosition;
    [HideInInspector]
    public int currentRecomendIndex;

	private Vector3 moneyTextPos;
	private bool counting = false;

    [SerializeField]
    private Image imgYourTurn = null;

   

    void Awake()
    {
        cards = new Dictionary<int, Card>();
        currentSelectCards = new List<Card>();
		moneyTextPos = moneyText.transform.localPosition;
    }

    public override void CountDownTime()
    {
        iTween.Stop(gameObject);
        if (controller.isNewRound)
            passBtn.SetActive(false);
        else
            passBtn.SetActive(true);
        buttonsGroup.SetActive(true);
		timerBackground.gameObject.SetActive(true);
        //focusPlayer.SetActive(true);
        timerImage.fillAmount = fillTo;

		//timerImage.color = Color.green;

		//timerImage.DOColor (Color.red, 15f);

        TLMNRoomController.Instance.MyTurnPlaying();
        imgYourTurn.gameObject.SetActive(true);
        Hashtable hash = new Hashtable();
		hash.Add ("name", "CountDownTime");
        hash.Add("from", fillTo);
        hash.Add("to", fillFrom);
        hash.Add("time", 15f);
        hash.Add("onupdate", "UpdateTime");
        hash.Add("oncomplete", "StopCountDownTime");
        iTween.ValueTo(gameObject, hash);
    }

    public void UpdateTime(float newValue)
    {
        timerImage.fillAmount = newValue;
		if (newValue > 1 - 0.3f && !counting)
			StartCoroutine ("PlayCountDownSound");
    }

	public void StopCountDownSound()
	{
		counting = false;
		SoundManager.StopSound (SoundManager.COUNT_DOWN);
		StopCoroutine ("PlayCountDownSound");
	}

	IEnumerator PlayCountDownSound()
	{
		counting = true;
		int count = 3;
		while (true) 
		{
			SoundManager.PlaySound (SoundManager.COUNT_DOWN);
			count--;
			if (count == 1)
				break;
			yield return new WaitForSeconds (1);
		}
		counting = false;
	}

    public override void StopCountDownTime()
    {
        StopCountDownSound ();
        buttonsGroup.SetActive(false);
		timerBackground.gameObject.SetActive(false);
        imgYourTurn.gameObject.SetActive(false);
        iTween.Stop (gameObject);
        //endOfMyTurn();
        //focusPlayer.SetActive(false);
    }

    //private void endOfMyTurn()
    //{
    //    Debug.LogError("end of my turn");
    //    foreach (KeyValuePair<int, Card> item in cards)
    //    {
    //        item.Value.setCanUse(true);
    //    }
    //}

    public void SendCard()
    {
        SendCard(currentSelectCards);
    }

    public override void SendCard(List<Card> hitCards)
    {
		lock (this) 
		{
			quickUnselectBtn.SetActive (false);
			currentSelectCards.Clear ();
			TLMNUtilities.SortCardsList (hitCards);
			controller.SortSendCards (hitCards);
			SortCards ();
			foreach (Card card in hitCards)
            {
				card.transform.localScale = Vector3.one;
				cards.Remove (card.Id);
                card.setCanUse(true);
            }
		}

        //endOfMyTurn();

    }

    public void SortCards(bool dontTween = false)
    {
        float distance = CARD_WIDTH;
        Vector3 center = cardGroup.transform.position;
        float x = center.x - cardGroup.transform.childCount / 2 * distance;

        if (dontTween == false)
        {
            Hashtable hash = new Hashtable();
            hash.Add("time", 0.1f);
            hash.Add("position", null);
            hash.Add("easetype", iTween.EaseType.linear);
            hash.Add("islocal", true);
            foreach (Transform t in cardGroup.transform)
            {
                hash["position"] = new Vector3(x, center.y);
                iTween.MoveTo(t.gameObject, hash);
                x += distance;
            }
        }
        else
        {
            foreach (Transform t in cardGroup.transform)
            {
                t.GetComponent<RectTransform>().anchoredPosition = new Vector3(x, center.y);
                x += distance; ;
            }
        }

        
    }

    public float getCardPosXByIndex(int cardIndex, int numChild)
    {
        float distance = CARD_WIDTH;
        Vector3 center = cardGroup.transform.position;
        float x = center.x - numChild / 2 * distance;

        return x + cardIndex * distance;
    }


    public void quickPickCard(TLMNCardType cardTypePlaying, Card cardSelect)
    {
        int numCard = TLMNRoomController.Instance.currentCards.Count;

        

        cards = cards.OrderBy(value => value.Value.Value).ThenBy(value => value.Value.Type).ToDictionary(x => x.Key, x => x.Value);

        if (cardTypePlaying == TLMNCardType.SINGLE)
        {
            if (TLMNRoomController.Instance.currentCards[0].Value == 15 && cardSelect.Value != 15)//no dang danh heo va card minh chon ko phai heo
            {
                List<Card> lstCardFourKind = cards.Where(x => x.Value.Value == cardSelect.Value).ToDictionary(x => x.Key, x => x.Value).Values.ToList();
                if (lstCardFourKind.Count >= 4)
                {
                    for (int i = 0; i < lstCardFourKind.Count; i++)
                    {
                        SelectCard(lstCardFourKind[i]);
                    }
                }
                else if (pickPairSequence(cardSelect, 4) == false)//neu ko co 4 doi thong thi check 3 doi thong
                {
                    pickPairSequence(cardSelect, 3);
                }
            }
            else
            {
                SelectCard(cardSelect);
            }
        }
        else if (cardTypePlaying == TLMNCardType.PAIR)
        {
            if (TLMNRoomController.Instance.currentCards[0].Value == 15 && cardSelect.Value != 15)//no dang danh doi heo && minh chon ko phai hep => la 3-4 doi thong or tu qui
            {
                List<Card> lstCardFourKind = cards.Where(x => x.Value.Value == cardSelect.Value).ToDictionary(x => x.Key, x => x.Value).Values.ToList();
                if (lstCardFourKind.Count >= 4)
                {
                    for (int i = 0; i < lstCardFourKind.Count; i++)
                    {
                        SelectCard(lstCardFourKind[i]);
                    }
                }
                else
                {
                    pickPairSequence(cardSelect, 4);//check doi heo
                }
            }
            else
            {
                List<Card> lstCardCanPlay = cards.Where(x => x.Value.Value == cardSelect.Value).ToDictionary(x => x.Key, x => x.Value).Values.ToList();
                if (lstCardCanPlay.Count >= numCard)
                {
                    for (int i = 0; i < numCard; i++)
                    {
                        SelectCard(lstCardCanPlay[i]);
                    }
                }
            }
        }
        else if ( cardTypePlaying == TLMNCardType.THREE_OF_A_KIND || cardTypePlaying == TLMNCardType.FOUR_OF_A_KIND)
        {
            List<Card> lstCardCanPlay = cards.Where(x => x.Value.Value == cardSelect.Value).ToDictionary(x => x.Key, x => x.Value).Values.ToList();
            if (lstCardCanPlay.Count >= numCard)
            {
                for (int i = 0; i < numCard; i++)
                {
                    SelectCard(lstCardCanPlay[i]);
                }
            }
            else if (cardTypePlaying == TLMNCardType.FOUR_OF_A_KIND)//cai dang danh la tu qui va pick vua roi la ko dc => check 4 doi thong
            {
                //4 doi thong an tu quy
                pickPairSequence(cardSelect, 4);
            }
        }
        else if (cardTypePlaying == TLMNCardType.STRAIGHT)
        {
            Card maxStraightCard = TLMNRoomController.Instance.currentCards[numCard - 1];
            List<List<Card>> allStraight = TLMNUtilities.GetAllStraight(cards.Values.ToList(), numCard);
            for (int i = 0; i < allStraight.Count; i++)
            {
                if (allStraight[i].Contains(cardSelect) == true && (allStraight[i][numCard - 1].Value > maxStraightCard.Value || (allStraight[i][numCard - 1].Value >= maxStraightCard.Value && (int)allStraight[i][numCard - 1].Type > (int)maxStraightCard.Type))) //card chon co trong list nay
                {
                    for (int k = 0; k < allStraight[i].Count; k++)
                    {
                        SelectCard(allStraight[i][k]);
                    }
                    break;
                }
            }
        }
        else if (cardTypePlaying == TLMNCardType.THREE_PAIR_SEQUENCE || cardTypePlaying == TLMNCardType.FOUR_PAIR_SEQUENCE)
        {
            bool isPicked = pickPairSequence(cardSelect, numCard / 2);
            if (isPicked == false)//ko co 3 doi thong hoac 4 doi thong
            {
                if (cardTypePlaying == TLMNCardType.THREE_PAIR_SEQUENCE)//la 3 doi thong thi moi check tu qui
                {
                    List<Card> lstCardCanPlay = cards.Where(x => x.Value.Value == cardSelect.Value).ToDictionary(x => x.Key, x => x.Value).Values.ToList();
                    if (lstCardCanPlay.Count >= 4)
                    {
                        for (int i = 0; i < numCard; i++)
                        {
                            SelectCard(lstCardCanPlay[i]);
                        }
                    }
                    else//ko co tu quy => check qua 4 doi thong
                    {
                        pickPairSequence(cardSelect, 4);
                    }
                }
            }
        }
    }

    private bool pickPairSequence(Card cardMustContain, int numPairSequence)
    {
        List<List<Card>> allPairSequence = TLMNUtilities.GetAllPairSequence(cards.Values.ToList(), numPairSequence);
        for (int i = 0; i < allPairSequence.Count; i++)
        {
            if (allPairSequence[i].Contains(cardMustContain) == true)//card chon co trong list nay
            {
                for (int k = 0; k < allPairSequence[i].Count; k++)
                {
                    SelectCard(allPairSequence[i][k]);
                }
                break;
            }
        }
        return allPairSequence.Count > 0 ? true : false;
    }

   


    public void PickCard(Card card)
    {
        if (!cards.ContainsValue(card))
            return;

		if (!controller.isStarting)
			return;
		
		SoundManager.PlaySound (SoundManager.SELECT_CARD);

        if (TLMNRoomController.Instance.isQuickChoice == true)
        {
            TLMNCardType cardTypePlaying = TLMNUtilities.GetCardType(TLMNRoomController.Instance.currentCards);

            if (cardTypePlaying != TLMNCardType.NULL && currentSelectCards.Count == 0 && TLMNRoomController.Instance.isNewRound == false)//chua chon cay nao ca => pick nhanh cho nguoi ta luon
            {
                quickPickCard(cardTypePlaying, card);
            }
            else if (TLMNRoomController.Instance.isOnMyTurn == true && TLMNRoomController.Instance.isNewRound == false)//dang trong luot minh chon ma chon cay moi
            {
                List<Card> lstNextCard = new List<Card>(currentSelectCards);
                lstNextCard.Add(card);
                TLMNCardType typeSelected = TLMNUtilities.GetCardType(lstNextCard);
                if (typeSelected == cardTypePlaying && lstNextCard.Count == currentSelectCards.Count)//card co them cay moi == type => chon moi luon && sanh moi phai bang sanh cu
                {
                    SelectCard(card);
                }
                else if (cardTypePlaying == TLMNCardType.SINGLE && currentSelectCards.Contains(card) == true)//nguoi ta muon deselect card
                {
                    SelectCard(card);
                }
                else //nugoi ta dang danh doi hay gi do
                {
                    int countOut = 0;
                    while (currentSelectCards.Count > 0)
                    {
                        SelectCard(currentSelectCards[0]);//deselect old card
                        ++countOut;
                        if (countOut > 20)
                        {
                            break;
                        }
                    }
                    quickPickCard(cardTypePlaying, card);

                }
            }
            else//chon 1 cay thoi
            {
                SelectCard(card);
            }
        }
        else
        {
            SelectCard(card);
        }

      
    }

    public void SelectCard(Card card)
    {
        Vector3 cardPos = card.transform.localPosition;
        lock (this)
        {
            if (currentSelectCards.Contains(card))//da seelect roi => unselect
            {
                currentSelectCards.Remove(card);
                card.transform.localPosition = new Vector3(cardPos.x, -2.9f);
                if (currentSelectCards.Count == 0)
                    quickUnselectBtn.SetActive(false);
            }
            else
            {
                currentSelectCards.Add(card);
                card.transform.localPosition = new Vector3(cardPos.x, 27.1f);
                if (!quickUnselectBtn.activeInHierarchy)
                    quickUnselectBtn.SetActive(true);
            }
        }
    }

	public void UnselectAllCards()
	{
		SoundManager.PlaySound (SoundManager.SELECT_CARD);

		lock (this)
		{
			List<Card> tempList = new List<Card>(currentSelectCards);
			foreach (Card card in tempList) 
			{
				Vector3 cardPos = card.transform.localPosition;
				card.transform.localPosition = new Vector3(cardPos.x, -2.9f);
			}
			currentSelectCards.Clear ();
			quickUnselectBtn.SetActive (false);
		}
	}
    // Dang xu ly
    public override void SetInfo(string[] info)
    {

        int sfsId = int.Parse(info[0]);
        string userId = info[1];
        string userName = info[2];
        long chip = long.Parse(info[3]);
        string avatar = info[4];
		string vip_Collect = info [5];
        this.level = int.Parse(info[6]);
        string Id_Border = info[7];

        if (MyInfo.MY_BOR_AVATAR != -1)
        {
            if (DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName) != null)
            {
                this.avatarBoder.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
                this.avatarBoder.gameObject.SetActive(true);

                this.avatarBoder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                this.avatarBoder.gameObject.SetActive(false);
                this.avatarBoder_Defaut.gameObject.SetActive(true);
            }
                       
        }
        else
        {
            this.avatarBoder.gameObject.SetActive(false);
            this.avatarBoder_Defaut.gameObject.SetActive(true);
        }

        avatarBoder.transform.localScale = new Vector2(1.1f, 1.1f);
        Debug.LogWarning("txtLevel.text=====" + info[6] + "        vip_Collect=========== " + vip_Collect + "         Id_Border================ " + Id_Border);
        txtLevel.text = info[6];
        txtLevel.text = DataHelper.GetStringVipByStringVipPoint(vip_Collect);//doi level thanh vip ben Chinh//
        this.sfsId = sfsId;
        this.userId = userId;
        this.chip = chip;

        displayName.text = userName;
        gold.text = Utilities.GetStringMoneyByLong(chip);
		this.avatar.gameObject.SetActive (true);
		LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
		// Debug.LogError("KhuongTest==========" + loginType);
		//if (loginType != LoginType.Facebook) {
		//	this.avatar.sprite = DataHelper.GetAvatar (avatar);
		//} else {
		//	StartCoroutine (UpdateAvatarThread (avatar, this.avatar));
		//	//this.avatarBoder.sprite = DataHelper.GetVip (int.Parse (vip));
		//}

        //load facebook avatar -- khuong
        if (MyInfo.SFS_ID == sfsId)
        {
            if (loginType != LoginType.Facebook)
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
            else
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
        }
        else
        {
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
            else
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
        }

        if (inviteBtn != null)
			inviteBtn.gameObject.SetActive (false);		
	
    }

    

    public override void Reset()
    {
        moneyText.text = "";
		moneyText.gameObject.transform.localPosition = moneyTextPos;
        immediateWinImage.gameObject.SetActive(false);
        rankImage.enabled = false;
        sampleCard.SetActive(false);
        currentSelectCards.Clear();
        Vector3 deckPosition = new Vector3(-400, -400);
        List<Transform> cards = new List<Transform>();
        foreach (Transform tf in cardGroup.transform)
            cards.Add(tf);
        foreach (Transform tf in cards)
        {
            tf.SetParent(controller.deck.transform);
            tf.localPosition = deckPosition;
        }
    }

//    private void SetAvatar(string avatar)
//    {
////        Texture2D mainImage;
////        WWW www = new WWW(avatarUrl);
////        yield return www;
////
////        mainImage = www.texture;
////        Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
//
//		avatar.sprite = DataHelper.GetAvatar(avatar);
//    }

}
