﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyMiniCommandKey
{
    public const string JOIN_LUCKY_MINi_GAME = "241";
    public const string LuckyBetGold = "217";
    public const string LuckyMaxBetting = "262";
    public const string LuckyBetChip = "30";
    public const string LuckyBetFail = "11";
    public const string LuckyHistory = "17";
    public const string LuckyCup = "18";
    public const string LuckyBet = "31";
    public const string GetLuckyGameInfo = "20";
    public const string LuckyWaitTime = "24";
    public const string LuckyResult = "9";
    public const string LuckyStartEvent = "59";
    public const string LuckyResultDetail = "29";
    public const string ExitGame = "43";
    public const string UpdateBetInfo = "16";
    public const string GetSicboHistory = "gsbh";
    public const string GetSicboTop = "gsbt";
    public const string GetSicBoLog100 = "gsl1";
    public const string USER_EXIT = "110";
}
