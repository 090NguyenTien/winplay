﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckyMiniParamKey
{
   
    public const string TYPE = "type";
    public const string MINE = "mine";
    public const string BETTING_MONEY = "betting_money";
    public const string Chip = "chip";
    public const string Gold = "gold";
    public const string betMoney = "bet";
    public const string time = "tj";
    public const string luckyLog = "logTen";

    public const string HistoryResult = "hr";
    public const string betMoneyType = "type";
    public const string betType = "nb";
    public const string luckyTotalBet = "chipR";
    public const string luckyTotalUser = "lu";
    public const string luckyUserBet = "betTT";
    public const string ResultChip = "enc";
    public const string LuckyBetFailReason = "nb";
    public const string timeType = "st1";
    public const string NumDice = "nd";
    public const string LogTx = "logTX";
    public const string NowMoney = "nm";
    public const string TopSicbo = "tsb";
    public const string TaiMoney = "mab";
    public const string XiuMoney = "mib";
}
