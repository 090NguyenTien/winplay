﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class JackBotHistoryContent : MonoBehaviour {
    public Text Order;
    public Text Time;
    public Text UserName;
    public Text Result;
    public Text Description;
    public GameObject RowBg;
    public void Show(int index, string description, SFSObject data)
    {
        //{ "choice": "tai", "type_money": "chip", "bet_money": 100, "return_money": 100, "gain_money": 200, "dice": "2,5,6", "room_id": 270716111538, "created_date": "2016-22-10 19:00:00" }
		Order.text = data.GetInt("gameno").ToString();
		Time.text = data.GetUtfString("created_date");
		UserName.text = data.GetUtfString("username");
        Result.text = data.GetLong("money").ToString();
        Description.text = description;
		
        RowBg.SetActive(index % 2 == 0);
    }

    internal void ShowTop(int index, string description, SFSObject data)
    {
        //{ "choice": "tai", "type_money": "chip", "bet_money": 100, "return_money": 100, "gain_money": 200, "dice": "2,5,6", "room_id": 270716111538, "created_date": "2016-22-10 19:00:00" }
        Order.text = data.GetInt("gameno").ToString();
        Time.text = data.GetUtfString("date");
        UserName.text = data.GetUtfString("user_name");
        Result.text = data.GetDouble("chip_win").ToString();
        Description.text = description;

        RowBg.SetActive(index % 2 == 0);
    }
}
