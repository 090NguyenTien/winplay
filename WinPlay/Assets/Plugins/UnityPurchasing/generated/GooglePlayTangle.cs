#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("/2rvXWLj6rmXAYZaUm+amXcz58J6UY57TvOGV2f2QqtQMuI7UYJ/qTpwqq1egWcTZBw7xiiyE+4pt4HoCgRTRSOSXhHLud2TVoMV15HfRyDhDTgbljOQRNeYrNVykYhcgjxPtjt+RdvVojs6XukHKInqZACC0hj4/abKqTe3XzSD0GjT3lV7jyf0XN5T2WkWhv2W2Y75j9OLpIVVLvSD0CKhr6CQIqGqoiKhoaAl3lt4uMNqW32GU+BEzNt71yiLD7AH7nm+AYx/1ScFYDv7fYc/vMESwatRP5YQg5AioYKQraapiiboJletoaGhpaCjTExdmgNnG1iGlsup6BWVdmNG37J9e57ThKEUSwuYrnV5klwHd+NPad5AOThma7qIiaKjoaCh");
        private static int[] order = new int[] { 10,5,4,4,9,11,12,9,11,9,11,11,13,13,14 };
        private static int key = 160;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
