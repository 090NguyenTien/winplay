#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("ee3SKZC+bY/wBw2SdNe5Xw6+tIbmfHp84mDEvs4LUGK5d9UtSGnrIcTfntlzypMO9Hs2JxJa1gCqk6Kd5mgi576pEvwUp4B91BLPW661rBVHDYpiFyud9jKAts0hW8cAgQaSMY2RlouQjYDI78nt//qs/frq9LiJ38nd//qs/fLq5LiJiZWc2bqci41Mw1QN9vf5a/JI2O/XjSzF9CKb70jJoRWj/ct1kUp25Cecigaep5xFUlqIa76qrDhW1rhKAQIaiTQfWrWGuFFhACgzn2XdkugpWkId4tM65naKeJk/4qLw1mtLAb2xCZnBZ+wM2ZiXndmanIuNkJ+QmpiNkJaX2YmXndmalpedkI2QlpeK2Zaf2YyKnMzLyM3Jys+j7vTKzMnLycDLyM3JyXv9Qsl7+lpZ+vv4+/v4+8n0//DZurjJe/jbyfT/8NN/sX8O9Pj4+Gxng/VdvnKiLe/OyjI99rQ37ZAojZCfkJqYjZzZm4DZmJeA2YmYi40gz4Y4fqwgXmBAy7sCISyIZ4dYqzmayo4Ow/7VrxIj9tj3I0OK4LZMz2C11IFOFHViJQqOYguPK47Jtjj/+qzk9/3v/e3SKZC+bY/wBw2SdNmWn9mNkZzZjZGcl9mYiYmVkJqYyej/+qz98+rzuImJlZzZsJea18iOjteYiYmVnNealpTWmImJlZyamKuclZCYl5qc2ZaX2Y2RkIrZmpyL8afJe/jo//qs5Nn9e/jxyXv4/cn2ZMQK0rDR4zEHN0xA9yCn5S8yxIuYmo2QmpzZio2YjZyUnJeNitfJ/8n2//qs5Or4+Ab9/Mn6+PgGyeTXuV8OvrSG8afJ5v/6rOTa/eHJ75528U3ZDjJV1dmWiU/G+Ml1Tro2/f/q+6yqyOrJ6P/6rP3z6vO4iYmJlZzZupyLjZCfkJqYjZCWl9m4jJuVnNmKjZiXnZiLndmNnIuUitmYiZWc2auWlo3ZurjJ5+70yc/Jzcv0//DTf7F/DvT4+Pz8+fp7+Pj5pZWc2bCXmtfI38nd//qs/fLq5LiJ/hWEwHpyqtkqwT1IRmO285IG0gW8h+a1kqlvuHA9jZvy6Xq4fspzePz5+nv49vnJe/jz+3v4+PkdaFDwcuBwJwCylQz+UtvJ+xHhxwGp8CrWyXg6//HS//j8/P77+8l4T+N4SjDgiwyk9yyGpmIL3PpDrHa0pPQITuJEarvd69M+9uRPtGWnmjGyee7Kz6PJm8jyyfD/+qz9/+r7rKrI6oPJe/iPyff/+qzk9vj4Bv39+vv4oF788IXuua/o540qTnLawr5aLJbV2Zqci42Qn5CamI2c2YmWlZCagN0bEihOiSb2vBjeMwiUgRQeTO7uncza7LLsoORKbQ4PZWc2qUM4oanx0v/4/Pz++/jv55GNjYmKw9bWjpCfkJqYjZCWl9m4jI2RlouQjYDIUSWH28wz3Cwg9i+SLVvd2ugOWFXvye3/+qz9+ur0uImJlZzZq5aWjdN/sX8O9Pj4/Pz5yZvI8snw//qssCGPZsrtnFiObTDU+/r4+fhae/iA2ZiKioyUnIrZmJqanImNmJeanHv4+f/w03+xfw6anfz4yXgLydP/qVNzLCMdBSnw/s5JjIzY");
        private static int[] order = new int[] { 32,49,56,15,50,5,10,23,26,24,17,55,45,43,23,32,43,18,39,58,57,48,30,41,24,36,38,33,30,46,46,31,48,56,50,42,39,47,48,51,51,41,43,47,58,59,48,48,52,58,53,57,52,55,54,59,58,58,58,59,60 };
        private static int key = 249;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
