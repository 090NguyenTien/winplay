﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;


public class ResultLotteryManager : MonoBehaviour {
    private static ResultLotteryManager instance;
    public static ResultLotteryManager Instance { get { return instance; } }

    [SerializeField]
    GameObject PanelResultManager;  

    #region Phần đặt cược hôm nay

    [SerializeField]
    GameObject PanelBetToDay;
    [SerializeField]
    GameObject List_BetToDay;
    [SerializeField]
    Transform Parent_BetToDay;
    [SerializeField]
    GameObject ImgBetTitle;
    [SerializeField]
    GameObject BetToDayModule;

    private Dictionary<int, DataBetBeforResult> DicBetBeforResult;
    private Dictionary<int, DataBetAfterResult> DicBetAfterResult;

    private int StateBet;

    #endregion


    #region Phần giấy dò

    [SerializeField]
    GameObject PanelResultInfo;
    [SerializeField]
    Button BtnWatchResult6Day;
    [SerializeField]
    GameObject List_Result;    
    [SerializeField]
    Transform Parent_Result;
    [SerializeField]
    Text ResultTitle;
    [SerializeField]
    Button BtnRight;
    [SerializeField]
    Button BtnLeft;
    [SerializeField]
    GameObject Border;

    // Module Các giải xổ số
    [SerializeField]
    GameObject DacBietModule;
    [SerializeField]
    GameObject GiaiNhatModule;
    [SerializeField]
    GameObject GiaiNhiModule;
    [SerializeField]
    GameObject GiaiBaModule;
    [SerializeField]
    GameObject GiaiTuModule;
    [SerializeField]
    GameObject GiaiSauModule;

    private Dictionary<string, Text> DicTextResult;

    private string GiaiDacBiet;
    private string GiaiNhat;
    private string[] ArrayNhi;
    private string[] ArrayBa;
    private string[] ArrayTu;
    private string[] ArrayNam;
    private string[] ArraySau;
    private string[] ArrayBay;

    private string _DacBiet = "";
    private string _Nhat = "";
    private string _Nhi = "";
    private string _Ba = "";
    private string _Tu = "";
    private string _Nam = "";
    private string _Sau = "";
    private string _Bay = "";

    private int Pase_Result;

    #endregion

    [SerializeField]
    PopupLotteryManager LotteryManager;
  
    void Awake()
    {
        instance = this;
    }

    public void ShowPanelResult()
    {
        //if (HomeController.FistClickBtnResultAfterHaveResult == true)
        //{
        //    HomeController.FistClickBtnResultAfterHaveResult = false;
        //}
        PopupLotteryManager.Panel = "KetQua";
        SendBetResultRequest();
        LotteryManager.BtnBet.gameObject.SetActive(true); // Hiện BtnBet ở TOP Lottery
        DicTextResult = new Dictionary<string, Text>();
        PanelResultManager.SetActive(true);
        ImgBetTitle.SetActive(true);

        BtnWatchResult6Day.onClick.RemoveAllListeners();
        BtnWatchResult6Day.onClick.AddListener(BtnWatchResult6DayOnClick);
    }


    public void BackUpInit()
    {
        if (DicTextResult.Count > 0)
        {
            DicTextResult.Clear();
        }
        
        PanelResultManager.SetActive(false);                
        List_Result.SetActive(true);             
        OnPanelBetResult(true, true);
    }

    public void SendBetResultRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.ResultBet);
        SFS.Instance.SendRoomRequest(gp);
    }


    public void BtnExit_1()
    {
        LotteryManager.BtnBackOnClick();
    }

    #region GIẤY DÒ


    private void GetResultToDay()
    {
        string TieuDe = LotteryManager.GetStringDateResult(0);
        Pase_Result = 0;
        ResultTitle.text = TieuDe;
        DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(0);
        _DacBiet = ketQua.DacBiet;
        _Nhat = ketQua.GiaiNhat;
        _Nhi = ketQua.GiaiNhi;
        _Ba = ketQua.GiaiBa;
        _Tu = ketQua.GiaiTu;
        _Nam = ketQua.GiaiNam;
        _Sau = ketQua.GiaiSau;
        _Bay = ketQua.GiaiBay;
    }

    public void ShowResultDefault()
    {
        BtnLeft.gameObject.GetComponent<Image>().color = Color.gray;
        BtnRight.gameObject.GetComponent<Image>().color = Color.white;
        DicTextResult.Clear();
        for (int i = 0; i < Parent_Result.childCount; i++)
        {
            GameObject.Destroy(Parent_Result.GetChild(i).gameObject);
        }
        GetResultToDay();
        ShowModulesResult(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
    }

    public void ShowDayBeforResult()
    {
        if (Pase_Result < 5)
        {
            SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
            ++Pase_Result;
            if (Pase_Result == 5)
            {
                BtnRight.gameObject.GetComponent<Image>().color = Color.gray;
            }
            BtnLeft.gameObject.GetComponent<Image>().color = Color.white;
            string TieuDe = LotteryManager.GetStringDateResult(Pase_Result);
            ResultTitle.text = TieuDe;
            DicTextResult.Clear();
            for (int i = 0; i < Parent_Result.childCount; i++)
            {
                GameObject.Destroy(Parent_Result.GetChild(i).gameObject);
            }
            DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(Pase_Result);
            _DacBiet = ketQua.DacBiet;
            _Nhat = ketQua.GiaiNhat;
            _Nhi = ketQua.GiaiNhi;
            _Ba = ketQua.GiaiBa;
            _Tu = ketQua.GiaiTu;
            _Nam = ketQua.GiaiNam;
            _Sau = ketQua.GiaiSau;
            _Bay = ketQua.GiaiBay;

            ShowModulesResult(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
        }
    }

    public void ShowDayAfterResult()
    {
        if (Pase_Result > 0)
        {
            SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
            --Pase_Result;
            if (Pase_Result == 0)
            {
                BtnLeft.gameObject.GetComponent<Image>().color = Color.gray;
            }
            BtnRight.gameObject.GetComponent<Image>().color = Color.white;
            string TieuDe = LotteryManager.GetStringDateResult(Pase_Result);
            ResultTitle.text = TieuDe;
            DicTextResult.Clear();
            for (int i = 0; i < Parent_Result.childCount; i++)
            {
                GameObject.Destroy(Parent_Result.GetChild(i).gameObject);
            }
            DataResultLottery ketQua = LotteryManager.GetDataResultByDateIndex(Pase_Result);
            _DacBiet = ketQua.DacBiet;
            _Nhat = ketQua.GiaiNhat;
            _Nhi = ketQua.GiaiNhi;
            _Ba = ketQua.GiaiBa;
            _Tu = ketQua.GiaiTu;
            _Nam = ketQua.GiaiNam;
            _Sau = ketQua.GiaiSau;
            _Bay = ketQua.GiaiBay;

            ShowModulesResult(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
        }
    }

    #region SHOW MODULES

    public void ShowModulesResult(string dacbiet, string nhat, string nhi, string ba, string tu, string nam, string sau, string bay)
    {
        int index = 0;
        GiaiDacBiet = dacbiet.Trim();
        ShowModuleDacBiet(index, "Đặc Biệt", GiaiDacBiet);

        GiaiNhat = nhat.Trim();
        ShowModuleDacBiet(++index, "Giải Nhất", GiaiNhat, false);

        ArrayNhi = nhi.Split('|');
        string nhi_1, nhi_2;
        nhi_1 = ArrayNhi[0].Trim();
        nhi_2 = ArrayNhi[1].Trim();
        ShowModuleNhi(++index, "Giải Nhì", nhi_1, nhi_2);

        ArrayBa = ba.Split('|');
        string ba_1, ba_2, ba_3, ba_4, ba_5, ba_6;
        ba_1 = ArrayBa[0].Trim();
        ba_2 = ArrayBa[1].Trim();
        ba_3 = ArrayBa[2].Trim();
        ba_4 = ArrayBa[3].Trim();
        ba_5 = ArrayBa[4].Trim();
        ba_6 = ArrayBa[5].Trim();
        ArrayBa[0] = ba_1;
        ArrayBa[1] = ba_2;
        ArrayBa[2] = ba_3;
        ArrayBa[3] = ba_4;
        ArrayBa[4] = ba_5;
        ArrayBa[5] = ba_6;
        ShowModuleBa(++index, "Giải Ba", ba_1, ba_2, ba_3, ba_4, ba_5, ba_6);

        ArrayTu = tu.Split('|');
        string tu_1, tu_2, tu_3, tu_4;
        tu_1 = ArrayTu[0].Trim();
        tu_2 = ArrayTu[1].Trim();
        tu_3 = ArrayTu[2].Trim();
        tu_4 = ArrayTu[3].Trim();
        ArrayTu[0] = tu_1;
        ArrayTu[1] = tu_2;
        ArrayTu[2] = tu_3;
        ArrayTu[3] = tu_4;
        ShowModuleTu(++index, "Giải Tư", tu_1, tu_2, tu_3, tu_4);

        ArrayNam = nam.Split('|');
        string nam_1, nam_2, nam_3, nam_4, nam_5, nam_6;
        nam_1 = ArrayNam[0].Trim();
        nam_2 = ArrayNam[1].Trim();
        nam_3 = ArrayNam[2].Trim();
        nam_4 = ArrayNam[3].Trim();
        nam_5 = ArrayNam[4].Trim();
        nam_6 = ArrayNam[5].Trim();
        ArrayNam[0] = nam_1;
        ArrayNam[1] = nam_2;
        ArrayNam[2] = nam_3;
        ArrayNam[3] = nam_4;
        ArrayNam[4] = nam_5;
        ArrayNam[5] = nam_6;
        ShowModuleBa(++index, "Giải Năm", nam_1, nam_2, nam_3, nam_4, nam_5, nam_6, false);

        ArraySau = sau.Split('|');
        string sau_1, sau_2, sau_3;
        sau_1 = ArraySau[0].Trim();
        sau_2 = ArraySau[1].Trim();
        sau_3 = ArraySau[2].Trim();
        ArraySau[0] = sau_1;
        ArraySau[1] = sau_2;
        ArraySau[2] = sau_3;
        ShowModuleSau(++index, "Giải Sáu", sau_1, sau_2, sau_3);

        ArrayBay = bay.Split('|');
        string bay_1, bay_2, bay_3, bay_4;
        bay_1 = ArrayBay[0].Trim();
        bay_2 = ArrayBay[1].Trim();
        bay_3 = ArrayBay[2].Trim();
        bay_4 = ArrayBay[3].Trim();
        ArrayBay[0] = bay_1;
        ArrayBay[1] = bay_2;
        ArrayBay[2] = bay_3;
        ArrayBay[3] = bay_4;
        ShowModuleTu(++index, "Giải Bảy", bay_1, bay_2, bay_3, bay_4, false);
    }

    void ShowModuleDacBiet(int index, string type, string number, bool GiaiDacBiet = true)
    {
        KetQuaType itemView;
        GameObject obj = new GameObject();
        if (GiaiDacBiet == true)
        {
            obj = Instantiate(DacBietModule) as GameObject;
        }
        else
        {
            obj = Instantiate(GiaiNhatModule) as GameObject;
        }
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_Result);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number);
        Text txtDacBiet = itemView.Number_1_Txt;
        string key = "";
        if (GiaiDacBiet == true)
        {
            key = "db";
        }
        else
        {
            key = "1";
        }
        DicTextResult.Add(key, txtDacBiet);
    }

    void ShowModuleNhi(int index, string type, string number, string numBer_2)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiNhiModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_Result);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2);
        Text txtNhi_1 = itemView.Number_1_Txt;
        Text txtNhi_2 = itemView.Number_2_Txt;
        DicTextResult.Add("2_1", txtNhi_1);
        DicTextResult.Add("2_2", txtNhi_2);
    }

    void ShowModuleBa(int index, string type, string number, string numBer_2, string number_3, string number_4, string number_5, string number_6, bool giaiBa = true)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiBaModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_Result);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3, number_4, number_5, number_6);

        Text txtBa_1 = itemView.Number_1_Txt;
        Text txtBa_2 = itemView.Number_2_Txt;
        Text txtBa_3 = itemView.Number_3_Txt;
        Text txtBa_4 = itemView.Number_4_Txt;
        Text txtBa_5 = itemView.Number_5_Txt;
        Text txtBa_6 = itemView.Number_6_Txt;
        string key_1, key_2, key_3, key_4, key_5, key_6;
        if (giaiBa == true)
        {
            key_1 = "3_1";
            key_2 = "3_2";
            key_3 = "3_3";
            key_4 = "3_4";
            key_5 = "3_5";
            key_6 = "3_6";
        }
        else
        {
            key_1 = "5_1";
            key_2 = "5_2";
            key_3 = "5_3";
            key_4 = "5_4";
            key_5 = "5_5";
            key_6 = "5_6";
        }

        DicTextResult.Add(key_1, txtBa_1);
        DicTextResult.Add(key_2, txtBa_2);
        DicTextResult.Add(key_3, txtBa_3);
        DicTextResult.Add(key_4, txtBa_4);
        DicTextResult.Add(key_5, txtBa_5);
        DicTextResult.Add(key_6, txtBa_6);
    }

    void ShowModuleTu(int index, string type, string number, string numBer_2, string number_3, string number_4, bool giaiTu = true)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiTuModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_Result);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3, number_4);

        Text txtTu_1 = itemView.Number_1_Txt;
        Text txtTu_2 = itemView.Number_2_Txt;
        Text txtTu_3 = itemView.Number_3_Txt;
        Text txtTu_4 = itemView.Number_4_Txt;
        string key_1, key_2, key_3, key_4;
        if (giaiTu == true)
        {
            key_1 = "4_1";
            key_2 = "4_2";
            key_3 = "4_3";
            key_4 = "4_4";
        }
        else
        {
            key_1 = "7_1";
            key_2 = "7_2";
            key_3 = "7_3";
            key_4 = "7_4";
        }

        DicTextResult.Add(key_1, txtTu_1);
        DicTextResult.Add(key_2, txtTu_2);
        DicTextResult.Add(key_3, txtTu_3);
        DicTextResult.Add(key_4, txtTu_4);
    }

    void ShowModuleSau(int index, string type, string number, string numBer_2, string number_3)
    {
        KetQuaType itemView;
        GameObject obj = Instantiate(GiaiSauModule) as GameObject;
        DataItemResult item = new DataItemResult();
        obj.transform.SetParent(Parent_Result);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<KetQuaType>();
        itemView.Init(item);
        itemView.Show(type, number, numBer_2, number_3);

        Text txtSau_1 = itemView.Number_1_Txt;
        Text txtSau_2 = itemView.Number_2_Txt;
        Text txtSau_3 = itemView.Number_3_Txt;

        string key_1, key_2, key_3;

        key_1 = "6_1";
        key_2 = "6_2";
        key_3 = "6_3";

        DicTextResult.Add(key_1, txtSau_1);
        DicTextResult.Add(key_2, txtSau_2);
        DicTextResult.Add(key_3, txtSau_3);
    }

    #endregion


    #endregion


    #region ĐẶT NGÀY HÔM NAY


    public void SetBetResult(GamePacket gp)
    {
        DicBetAfterResult = new Dictionary<int, DataBetAfterResult>();
        DicBetBeforResult = new Dictionary<int, DataBetBeforResult>();
        int resultIndex = gp.GetInt("ResultIndex");
        Debug.Log("Vo ham nhan cua Resuslt " + resultIndex);

        if (resultIndex == 0)
        {
            //CHƯA ĐẶT CƯỢC
            StateBet = 0;
        }
        else if (resultIndex == 1)
        {
            // ĐÃ ĐẶT NHƯNG CHƯA CÓ KẾT QUẢ XỔ SỐ
            ISFSArray myArray = gp.GetSFSArray(ParamKey.ResultBet);

            Debug.Log(myArray.ToJson());
            for (int i = 0; i < myArray.Size(); i++)
            {
                SFSObject obj = (SFSObject)myArray.GetSFSObject(i);
                int indexType = obj.GetInt("Type");
                string LoaiDe = GetStringTypeByIndex(indexType);
                string SoDanh = obj.GetUtfString("BetNumber");
                long TienDat = obj.GetLong("BetMoney");
                DataBetBeforResult DataBefor = new DataBetBeforResult();
                DataBefor.index = i;
                DataBefor.SoDanh = SoDanh;
                DataBefor.LoaiDe = LoaiDe;
                DataBefor.TienCuoc = TienDat.ToString();
                DicBetBeforResult.Add(i, DataBefor);
            }
            StateBet = 1;
        }
        else if (resultIndex == 2)
        {
            // ĐÃ CÓ KẾT QUẢ
            Debug.Log("ĐÃ CÓ KẾT QUẢ");
            ISFSArray myArray = gp.GetSFSArray(ParamKey.ResultBet);
            for (int i = 0; i < myArray.Size(); i++)
            {
                SFSObject obj = (SFSObject)myArray.GetSFSObject(i);
                bool win = obj.GetBool("isWin");
                int indexType = obj.GetInt("Type");
                string LoaiDe = GetStringTypeByIndex(indexType);
                string SoDanh = obj.GetUtfString("BetNumber");
                long TienDat = obj.GetLong("BetMoney");
                long TienThang = obj.GetLong("BetMoneyResult");
                int SoLanXuatHien = obj.GetInt("Count");
                DataBetAfterResult DataAfter = new DataBetAfterResult();
                DataAfter.index = i;
                DataAfter.iwin = win;
                DataAfter.SoDanh = SoDanh;
                DataAfter.LoaiDe = LoaiDe;
                DataAfter.TienCuoc = TienDat.ToString();
                DataAfter.TienThang = TienThang.ToString();
                DataAfter.SolanXuatHien = SoLanXuatHien.ToString();
                DicBetAfterResult.Add(i, DataAfter);
                StateBet = 2;
            }
        }

        CheckOnPanelResult();
    }

    public void CheckOnPanelResult()
    {
        if (StateBet == 0)
        {
            OnPanelBetResult(false);
        }
        else if (StateBet == 1)
        {
            OnPanelBetResult(true);
        }
        else
        {
            OnPanelBetResult(true, true);
        }
    }


    public void OnPanelBetResult(bool IsBet, bool IsHaveResult = false)
    {
        PopupLotteryManager.OnThangThua = true;
        ImgBetTitle.SetActive(true);
        PanelResultInfo.SetActive(false);
        Border.SetActive(false);
        PanelBetToDay.SetActive(true);
        List_BetToDay.SetActive(true);
        List_BetToDay.transform.GetChild(0).gameObject.SetActive(true);
        List_BetToDay.transform.GetChild(1).gameObject.SetActive(true);
        List_BetToDay.transform.GetChild(3).gameObject.SetActive(false);
        if (IsBet == false)
        {
            List_BetToDay.transform.GetChild(0).gameObject.SetActive(false);
            List_BetToDay.transform.GetChild(1).gameObject.SetActive(false);
            List_BetToDay.transform.GetChild(3).gameObject.SetActive(true);
            Text ThongBao = List_BetToDay.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
            ThongBao.text = "HÔM NAY BẠN CHƯA ĐẶT CƯỢC";
        }
        else
        {
            for (int i = 0; i < Parent_BetToDay.childCount; i++)
            {
                GameObject.Destroy(Parent_BetToDay.GetChild(i).gameObject);
            }
            if (IsHaveResult == false)
            {
                foreach (KeyValuePair<int, DataBetBeforResult> item in DicBetBeforResult)
                {
                    int index = item.Value.index;
                    string sodanh = item.Value.SoDanh;
                    string loaide = item.Value.LoaiDe;
                    string tiencuoc = item.Value.TienCuoc;
                    ShowModuleBetResultBeforHaveResult(index, sodanh, loaide, tiencuoc);
                }
            }
            else
            {
                foreach (KeyValuePair<int, DataBetAfterResult> item in DicBetAfterResult)
                {
                    int index = item.Value.index;
                    bool iswin = item.Value.iwin;
                    string sodanh = item.Value.SoDanh;
                    string loaide = item.Value.LoaiDe;
                    string tiencuoc = item.Value.TienCuoc;
                    string tienthang = item.Value.TienThang;
                    string solan = item.Value.SolanXuatHien;

                    ShowModuleBetResultAfterHaveResult(index, iswin, sodanh, loaide, tiencuoc, tienthang, solan);
                }
            }
        }
    }

    public void OffPanelBetResult()
    {
        PopupLotteryManager.OnThangThua = false;
        PanelBetToDay.SetActive(false);
        PanelResultInfo.SetActive(true);
        Border.SetActive(true);
        ImgBetTitle.SetActive(false);
    }


    void ShowModuleBetResultAfterHaveResult(int index, bool win, string number, string type, string money, string winmoney, string count)
    {
        ThangThuaModule itemView;
        GameObject obj = Instantiate(BetToDayModule) as GameObject;
        DataItemBetToDay item = new DataItemBetToDay();
        obj.transform.SetParent(Parent_BetToDay);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;

        itemView = obj.GetComponent<ThangThuaModule>();
        itemView.Init(item);
        itemView.ShowAfterHaveResult(win, number, type, money, winmoney, count);
    }

    void ShowModuleBetResultBeforHaveResult(int index, string number, string type, string money)
    {
        ThangThuaModule itemView;
        GameObject obj = Instantiate(BetToDayModule) as GameObject;
        DataItemBetToDay item = new DataItemBetToDay();
        obj.transform.SetParent(Parent_BetToDay);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;

        itemView = obj.GetComponent<ThangThuaModule>();
        itemView.Init(item);
        itemView.ShowBeforHaveResult(number, type, money);
    }


    public void ClearDicData()
    {
        if (DicBetBeforResult != null)
        {
            DicBetBeforResult.Clear();
        }
        if (DicBetAfterResult != null)
        {
            DicBetAfterResult.Clear();
        }
    }

    #endregion


    #region TÌM SỐ TRÚNG

    public void TimSoTrung(string SoDanh, int Type)
    {
        if (Type == 0 || Type == 1 || Type == 3)
        {
            TimTrenCacLo(SoDanh, Type);
        }
        else
        {
            TimTrenGiaiDacBiet(SoDanh, Type);
        }
    }

    public void TimTrenCacLo(string SoDanh, int Type)
    {
        if (Type == 3)
        {
            for (int p = 0; p < ArrayBay.Length; p++)
            {
                string bay = ArrayBay[p];
                if (SoDanh == bay)
                {
                    int a = p + 1;
                    HienThiSoTrung("7_" + (a).ToString(), "2");
                }
            }
        }
        else if (Type == 1)
        {
            string db = GiaiDacBiet.Substring(2, 3);
            if (SoDanh == db)
            {
                HienThiSoTrung("db", "3");
            }
            string nhat = GiaiNhat.Substring(2, 3);
            if (SoDanh == nhat)
            {
                HienThiSoTrung("1", "3");
            }
            for (int i = 0; i < ArrayNhi.Length; i++)
            {
                string nhi = ArrayNhi[i].Substring(2, 3);
                if (SoDanh == nhi)
                {
                    int a = i + 1;
                    HienThiSoTrung("2_" + (a).ToString(), "3");
                }
            }
            for (int j = 0; j < ArrayBa.Length; j++)
            {
                string ba = ArrayBa[j].Substring(2, 3);
                if (SoDanh == ba)
                {
                    int a = j + 1;
                    HienThiSoTrung("3_" + (a).ToString(), "3");
                }
            }
            for (int k = 0; k < ArrayTu.Length; k++)
            {
                string tu = ArrayTu[k].Substring(1, 3);
                if (SoDanh == tu)
                {
                    int a = k + 1;
                    HienThiSoTrung("4_" + (a).ToString(), "3");
                }
            }
            for (int l = 0; l < ArrayNam.Length; l++)
            {
                string nam = ArrayNam[l].Substring(1, 3);
                if (SoDanh == nam)
                {
                    int a = l + 1;
                    HienThiSoTrung("5_" + (a).ToString(), "3");
                }
            }
            for (int t = 0; t < ArraySau.Length; t++)
            {
                string sau = ArraySau[t];
                if (SoDanh == sau)
                {
                    int a = t + 1;
                    HienThiSoTrung("6_" + (a).ToString(), "3");
                }
            }
        }
        else if (Type == 0)
        {
            string db = GiaiDacBiet.Substring(3, 2);
            if (SoDanh == db)
            {
                HienThiSoTrung("db", "2");
            }
            string nhat = GiaiNhat.Substring(3, 2);
            if (SoDanh == nhat)
            {
                HienThiSoTrung("1", "2");
            }
            for (int i = 0; i < ArrayNhi.Length; i++)
            {
                string nhi = ArrayNhi[i].Substring(3, 2);
                if (SoDanh == nhi)
                {
                    int a = i + 1;
                    HienThiSoTrung("2_" + (++i).ToString(), "2");
                }
            }
            for (int j = 0; j < ArrayBa.Length; j++)
            {
                string ba = ArrayBa[j].Substring(3, 2);
                if (SoDanh == ba)
                {
                    int a = j + 1;
                    HienThiSoTrung("3_" + (a).ToString(), "2");
                }
            }
            for (int k = 0; k < ArrayTu.Length; k++)
            {
                string tu = ArrayTu[k].Substring(2, 2);
                if (SoDanh == tu)
                {
                    int a = k + 1;
                    HienThiSoTrung("4_" + (a).ToString(), "2");
                }
            }
            for (int l = 0; l < ArrayNam.Length; l++)
            {
                string nam = ArrayNam[l].Substring(2, 2);
                if (SoDanh == nam)
                {
                    int a = l + 1;
                    HienThiSoTrung("5_" + (a).ToString(), "2");
                }
            }
            for (int t = 0; t < ArraySau.Length; t++)
            {
                string sau = ArraySau[t].Substring(1, 2);
                if (SoDanh == sau)
                {
                    int a = t + 1;
                    HienThiSoTrung("6_" + (a).ToString(), "2");
                }
            }
            for (int p = 0; p < ArrayBay.Length; p++)
            {
                string bay = ArrayBay[p];
                if (SoDanh == bay)
                {
                    int a = p + 1;
                    HienThiSoTrung("7_" + (a).ToString(), "2");
                }
            }
        }
    }


    public void TimTrenGiaiDacBiet(string SoDanh, int Type)
    {
        string CatDacBiet = "";
        if (Type == 2)
        {
            CatDacBiet = GiaiDacBiet.Substring(2, 3);
            if (SoDanh == CatDacBiet)
            {
                HienThiSoTrung("db", "3");
            }
        }
        else if (Type == 4)
        {
            CatDacBiet = GiaiDacBiet.Substring(3, 2);
            if (SoDanh == CatDacBiet)
            {
                HienThiSoTrung("db", "2");
            }
        }
        else if (Type == 5)
        {
            CatDacBiet = GiaiDacBiet.Substring(3, 1);
            if (SoDanh == CatDacBiet)
            {
                HienThiSoTrung("db", "dau");
            }
        }
        else if (Type == 6)
        {
            CatDacBiet = GiaiDacBiet.Substring(4, 1);
            if (SoDanh == CatDacBiet)
            {
                HienThiSoTrung("db", "duoi");
            }
        }
    }


    private void HienThiSoTrung(string key, string type)
    {
        foreach (KeyValuePair<string, Text> item in DicTextResult)
        {
            if (key == item.Key)
            {
                string txtBanDau = item.Value.text;
                string txtTruoc = "";
                string txtSau = "";
                string txtCuoi = "";
                bool ToanPhan = false;
                if (type == "2")
                {
                    if (txtBanDau.Length > 2)
                    {
                        txtSau = txtBanDau.Substring(txtBanDau.Length - 2, 2);
                        txtTruoc = txtBanDau.Substring(0, txtBanDau.Length - txtSau.Length);
                    }
                    else
                    {
                        ToanPhan = true;
                    }
                }
                else if (type == "3")
                {
                    if (txtBanDau.Length > 3)
                    {
                        txtSau = txtBanDau.Substring(txtBanDau.Length - 3, 3);
                        txtTruoc = txtBanDau.Substring(0, txtBanDau.Length - txtSau.Length);
                    }
                    else
                    {
                        ToanPhan = true;
                    }
                }
                else if (type == "dau")
                {
                    txtTruoc = txtBanDau.Substring(0, 3);
                    txtSau = txtBanDau.Substring(3, 1);
                    txtCuoi = txtBanDau.Substring(4, 1);
                }
                else if (type == "duoi")
                {
                    txtTruoc = txtBanDau.Substring(0, 4);
                    txtSau = txtBanDau.Substring(4, 1);
                }
                if (type == "dau")
                {
                    item.Value.text = txtTruoc + "<color=#A500ECFF>" + txtSau + "</color>" + txtCuoi;
                }
                else
                {
                    if (ToanPhan == true)
                    {
                        item.Value.text = "<color=#A500ECFF>" + txtBanDau + "</color>";
                    }
                    else
                    {
                        item.Value.text = txtTruoc + "<color=#A500ECFF>" + txtSau + "</color>";
                    }
                }
            }
        }
    }


    private void ResetLookingWinNumber()
    {
        for (int i = 0; i < Parent_Result.childCount; i++)
        {
            GameObject.Destroy(Parent_Result.GetChild(i).gameObject);
        }
        DicTextResult.Clear();
        ShowModulesResult(_DacBiet, _Nhat, _Nhi, _Ba, _Tu, _Nam, _Sau, _Bay);
    }

    public void voiTestTimSoTrung()
    {
        ResetLookingWinNumber();
    }

    #endregion



    public void BtnWatchResult6DayOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PopupLotteryManager.OnThangThua = false;
        PanelBetToDay.SetActive(false);
        ImgBetTitle.SetActive(false);
        Border.SetActive(true);
        PanelResultInfo.SetActive(true);
        List_Result.SetActive(true);
        ShowResultDefault();
    }


    public void OnBtnBackClick()
    {
        Border.SetActive(false);
        PanelResultInfo.SetActive(false);
        PanelBetToDay.SetActive(false);
    }


    public string GetStringTypeByIndex(int index)
    {
        string Type = "LÔ 2 SỐ";

        if (index == 1)
        {
            Type = "LÔ 3 SỐ";
        }
        else if (index == 2)
        {
            Type = "3 CÀNG";
        }
        else if (index == 3)
        {
            Type = "ĐỀ ĐẦU";
        }
        else if (index == 4)
        {
            Type = "ĐỀ ĐẶC BIỆT";
        }
        else if (index == 5)
        {
            Type = "ĐÁNH ĐẦU";
        }
        else if (index == 6)
        {
            Type = "ĐÁNH ĐUÔI";
        }
        return Type;
    }

    private int GetIndexByStringType(string Type)
    {
        int index = 0;

        if (Type == "LO 3 SO")
        {
            index = 1;
        }
        else if (Type == "3 CANG")
        {
            index = 2;
        }
        else if (Type == "DE DAU")
        {
            index = 3;
        }
        else if (Type == "DE DAC BIET")
        {
            index = 4;
        }
        else if (Type == "DANH DAU")
        {
            index = 5;
        }
        else if (Type == "DANH DUOI")
        {
            index = 6;
        }
        return index;
    }


}
