﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatController : MonoBehaviour
{
    private bool isInit = false;
    [SerializeField]
    Toggle chatAll, friends, beInvite;
    [SerializeField]
    GameObject warningChat, warningFriend, warningInvite, warningBtnChat;
    int maxMessage = 50;
    List<Messager> messageList= new List<Messager>();
    List<FriendItem> FriendItemList = new List<FriendItem>();
    [SerializeField]
    GameObject ChatAllContainer, FriendsContainer, FriendsChatContainer, textObject, textSend, chatInputGroup, FriendItem, InviteFriend, InviteFriendContainer;
    [SerializeField]
    ScrollRect ScrollViewChatAll, ScrollViewFriends, ScrollViewFriendsChat, ScrollViewInviteFriend;
    [SerializeField]
    GameObject Content;
    int curTabActive = 0; //0 all 1 ban be
    bool isGetFriend = false;
    // Start is called before the first frame update
    void Start()
    {
        Content.gameObject.SetActive(false);
        warningBtnChat.SetActive(false);
        warningChat.SetActive(false);
        warningFriend.SetActive(false);
        warningInvite.SetActive(false);
        ActiveScreenTab(true, false,false);
        chatAll.onValueChanged.AddListener(isToggleChange);
        beInvite.onValueChanged.AddListener(isToggleChange);
        friends.onValueChanged.AddListener(isToggleChange);
    }
    public void AllowGetFriendStatus()
    {
        isGetFriend = false;       
    }
    public bool CheckActiveSelfContent()
    {
        return Content.activeSelf;
    }
    internal void Hide()
    {
        Content.gameObject.SetActive(false);
    }
    internal void Show()
    {
        Content.gameObject.SetActive(true);
        if (isInit == false)
        {
            isInit = true;
            InitChat();
        }
        //SFS.Instance.RequestJoinChatRoom();
    }

    private void InitChat()
    {
        curTabActive = 0;
        chatAll.isOn = true;
        isGetFriend = false;
    }
    private void ActiveScreenTab(bool isChatAll, bool isFriends, bool invite)
    {
        ScrollViewChatAll.gameObject.SetActive(isChatAll);
        ScrollViewFriendsChat.gameObject.SetActive(isFriends);
        ScrollViewFriends.gameObject.SetActive(isFriends);
        ScrollViewInviteFriend.gameObject.SetActive(invite);
        chatInputGroup.gameObject.SetActive(!invite);
    }
    public void isToggleChange(bool val)
    {
        if (val == true)
        {
            //chatInputGroup.SetActive(true);
            ActiveScreenTab(chatAll.isOn, friends.isOn, beInvite.isOn);
            RectTransform tem = ChatAllContainer.transform.GetComponent<RectTransform>();
            tem = ChatAllContainer.GetComponent<RectTransform>();
            if (chatAll.isOn && curTabActive != 0)
            {
                warningChat.SetActive(false);
                curTabActive = 0;
            }
            
            else if (friends.isOn && curTabActive!=1)
            {
                chatInputGroup.SetActive(false);
                warningChat.SetActive(false);
                curTabActive = 1;
                if (!isGetFriend)
                {
                    SFS.Instance.GetListFriend();
                    isGetFriend = true;
                }
            }else if (beInvite.isOn && curTabActive != 2)
            {
                warningInvite.SetActive(false);
                curTabActive = 2;
            }
        }
    }
    
    public void SendMessageToServer()
    {
        InputField txt = textSend.GetComponent<InputField>();
       // string msg = txt.textComponent.text;
        string msg = txt.text;
        if (msg == "") return;
        int idUser = MyInfo.SFS_ID;
        int tab = chatAll.isOn == true ? 1 : 0;

        Debug.LogError("txt.textComponent.text=====" + txt.text);
        //string msg = idUser.ToString() + "#" + textSend.GetComponent<Text>().text +"#" +tab+ "#" + Const.CHAT_CHAR_COMPONENT;
        
        string uName = MyInfo.NAME;
        if (tab == 1)
        {
            SFS.Instance.SendPublicChatRoom(MyInfo.MY_ID_VIP, msg, idUser, uName, tab);
        }else
        {            
            if (curIdFriendChat == -1)
            {
                FriendItem friend = GetFriendById(curIdFriendChat);
                if (friend != null)
                {
                    friend.ShowOfflineMessage("Người dùng đang không trực tuyến!");
                }
            }
            else
            {
                SFS.Instance.SendPrivateMessageChatRoom(msg, idUser, uName, tab, curIdFriendChat);
            }
        }

        txt.text = "";
    }
    public void ResponseChatFromServer(IDictionary param)
    {
        if (messageList.Count > maxMessage)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }
        warningChat.SetActive(true);
        warningBtnChat.SetActive(true);
        string msg = (string)param["message"];
        Room room = (Room)param["room"];
        User sender = (User)param["sender"];
        SFSObject data = (SFSObject)param["data"];


        //Debug.LogError("msg------------ " + msg);

        Messager newMsg = new Messager();
        
        newMsg.text = msg;
        GameObject newText=null;
        //if (data.GetInt("tab") == 1)
        //{
            newText = Instantiate(textObject, ChatAllContainer.transform);
            newMsg.textObject = newText.GetComponent<Text>();
        //string vipShow = data.GetInt("vip") > 0 ? data.GetInt("vip").ToString() : "";
            if (data.GetInt("vip") == 0)
            {
                newMsg.textObject.text = data.GetUtfString("uName") + " : " + newMsg.text;
            }
            else
            {
                newMsg.textObject.text = data.GetUtfString("uName") + "[vip" + data.GetInt("vip") + "]" + " : " + newMsg.text;
            }
            
            if (data.GetInt("idSFS") == MyInfo.SFS_ID)
            {
                newMsg.textObject.color = Color.yellow;
            }
			if (data.GetInt("vip") >= 10)
            {
                newMsg.textObject.color = Color.red;
            }
            else if(data.GetInt("vip") >= 7)
            {
                newMsg.textObject.color = new Color(255.0f/255.0f, 20.0f/255.0f, 147.0f/255.0f);
            }
            else if (data.GetInt("vip") >= 4)
            {
                newMsg.textObject.color = new Color(255.0f/255.0f, 165.0f/255.0f, 0);
            }
        //for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        //{
        //    string mod = DataHelper.DanhSachMod[i];
        //    string ten = data.GetUtfString("uName");
        //    if (ten == mod)
        //    {
        //        newMsg.textObject.color = Color.green;
        //    }
        //}
            messageList.Add(newMsg);
        //}        
    }
    private void OnChatFriendResponse(IDictionary param)
    {
        SFSObject data = (SFSObject)param["data"];
        int idReceive = data.GetInt("idReceive");
        int idSFSSender = data.GetInt("idSFS");
        FriendItem friend = GetFriendById(idReceive);
        if (friend != null)
        {
            friend.AddChatFromServer(param);
        }
        else
        {
            friend = GetFriendById(idSFSSender);
            if (friend != null)
            {
                friend.AddChatFromServer(param);
            }
        }
        
    }
    private FriendItem GetFriendById(int id)
    {
        foreach(FriendItem friend in FriendItemList)
        {
            if(friend.sfsId == id)
            {
                return friend;
            }
        }
        return null;
    }
    public void OnExitChatRoom(string uName)
    {
        foreach (FriendItem friend in FriendItemList)
        {
            if (friend.uId == uName)
            {
                friend.ChangeStatusOff();
                break;
            }
        }
    }
    public void OnEnterChatRoom(Hashtable data)
    {
        //string uName = data["uN"].ToString();
        int sfsid = (int)data["sfsid"];
        string uid = data["uid"].ToString();
        string avatar = data["avatar"].ToString();
        int avatarborder = (int)data["avatarborder"];
        
        foreach (FriendItem friend in FriendItemList)
        {
            if (friend.uId == uid)
            {
                friend.ChangeStatusON(uid, sfsid, avatar, avatarborder);
                break;
            }
        }
    }
    public void OnChatResponse(GamePacket param)
    {
        Debug.Log("OnChatResponse rsp - " + param.ToString());
        Debug.Log("OnChatResponse cmd - " + param.cmd);

        switch (param.cmd)
        {

            case CommandKey.JOIN_CHAT_ROOM:
                JoinChatComplete(param);
                break;
            case CommandKey.GET_LIST_FRIEND:
                GetListFriend(param);
                break;
            case CommandKey.ADD_FRIEND_REQUEST://gửi về cho user được mời
                AddFriendBeInvite(param);
                break;
            case CommandKey.ADD_FRIEND_RESULT://gửi về cho user đã gửi lời mời
                AddFriendRes(param);
                break;
            case CommandKey.ADD_FRIEND_CONFIRM:
                AddFriendConfirm(param);
                break;
            case CommandKey.ADD_FRIEND_SUCCESS:
                CheckGetListFriendAgain();
                break;
            case CommandKey.ADD_FRIEND_IGNORE:
                //uid: id user ko đồng ý kết bạn
                //uN: username user ko đồng ý kết ban
                break;
            case CommandKey.UNFRIEND_FRIEND_REQUEST:
                UnFriendRes(param);
                break;
        }
    }

    private void UnFriendRes(GamePacket param)
    {
        Debug.Log("AddFriendRes rsp - ");
        //user_id_1,user_name_1,user_id_2,user_name_2
        string user_id_1, user_name_1, user_id_2, user_name_2;

        user_id_1 = param.GetString("user_id_1");
        user_name_1 = param.GetString("user_name_1");
        user_id_2 = param.GetString("user_id_2");
        user_name_2 = param.GetString("user_name_2");

        if (user_id_1 == MyInfo.ID)
        {
            foreach (FriendItem friend in FriendItemList)
            {
                if (friend.uId == user_id_2)
                {
                    friend.RemoveFriend();
                    FriendItemList.Remove(friend);
                    Destroy(friend.gameObject);
                    break;
                }
            }
        }
        if(user_id_2 == MyInfo.ID)
        {
            foreach (FriendItem friend in FriendItemList)
            {
                if (friend.uId == user_id_1)
                {
                    friend.RemoveFriend();
                    FriendItemList.Remove(friend);
                    Destroy(friend.gameObject);
                    break;
                }
            }
        }
    }

    private void CheckGetListFriendAgain()
    {
        if (friends.isOn)
        {
            SFS.Instance.GetListFriend();
            isGetFriend = true;
        }else
        {
            isGetFriend = false;
        }
    }
    private void AddFriendConfirm(GamePacket param)
    {
        CheckGetListFriendAgain();
        warningFriend.SetActive(true);
        warningBtnChat.SetActive(true);
    }
    private void AddFriendRes(GamePacket param)//gửi về cho user đã gửi lời mời
    {
        Debug.Log("AddFriendRes rsp - ");
        int result = param.GetInt("grs");
        if (result == 1)
        {
            string uName = param.GetString("uN");
        }else
        {
            Debug.Log("User Này Không Tồn Tại");
        }
    }

    private void AddFriendBeInvite(GamePacket param)//gửi về cho user được mời
    {
        //{ "avatarborder":-1,"uid":"5cef876b8ead77e5338b4583","uN":"tfczzz","grs":1,"avatar":"12.png"}
        warningInvite.SetActive(true);
        warningBtnChat.SetActive(true);
        Debug.Log("AddFriendBeInvite rsp - ");        
        string uid = param.GetString("uid");
        string uName = param.GetString("uN");
        string avatar = param.GetString("avatar");
        int avatarborder = param.GetInt("avatarborder");

        GameObject friendInvite = Instantiate(InviteFriend, InviteFriendContainer.transform);
        friendInvite.GetComponent<FriendInvite>().Init(uid, uName, avatar, avatarborder);
    }

    private void GetListFriend(GamePacket param)
    {
        curIdFriendChat = -2;
        foreach (Transform child in FriendsContainer.transform)
        {
            Destroy(child.gameObject);
        }

        //SFS.Instance.RequestAddFriend(MyInfo.ID);
        Debug.Log("GetListFriend rsp - ");
        ISFSObject data = param.GetSFSObject("dt");
        ISFSArray lstoff = data.GetSFSArray("list_off");
        ISFSArray lston = data.GetSFSArray("list_onl");
        GameObject friendItem=null;
        string avatar = "";
        int avatarborder=-1;
        int sfsId = -1;
        if (lston.Count > 0)
        {
            foreach (SFSObject item in lston)
            {
                avatar = "";
                if (item.ContainsKey("avatar"))
                {
                    avatar = item.GetUtfString("avatar");
                }
                avatarborder = -1;
                if (item.ContainsKey("avatarborder"))
                {
                    avatarborder = item.GetInt("avatarborder");
                }
                sfsId = item.GetInt("sfsid");
                friendItem = Instantiate(FriendItem, FriendsContainer.transform);
                friendItem.GetComponent<FriendItem>().Init(FriendsChatContainer,textObject, OnClickFriendItem, item.GetUtfString("user_id"), item.GetUtfString("user_name"), avatar, avatarborder, true, sfsId);
                FriendItemList.Add(friendItem.GetComponent<FriendItem>());
            }
        }
        avatar = "";
        avatarborder = -1;
        sfsId = -1;
        if (lstoff.Count > 0)
        {            
            foreach(SFSObject item in lstoff)
            {
                friendItem = Instantiate(FriendItem, FriendsContainer.transform);                
                friendItem.GetComponent<FriendItem>().Init(FriendsChatContainer, textObject, OnClickFriendItem, item.GetUtfString("user_id"), item.GetUtfString("user_name"), avatar, avatarborder, false,sfsId);
                FriendItemList.Add(friendItem.GetComponent<FriendItem>());
            }
            
        }
        
    }
    int curIdFriendChat=-2;
    private void OnClickFriendItem(GameObject obj)
    {
        FriendItem item = obj.GetComponent<FriendItem>();
        curIdFriendChat = item.sfsId;
        item.ShowMessage();
        chatInputGroup.SetActive(true);
    }
    private void JoinChatComplete(GamePacket param)
    {
        
    }
}

[System.Serializable]
public class Messager
{
    public string text;
    public Text textObject;
}
