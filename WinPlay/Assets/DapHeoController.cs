﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DapHeoController : MonoBehaviour {
    [SerializeField]
    GameObject ObjPig, ObjHammer, iconVideo;
    [SerializeField]
    ParticleSystem NoHeo_1, NoHeo_2, NoHeo_3;
    [SerializeField]
    public Text TxtTurn, TxtHetBua;
    [SerializeField]
    public Button BtnDapTiep;
    [SerializeField]
    RectTransform ViTriHeo, ViTriBua;
    [SerializeField]
    Sprite HinhHeo_1, HinhHeo_2, HinhHeo_3;
    [SerializeField]
    Sprite HinhBua_1, HinhBua_2, HinhBua_3;
    [SerializeField]
    EvenDapHeo EvenDH;
    [SerializeField]
    public GameObject imgHetBua;
    [SerializeField]
    Image IconBua;
    [SerializeField]
    Sprite BuaSat, BuaBac, BuaVang;
    public GameObject Heo = null;
    GameObject Bua = null;


    public int MyId = 0;
    public int MyTurn = 0;
    Button BuyTurn;
    // Use this for initialization
   
    public void Clear()
    {
        if (Heo != null)
        {
            Debug.Log("6666666666666666666666666666666666622222222222222222222");
            Destroy(Heo);
        }

        if (Bua != null)
        {
            Debug.Log("66666666666666666666666666666666666");
            Destroy(Bua);
        }
        
    }

    public void Init(int id, string turn)
    {
        if (Heo != null)
        {
            Destroy(Heo);
        }
        if (Bua != null)
        {
            Destroy(Bua);
        }
        SinhBua(id);
        SinhHeo(id);
        TxtTurn.text = turn;
        MyTurn = int.Parse(turn);
        MyId = id;

        BtnDapTiep.gameObject.SetActive(false);
        imgHetBua.SetActive(false);
        DapHeo();
        BuyTurn = imgHetBua.GetComponent<Button>();
        iconVideo.SetActive(false);
        BtnDapTiep.onClick.RemoveAllListeners();
        BtnDapTiep.onClick.AddListener(DapTiep);
        if (MyId == 1)
        {
            IconBua.sprite = BuaSat;
        }
        else if (MyId == 2)
        {
            IconBua.sprite = BuaBac;
        }
        else if (MyId == 3)
        {
            IconBua.sprite = BuaVang;
        }
    }

    public void DapHeo()
    {
        Debug.Log("dap heo 2 lan ne");
        imgHetBua.SetActive(false);
        BtnDapTiep.gameObject.SetActive(false);
        BuaActive(true);
        ChoNoHeo(1.4f);

    }


    public void DapTiep()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        EvenDH.ClosePanelGift(true);
        if(Heo == null)
        {
            SinhHeo(MyId);
        }
        
        
        BtnDapTiep.gameObject.SetActive(false);

    }


    /*
    private IEnumerator ChoDapTiep(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        DapHeo();
      
    }

    */

    public void CapNhatSoBua(int sl)
    {
        TxtTurn.text = sl.ToString();
        MyTurn = sl;
        if (MyTurn == 0)
        {

            BuyTurn.onClick.RemoveAllListeners();
      //      EvenDH.ClosePanelGift();

            if (MyId == 1)
            {
                iconVideo.SetActive(true);
                TxtHetBua.text = "Đập Tiếp   ";
                BuyTurn.onClick.AddListener(EvenDH.XemQuangCao);

            }
            else if (MyId == 2)
            {
                iconVideo.SetActive(false);
                TxtHetBua.text = "Thêm Búa";
                
                BuyTurn.onClick.AddListener(EvenDH.MuaLuot_2);
                
            }
            else if (MyId == 3)
            {
                iconVideo.SetActive(false);
                TxtHetBua.text = "Thêm Búa";
                BuyTurn.onClick.AddListener(EvenDH.MuaLuot_3);
            }
          //  SinhHeo(MyId);
            imgHetBua.SetActive(true);
            
        }     



    }

    void Update()
    {
        if (MyTurn == 0)
        {
            BtnDapTiep.gameObject.SetActive(false);
        }
    }


    public void BuaActive(bool isActive)
    {
        Animator BuaAnim = Bua.GetComponent<Animator>();
        BuaAnim.SetBool("DapHeo", isActive);
    }


    public void ChoNoHeo(float time)
    {
        StartCoroutine(WaitAndBummm(time));
    }



    private IEnumerator WaitAndBummm(float waitTime)
    {       
        yield return new WaitForSeconds(waitTime);
        HeoNo();
        if (MyId == 1)
        {
            EvenDH.DapHeo_1();
        }
        else if (MyId == 2)
        {
            EvenDH.DapHeo_2();
        }
        else if (MyId == 3)
        {
            EvenDH.DapHeo_3();
        }
       
        BuaActive(false);

       // 
    }

    public void ActiveBtnDapTiep()
    {
        if (MyTurn > 0)
        {
            BtnDapTiep.gameObject.SetActive(true);
        }
        
    }

    public void HeoNo()
    {
        SoundManager.PlaySound(SoundManager.DAP_HEO);
        Destroy(Heo);
        if (MyId == 1)
        {
            NoHeo_1.Play();
        }
        else if (MyId == 2)
        {
            NoHeo_2.Play();
        }
        else if (MyId == 3)
        {
            NoHeo_3.Play();
        }
    }


    public void SinhHeo(int id)
    {
        // Heo = new GameObject();
        if (Heo != null)
        {
            Destroy(Heo);
        }
        Heo = Instantiate(ObjPig, ViTriHeo) as GameObject;
        Heo.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        Image ImgHeo = Heo.GetComponent<Image>();
        if (id == 1)
        {
            ImgHeo.sprite = HinhHeo_1;
        }
        else if (id == 2)
        {
            ImgHeo.sprite = HinhHeo_2;
        }
        else if (id == 3)
        {
            ImgHeo.sprite = HinhHeo_3;
        }
    }

    public void SinhBua(int id)
    {
        //Bua = new GameObject();
        Bua = Instantiate(ObjHammer, ViTriBua) as GameObject;
        Bua.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        Image ImgBua = Bua.GetComponent<Image>();
        if (id == 1)
        {
            ImgBua.sprite = HinhBua_1;
        }
        else if (id == 2)
        {
            ImgBua.sprite = HinhBua_2;
        }
        else if (id == 3)
        {
            ImgBua.sprite = HinhBua_3;
        }
    }
}
