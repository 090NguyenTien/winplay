﻿using UnityEngine;
using System;

public class Reporter {

	public const String REPORTER_GAMEOBJECT_NAME = "ReporterGameObject";
	public const string REPORTER_PLUGIN_REPORT_PREFAB_DIRECTORY = "Assets/Reporter/ReporterGameObject.prefab";

	private static Report reportObject = null;

    /* Capture a screenshot from the current scene.
	 * This method is seperated from the report sending process, to be able to capture a screenshot before a screen triggered to 
	*/
    public static void CaptureScreenShot()
	{
		reportObject = checkReportGameObject (reportObject);
		reportObject.takeScreenShot ();
	}

    /* Send a custom report on demand with a title and message.
	 * Take a screenshot if not taken already.
	*/
    public static void SendUserReport(string title="", string message="", string userId="")
	{
		reportObject = checkReportGameObject (reportObject);
		if(reportObject.ScreenShotTexture != null)
		{
			reportObject.Send(title,message,userId);
		}else{
			reportObject.Invoke("Send",1);
		}
	}

    /* A GameObject is needed in the scene to be able to take a screenshot.
	  * Check if the Report GameObject exits, if not create one and put it in the scene.
	  * Return the Report instance.
	*/
    public static Report checkReportGameObject(Report reportObject)
	{
		if(reportObject == null)
		{
			if(GameObject.Find(REPORTER_GAMEOBJECT_NAME)){
				reportObject = GameObject.Find(REPORTER_GAMEOBJECT_NAME).GetComponent<Report>();
			}

			GameObject go = (GameObject)Resources.Load(REPORTER_PLUGIN_REPORT_PREFAB_DIRECTORY, typeof(GameObject));
			if(go == null){
				if(reportObject != null)
				{
					GameObject.DestroyImmediate(reportObject.gameObject);
				}
				go = new GameObject(REPORTER_GAMEOBJECT_NAME,typeof(Report));
				reportObject = go.GetComponent<Report> ();
			}
		}
		return reportObject;
	}

}
